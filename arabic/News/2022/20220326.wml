#use wml::debian::translation-check translation="d673db021e55bd42a14712c110ed1253cd2c8b1e" maintainer="med"
<define-tag pagetitle>تحديث دبيان 11: الإصدار 11.3</define-tag>
<define-tag release_date>2022-03-26</define-tag>
#use wml::debian::news

<define-tag release>11</define-tag>
<define-tag codename>bullseye</define-tag>
<define-tag revision>11.3</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>
يسعد مشروع دبيان الإعلان عن التحديث الثالث لتوزيعته المستقرة دبيان <release> (الاسم الرمزي <q><codename></q>).
بالإضافة إلى تسوية بعض المشكلات الحرجة يصلح هذا التحديث بالأساس مشاكلات الأمان. تنبيهات الأمان أعلنت بشكل منفصل ومشار إليها فقط في هذا الإعلان.
</p>

<p>
يرجى ملاحظة أن هذا التحديث لا يشكّل إصدار جديد لدبيان <release> بل فقط تحديثات لبعض الحزم المضمّنة
وبالتالي ليس بالضرورة رمي الوسائط القديمة للإصدار <q><codename></q>، يمكن تحديث الحزم باستخدام مرآة دبيان محدّثة.
</p>

<p>
الذين يثبّتون التحديثات من security.debian.org باستمرار لن يكون عليهم تحديث العديد من الحزم،
أغلب التحديثات مضمّنة في هذا التحديث.
</p>

<p>
صور جديدة لأقراص التثبيت ستكون متوفرة في موضعها المعتاد.
</p>

<p>
يمكن الترقية من  تثبيت آنيّ إلى هذه المراجعة بتوجيه نظام إدارة الحزم إلى إحدى مرايا HTTP الخاصة بدبيان.
قائمة شاملة لمرايا دبيان على المسار:
</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>




<h2>إصلاح العديد من العلاّت</h2>

<p>أضاف هذا التحديث للإصدار المستقر بعض الإصلاحات المهمة للحزم التالية:</p>

<table border=0>
<tr><th>الحزمة</th>               <th>السبب</th></tr>
<correction apache-log4j1.2 "Resolve security issues [CVE-2021-4104 CVE-2022-23302 CVE-2022-23305 CVE-2022-23307], by removing support for the JMSSink, JDBCAppender, JMSAppender and Apache Chainsaw modules">
<correction apache-log4j2 "Fix remote code execution issue [CVE-2021-44832]">
<correction apache2 "New upstream release; fix crash due to random memory read [CVE-2022-22719]; fix HTTP request smuggling issue [CVE-2022-22720]; fix out-of-bounds write issues [CVE-2022-22721 CVE-2022-23943]">
<correction atftp "Fix information leak issue [CVE-2021-46671]">
<correction base-files "Update for the 11.3 point release">
<correction bible-kjv "Fix off-by-one-error in search">
<correction chrony "Allow reading the chronyd configuration file that timemaster(8) generates">
<correction cinnamon "Fix crash when adding an online account with login">
<correction clamav "New upstream stable release; fix denial of service issue [CVE-2022-20698]">
<correction cups-filters "Apparmor: allow reading from Debian Edu's cups-browsed configuration file">
<correction dask.distributed "Fix undesired listening of workers on public interfaces [CVE-2021-42343]; fix compatibility with Python 3.9">
<correction debian-installer "Rebuild against proposed-updates; update Linux kernel ABI to 5.10.0-13">
<correction debian-installer-netboot-images "Rebuild against proposed-updates">
<correction debian-ports-archive-keyring "Add <q>Debian Ports Archive Automatic Signing Key (2023)</q>; move the 
2021 signing key to the removed keyring">
<correction django-allauth "Fix OpenID support">
<correction djbdns "Raise the axfrdns, dnscache, and tinydns data limit">
<correction dpdk "New upstream stable release">
<correction e2guardian "Fix missing SSL certificate validation issue [CVE-2021-44273]">
<correction epiphany-browser "Work around a bug in GLib, fixing a UI process crash">
<correction espeak-ng "Drop spurious 50ms delay while processing events">
<correction espeakup "debian/espeakup.service: Protect espeakup from system overloads">
<correction fcitx5-chinese-addons "fcitx5-table: add missing dependencies on fcitx5-module-pinyinhelper and fcitx5-module-punctuation">
<correction flac "Fix out-of-bounds write issue [CVE-2021-0561]">
<correction freerdp2 "Disable additional debug logging">
<correction galera-3 "New upstream release">
<correction galera-4 "New upstream release">
<correction gbonds "Use Treasury API for redemption data">
<correction glewlwyd "Fix possible privilege escalation">
<correction glibc "Fix bad conversion from ISO-2022-JP-3 with iconv [CVE-2021-43396]; fix buffer overflow issues [CVE-2022-23218 CVE-2022-23219]; fix use-after-free issue [CVE-2021-33574]; stop replacing older versions of /etc/nsswitch.conf; simplify the check for supported kernel versions, as 2.x kernels are no longer supported; support installation on kernels with a release number greater than 255">
<correction glx-alternatives "After initial setup of the diversions, install a minimal alternative to the diverted files so that libraries are not missing until glx-alternative-mesa processes its triggers">
<correction gnupg2 "scd: Fix CCID driver for SCM SPR332/SPR532; avoid network interaction in generator, which can lead to hangs">
<correction gnuplot "Fix division by zero [CVE-2021-44917]">
<correction golang-1.15 "Fix IsOnCurve for big.Int values that are not valid coordinates [CVE-2022-23806]; math/big: prevent large memory consumption in Rat.SetString [CVE-2022-23772]; cmd/go: prevent branches from materializing into versions [CVE-2022-23773]; fix stack exhaustion compiling deeply nested expressions [CVE-2022-24921]">
<correction golang-github-containers-common "Update seccomp support to enable use of newer kernel versions">
<correction golang-github-opencontainers-specs "Update seccomp support to enable use of newer kernel versions">
<correction gtk+3.0 "Fix missing search results when using NFS; prevent Wayland clipboard handling from locking up in certain corner cases; improve printing to mDNS-discovered printers">
<correction heartbeat "Fix creation of /run/heartbeat on systems using systemd">
<correction htmldoc "Fix out-of-bounds read issue [CVE-2022-0534]">
<correction installation-guide "Update documentation and translations">
<correction intel-microcode "Update included microcode; mitigate some security issues [CVE-2020-8694 CVE-2020-8695 CVE-2021-0127 CVE-2021-0145 CVE-2021-0146 CVE-2021-33120]">
<correction ldap2zone "Use <q>mktemp</q> rather than the deprecated <q>tempfile</q>, avoiding warnings">
<correction lemonldap-ng "Fix auth process in password-testing plugins [CVE-2021-40874]">
<correction libarchive "Fix extracting hardlinks to symlinks; fix handling of symlink ACLs [CVE-2021-23177]; never follow symlinks when setting file flags [CVE-2021-31566]">
<correction libdatetime-timezone-perl "Update included data">
<correction libgdal-grass "Rebuild against grass 7.8.5-1+deb11u1">
<correction libpod "Update seccomp support to enable use of newer kernel versions">
<correction libxml2 "Fix use-after-free issue [CVE-2022-23308]">
<correction linux "New upstream stable release; [rt] Update to 5.10.106-rt64; increase ABI to 13">
<correction linux-signed-amd64 "New upstream stable release; [rt] Update to 5.10.106-rt64; increase ABI to 13">
<correction linux-signed-arm64 "New upstream stable release; [rt] Update to 5.10.106-rt64; increase ABI to 13">
<correction linux-signed-i386 "New upstream stable release; [rt] Update to 5.10.106-rt64; increase ABI to 13">
<correction mariadb-10.5 "New upstream release; security fixes [CVE-2021-35604 CVE-2021-46659 CVE-2021-46661 CVE-2021-46662 CVE-2021-46663 CVE-2021-46664 CVE-2021-46665 CVE-2021-46667 CVE-2021-46668 CVE-2022-24048 CVE-2022-24050 CVE-2022-24051 CVE-2022-24052]">
<correction mpich "Add Breaks: on older versions of libmpich1.0-dev, resolving some upgrade issues">
<correction mujs "Fix buffer overflow issue [CVE-2021-45005]">
<correction mutter "Backport various fixes from upstream's stable branch">
<correction node-cached-path-relative "Fix prototype pollution issue [CVE-2021-23518]">
<correction node-fetch "Don't forward secure headers to third party domains [CVE-2022-0235]">
<correction node-follow-redirects "Don't send Cookie header across domains [CVE-2022-0155]; don't send confidential headers across schemes [CVE-2022-0536]">
<correction node-markdown-it "Fix regular expression-based denial of service issue [CVE-2022-21670]">
<correction node-nth-check "Fix regular expression-based denial of service issue [CVE-2021-3803]">
<correction node-prismjs "Escape markup in command line output [CVE-2022-23647]; update minified files to ensure that Regular Expression Denial of Service issue is resolved [CVE-2021-3801]">
<correction node-trim-newlines "Fix regular expression-based denial of service issue [CVE-2021-33623]">
<correction nvidia-cuda-toolkit "cuda-gdb: Disable non-functional python support causing segmentation faults; use a snapshot of openjdk-8-jre (8u312-b07-1)">
<correction nvidia-graphics-drivers-tesla-450 "New upstream release; fix denial of service issues [CVE-2022-21813 CVE-2022-21814]; nvidia-kernel-support: Provide /etc/modprobe.d/nvidia-options.conf as a template">
<correction nvidia-modprobe "New upstream release">
<correction openboard "Fix application icon">
<correction openssl "New upstream release; fix armv8 pointer authentication">
<correction openvswitch "Fix use-after-free issue [CVE-2021-36980]; fix installation of libofproto">
<correction ostree "Fix compatibility with eCryptFS; avoid infinite recursion when recovering from certain errors; mark commits as partial before downloading; fix an assertion failure when using a backport or local build of GLib &gt;= 2.71; fix the ability to fetch OSTree content from paths containing non-URI characters (such as backslashes) or non-ASCII">
<correction pdb2pqr "Fix compatibility of propka with Python 3.8 or above">
<correction php-crypt-gpg "Prevent additional options being passed to GPG [CVE-2022-24953]">
<correction php-laravel-framework "Fix cross-site scripting issue [CVE-2021-43808], missing blocking of executable content upload [CVE-2021-43617]">
<correction phpliteadmin "Fix cross-site scripting issue [CVE-2021-46709]">
<correction prips "Fix infinite wrapping if a range reaches 255.255.255.255; fix CIDR output with addresses that differ in their first bit">
<correction pypy3 "Fix build failures by removing extraneous #endif from import.h">
<correction python-django "Fix denial of service issue [CVE-2021-45115], information disclosure issue [CVE-2021-45116], directory traversal issue [CVE-2021-45452]; fix a traceback around the handling of RequestSite/get_current_site() due to a circular import">
<correction python-pip "Avoid a race-condition when using zip-imported dependencies">
<correction rust-cbindgen "New upstream stable release to support builds of newer firefox-esr and thunderbird versions">
<correction s390-dasd "Stop passing deprecated -f option to dasdfmt">
<correction schleuder "Migrate boolean values to integers, if the ActiveRecord SQLite3 connection adapter is in use, restoring functionality">
<correction sphinx-bootstrap-theme "Fix search functionality">
<correction spip "Fix several cross-site scripting issues">
<correction symfony "Fix CVE injection issue [CVE-2021-41270]">
<correction systemd "Fix uncontrolled recursion in systemd-tmpfiles [CVE-2021-3997]; demote systemd-timesyncd from Depends to Recommends, removing a dependency cycle; fix failure to bind mount a directory into a container using machinectl; fix regression in udev resulting in long delays when processing partitions with the same label; fix a regression when using systemd-networkd in an unprivileged LXD container">
<correction sysvinit "Fix parsing of <q>shutdown +0</q>; clarify that when called with a <q>time</q> shutdown will not exit">
<correction tasksel "Install CUPS for all *-desktop tasks, as task-print-service no longer exists">
<correction usb.ids "Update included data">
<correction weechat "Fix denial of service issue [CVE-2021-40516]">
<correction wolfssl "Fix several issues related to OCSP-handling [CVE-2021-3336 CVE-2021-37155 CVE-2021-38597] and TLS1.3 support [CVE-2021-44718 CVE-2022-25638 CVE-2022-25640]">
<correction xserver-xorg-video-intel "Fix SIGILL crash on non-SSE2 CPUs">
<correction xterm "Fix buffer overflow issue [CVE-2022-24130]">
<correction zziplib "Fix denial of service issue [CVE-2020-18442]">
</table>


<h2>تحديثات الأمان</h2>


<p>
أضافت هذه المراجعة تحديثات الأمان التالية للإصدار المستقر.
سبق لفريق الأمان نشر تنبيه لكل تحديث:
</p>

<table border=0>
<tr><th>معرَّف التنبيه</th>  <th>الحزمة</th></tr>
<dsa 2021 5000 openjdk-11>
<dsa 2021 5001 redis>
<dsa 2021 5012 openjdk-17>
<dsa 2021 5021 mediawiki>
<dsa 2021 5023 modsecurity-apache>
<dsa 2021 5024 apache-log4j2>
<dsa 2021 5025 tang>
<dsa 2021 5027 xorg-server>
<dsa 2021 5028 spip>
<dsa 2021 5029 sogo>
<dsa 2021 5030 webkit2gtk>
<dsa 2021 5031 wpewebkit>
<dsa 2021 5033 fort-validator>
<dsa 2022 5035 apache2>
<dsa 2022 5037 roundcube>
<dsa 2022 5038 ghostscript>
<dsa 2022 5039 wordpress>
<dsa 2022 5040 lighttpd>
<dsa 2022 5041 cfrpki>
<dsa 2022 5042 epiphany-browser>
<dsa 2022 5043 lxml>
<dsa 2022 5046 chromium>
<dsa 2022 5047 prosody>
<dsa 2022 5048 libreswan>
<dsa 2022 5049 flatpak-builder>
<dsa 2022 5049 flatpak>
<dsa 2022 5050 linux-signed-amd64>
<dsa 2022 5050 linux-signed-arm64>
<dsa 2022 5050 linux-signed-i386>
<dsa 2022 5050 linux>
<dsa 2022 5051 aide>
<dsa 2022 5052 usbview>
<dsa 2022 5053 pillow>
<dsa 2022 5054 chromium>
<dsa 2022 5055 util-linux>
<dsa 2022 5056 strongswan>
<dsa 2022 5057 openjdk-11>
<dsa 2022 5058 openjdk-17>
<dsa 2022 5059 policykit-1>
<dsa 2022 5060 webkit2gtk>
<dsa 2022 5061 wpewebkit>
<dsa 2022 5062 nss>
<dsa 2022 5063 uriparser>
<dsa 2022 5064 python-nbxmpp>
<dsa 2022 5065 ipython>
<dsa 2022 5067 ruby2.7>
<dsa 2022 5068 chromium>
<dsa 2022 5070 cryptsetup>
<dsa 2022 5071 samba>
<dsa 2022 5072 debian-edu-config>
<dsa 2022 5073 expat>
<dsa 2022 5075 minetest>
<dsa 2022 5076 h2database>
<dsa 2022 5077 librecad>
<dsa 2022 5078 zsh>
<dsa 2022 5079 chromium>
<dsa 2022 5080 snapd>
<dsa 2022 5081 redis>
<dsa 2022 5082 php7.4>
<dsa 2022 5083 webkit2gtk>
<dsa 2022 5084 wpewebkit>
<dsa 2022 5085 expat>
<dsa 2022 5087 cyrus-sasl2>
<dsa 2022 5088 varnish>
<dsa 2022 5089 chromium>
<dsa 2022 5091 containerd>
<dsa 2022 5092 linux-signed-amd64>
<dsa 2022 5092 linux-signed-arm64>
<dsa 2022 5092 linux-signed-i386>
<dsa 2022 5092 linux>
<dsa 2022 5093 spip>
<dsa 2022 5095 linux-signed-amd64>
<dsa 2022 5095 linux-signed-arm64>
<dsa 2022 5095 linux-signed-i386>
<dsa 2022 5095 linux>
<dsa 2022 5098 tryton-server>
<dsa 2022 5099 tryton-proteus>
<dsa 2022 5100 nbd>
<dsa 2022 5101 libphp-adodb>
<dsa 2022 5102 haproxy>
<dsa 2022 5103 openssl>
<dsa 2022 5104 chromium>
<dsa 2022 5105 bind9>
</table>


<h2>الحزم المزالة</h2>

<p>
الحزم التالية أزيلت لأسباب خارجة عن سيطرتنا:
</p>

<table border=0>
<tr><th>الحزمة</th>               <th>السبب</th></tr>
<correction angular-maven-plugin "No longer useful">
<correction minify-maven-plugin "No longer useful">

</table>

<h2>مُثبِّت دبيان</h2>
<p>
حدِّث المُثبِّت ليتضمن الإصلاحات المندرجة في هذا الإصدار المستقر.
</p>

<h2>المسارات</h2>

<p>
القائمة الكاملة للحزم المغيّرة في هذه المراجعة:
</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>التوزيعة المستقرة الحالية:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/stable/">
</div>

<p>التحديثات المقترحة للتوزيعة المستقرة:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/proposed-updates">
</div>

<p>معلومات حول التوزيعة المستقرة (ملاحظات الإصدار والأخطاء إلخ):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>معلومات وإعلانات الأمان:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>حول دبيان</h2>

<p>
مشروع دبيان هو اتحاد لمطوري البرمجيات الحرة تطوعوا بالوقت والمجهود لإنتاج نظام تشعيل دبيان حر بالكامل.
</p>

<h2>معلومات الاتصال</h2>

<p>
لمزيد من المعلومات يرجى زيارة موقع دبيان
<a href="$(HOME)/">https://www.debian.org/</a>
أو إرسال بريد إلكتروني إلى &lt;press@debian.org&gt;
أو الاتصال بفريق إصدار المستقرة على
&lt;debian-release@lists.debian.org&gt;.
</p>
