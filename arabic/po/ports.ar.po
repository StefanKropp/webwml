# translation of ports.ar.po to Arabic
# Ossama M. Khayat <okhayat@yahoo.com>, 2005.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2022-05-25 11:00+0000\n"
"Last-Translator: Med <medeb@protonmail.com>\n"
"Language-Team: Arabic <debian-l10n-arabic@lists.debian.org>\n"
"Language: ar\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 "
"&& n%100<=10 ? 3 : n%100>=11 ? 4 : 5;\n"
"X-Generator: Poedit 2.4.2\n"

#: ../../english/ports/alpha/menu.inc:6
msgid "Debian for Alpha"
msgstr "دبيان لبُنية Alpha"

#: ../../english/ports/hppa/menu.inc:6
msgid "Debian for PA-RISC"
msgstr "دبيان لبُنية PA-RISC"

#: ../../english/ports/hurd/menu.inc:10
msgid "Hurd CDs"
msgstr "أقراص Hurd مدمجة"

#: ../../english/ports/ia64/menu.inc:6
msgid "Debian for IA-64"
msgstr "دبيان لبُنية IA-64"

#: ../../english/ports/menu.defs:11
msgid "Contact"
msgstr "المراسلة"

#: ../../english/ports/menu.defs:15
msgid "CPUs"
msgstr "المعالجات"

#: ../../english/ports/menu.defs:19
msgid "Credits"
msgstr "المساهمون"

#: ../../english/ports/menu.defs:23
msgid "Development"
msgstr "تطوير"

#: ../../english/ports/menu.defs:27
msgid "Documentation"
msgstr "توثيق"

#: ../../english/ports/menu.defs:31
msgid "Installation"
msgstr "تثبيت"

#: ../../english/ports/menu.defs:35
msgid "Configuration"
msgstr "ضبط"

#: ../../english/ports/menu.defs:39
msgid "Links"
msgstr "روابط"

#: ../../english/ports/menu.defs:43
msgid "News"
msgstr "أخبار"

#: ../../english/ports/menu.defs:47
msgid "Porting"
msgstr "تحويل للأنظمة"

#: ../../english/ports/menu.defs:51
msgid "Ports"
msgstr "تحويلات"

#: ../../english/ports/menu.defs:55
msgid "Problems"
msgstr "مشاكل"

#: ../../english/ports/menu.defs:59
msgid "Software Map"
msgstr "خريطة البرامج"

#: ../../english/ports/menu.defs:63
msgid "Status"
msgstr "وضع"

#: ../../english/ports/menu.defs:67
msgid "Supply"
msgstr "تزويد"

#: ../../english/ports/menu.defs:71
msgid "Systems"
msgstr "أنظمة"

#: ../../english/ports/netbsd/menu.inc:6
msgid "Debian GNU/NetBSD for i386"
msgstr "دبيان جنو/NetBSD لبُنية i386"

#: ../../english/ports/netbsd/menu.inc:10
msgid "Debian GNU/NetBSD for Alpha"
msgstr "دبيان جنو/NetBSD لبُنية Alpha"

#: ../../english/ports/netbsd/menu.inc:14
msgid "Why"
msgstr "لماذا"

#: ../../english/ports/netbsd/menu.inc:18
msgid "People"
msgstr "الناس"

#: ../../english/ports/powerpc/menu.inc:6
msgid "Debian for PowerPC"
msgstr "دبيان لبُنية PowerPC"

#: ../../english/ports/sparc/menu.inc:6
msgid "Debian for Sparc"
msgstr "دبيان لبُنية Sparc"

#~ msgid "Debian for Laptops"
#~ msgstr "دبيان للحواسيب المحمولة"

#~ msgid "Debian for AMD64"
#~ msgstr "دبيان لأنظمة AMD64"

#~ msgid "Debian for ARM"
#~ msgstr "دبيان لأنظمة ARM"

#~ msgid "Debian for Beowulf"
#~ msgstr "دبيان لأنظمة Beowulf"

#~ msgid "Main"
#~ msgstr "رئيسي"

#~ msgid "Debian GNU/FreeBSD"
#~ msgstr "دبيان غنو/FreeBSD"

#~ msgid "Debian for Motorola 680x0"
#~ msgstr "دبيان لأنظمة موتورولا 680x0"

#~ msgid "Vendor/Name"
#~ msgstr "البائع/الاسم"

#~ msgid "Date announced"
#~ msgstr "تاريخ الإعلان"

#~ msgid "Clock"
#~ msgstr "ساعة"

#~ msgid "ICache"
#~ msgstr "ICache"

#~ msgid "DCache"
#~ msgstr "DCache"

#~ msgid "TLB"
#~ msgstr "TLB"

#~ msgid "ISA"
#~ msgstr "ISA"

#~ msgid "Specials"
#~ msgstr "الأشياء الخاصة"

#~ msgid "No FPU (R2010), external caches"
#~ msgstr "No FPU (R2010), external caches"

#~ msgid "No FPU (R3010)"
#~ msgstr "No FPU (R3010)"

#~ msgid "Virtual indexed L1 cache, L2 cache controller"
#~ msgstr "Virtual indexed L1 cache, L2 cache controller"

#~ msgid "External L1 cache"
#~ msgstr "External L1 cache"

#~ msgid "Multiple chip CPU"
#~ msgstr "وحدة معالجة مركزية (CPU) متعددة الرقاقات"

#~ msgid ""
#~ "Mips16 isa extension, No FPU, 512k flash, 16k ram, DMAC, UART, Timer, "
#~ "I2C, Watchdog"
#~ msgstr ""
#~ "Mips16 isa extension, No FPU, 512k flash, 16k ram, DMAC, UART, Timer, "
#~ "I2C, Watchdog"

#~ msgid "No FPU, DRAMC, ROMC, DMAC, UART, Timer, I2C, LCD Controller"
#~ msgstr "No FPU, DRAMC, ROMC, DMAC, UART, Timer, I2C, LCD Controller"

#~ msgid "No FPU, DRAMC, ROMC, DMAC, UART, Timer"
#~ msgstr "No FPU, DRAMC, ROMC, DMAC, UART, Timer"

#~ msgid "No FPU, DRAMC, ROMC, DMAC, PCIC, UART, Timer"
#~ msgstr "No FPU, DRAMC, ROMC, DMAC, PCIC, UART, Timer"

#~ msgid "No FPU, SDRAMC, ROMC, Timer, PCMCIA, LCD Controller, IrDA"
#~ msgstr "No FPU, SDRAMC, ROMC, Timer, PCMCIA, LCD Controller, IrDA"

#~ msgid "No FPU, SDRAMC, ROMC, UART, Timer, PCMCIA, LCD Controller, IrDA"
#~ msgstr "No FPU, SDRAMC, ROMC, UART, Timer, PCMCIA, LCD Controller, IrDA"

#~ msgid "No FPU, SDRAMC, ROMC, UART, Timer, PCMCIA, IrDA"
#~ msgstr "No FPU, SDRAMC, ROMC, UART, Timer, PCMCIA, IrDA"

#~ msgid "No FPU, SDRAMC, ROMC, DMAC, PCIC, UART, Timer"
#~ msgstr "No FPU, SDRAMC, ROMC, DMAC, PCIC, UART, Timer"

#~ msgid "2 10/100 Ethernet, 4 UART, IrDA, AC'97, I2C, 38 Digital I/O"
#~ msgstr "2 10/100 Ethernet, 4 UART, IrDA, AC'97, I2C, 38 Digital I/O"

#~ msgid "FPU, 32-bit external bus"
#~ msgstr "FPU, 32-bit external bus"

#~ msgid "FPU, 64-bit external bus"
#~ msgstr "FPU, 64-bit external bus"

#~ msgid "FPU, 64-bit external bus, external L2 cache"
#~ msgstr "FPU, 64-bit external bus, external L2 cache"

#~ msgid "256 L2 cache on die"
#~ msgstr "256 L2 cache on die"

#~ msgid "Mips 16"
#~ msgstr "Mips 16"

#~ msgid ""
#~ "Mips 16, RTC, Keyboard, TouchPanel, Audio, Compact-Flash, UART, Parallel"
#~ msgstr ""
#~ "Mips 16, RTC, Keyboard, TouchPanel, Audio, Compact-Flash, UART, Parallel"

#~ msgid "Mips 16, Compact Flash, UART, Parallel, RTC, Audio, PCIC"
#~ msgstr "Mips 16, Compact Flash, UART, Parallel, RTC, Audio, PCIC"

#~ msgid ""
#~ "Mips 16, LCD controller, Compact Flash, UART, Parallel, RTC, Keyboard, "
#~ "USB, Touchpad, Audio"
#~ msgstr ""
#~ "Mips 16, LCD controller, Compact Flash, UART, Parallel, RTC, Keyboard, "
#~ "USB, Touchpad, Audio"

#~ msgid "Debian for MIPS"
#~ msgstr "ديبيان لأنظمة MIPS"

#~ msgid "Debian for S/390"
#~ msgstr "دبيان لأنظمة S/390"

#~ msgid "Debian for Sparc64"
#~ msgstr "دبيان لأنظمة Sparc64"
