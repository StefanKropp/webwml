#use wml::debian::translation-check translation="fbe1364a65d5507c2b5e7690655892c887a863db" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Qualys Research Labs a découvert deux vulnérabilités dans libmount
d'util-linux. Ces défauts permettent à un utilisateur non privilégié de
démonter les systèmes de fichiers d'autres utilisateurs qui sont soit
eux-mêmes accessibles à tous en écriture, soit montés dans un répertoire
accessible à tous en écriture
(<a href="https://security-tracker.debian.org/tracker/CVE-2021-3996">\
CVE-2021-3996</a>), ou de démonter des systèmes de fichiers FUSE qui
appartiennent à certains autres utilisateurs
(<a href="https://security-tracker.debian.org/tracker/CVE-2021-3995">\
CVE-2021-3995</a>).</p>

<p>Pour la distribution stable (Bullseye), ces problèmes ont été corrigés
dans la version 2.36.1-8+deb11u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets util-linux.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de util-linux, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/util-linux">\
https://security-tracker.debian.org/tracker/util-linux</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5055.data"
# $Id: $
