#use wml::debian::translation-check translation="df11709715cec99dcc46d31e0498723d0062fe70" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Stephan Zeisberg a signalé une vulnérabilité d'écriture hors limites
dans la fonction _sasl_add_string() de cyrus-sasl2, une bibliothèque
implémentant « SASL », la couche simple d'authentification et de sécurité.
Un attaquant distant peut tirer avantage de ce problème pour provoquer des
conditions de déni de service pour les applications utilisant la
bibliothèque.</p>

<p>Pour la distribution oldstable (Stretch), ce problème a été corrigé dans
la version 2.1.27~101-g0780600+dfsg-3+deb9u1.</p>

<p>Pour la distribution stable (Buster), ce problème a été corrigé dans la
version 2.1.27+dfsg-1+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets cyrus-sasl2.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de cyrus-sasl2,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/cyrus-sasl2">\
https://security-tracker.debian.org/tracker/cyrus-sasl2</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4591.data"
# $Id: $
