#use wml::debian::translation-check translation="49d6d85a79874ccc5eece0c873e13c73f733ac07" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs problèmes ont été découverts dans le module Apache
auth_mellon qui fournit l'authentification SAML 2.0.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-3877">CVE-2019-3877</a>

<p>Il était possible de contourner la vérification de redirection d'URL
à la déconnexion, ainsi le module pouvait être utilisé comme une
fonctionnalité de redirection d'ouverture.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-3878">CVE-2019-3878</a>

<p>Lorsque mod_auth_mellon est utilisé dans une configuration d'Apache qui
sert de mandataire distant avec le module http_proxy, il était possible de
contourner l'authentification en envoyant des en-têtes SAML ECP.</p></li>

</ul>

<p>Pour la distribution stable (Stretch), ces problèmes ont été corrigés
dans la version 0.12.0-2+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libapache2-mod-auth-mellon.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de
libapache2-mod-auth-mellon, veuillez consulter sa page de suivi de sécurité
à l'adresse :
<a href="https://security-tracker.debian.org/tracker/libapache2-mod-auth-mellon">\
https://security-tracker.debian.org/tracker/libapache2-mod-auth-mellon</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4414.data"
# $Id: $
