#use wml::debian::template title="Assistance aux utilisateurs"
#use wml::debian::translation-check translation="2b215c6e7169b28ab15c0eb330ed620ef7b21647" maintainer="Jean-Paul Guillonneau"
#use wml::debian::recent_list

# Translators:
# Christophe Le Bars, 1998
# Pierre Machard, 2002
# Denis Barbier, 2002
# Norbert Bottlaender-Prier, 2003-2005
# Thomas Huriaux, 2005
# Mohammed Adnène Trojette, 2005
# Frédéric Bothamy, 2006
# Thomas Peteul, 2009
# David Prévot, 2011
# Jean-Paul Guillonneau, 2016-2021.

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<ul class="toc">
  <li><a href="#irc">IRC (aide en ligne en temps réel)</a></li>
  <li><a href="#mail_lists">Listes de diffusion</a></li>
  <li><a href="#usenet">Groupes de discussion Usenet</a></li>
  <li><a href="#forums">Forums des utilisateurs de Debian</a></li>
  <li><a href="#maintainers">Contacter les responsables des paquets Debian</a></li>
  <li><a href="#bts">Le système de suivi des bogues</a></li>
  <li><a href="#release">Problèmes connus</a></li>
</ul>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> L'assistance de Debian est
offerte par un groupe de bénévoles. Si l’assistance fournie par cette communauté
ne satisfait pas vos besoins et que vous ne trouvez pas de réponse dans notre
<a href="doc/">documentation</a>, vous pouvez embaucher un <a href="consultants/">consultant</a>
pour entretenir votre système Debian ou lui ajouter des fonctionnalités
supplémentaires.</p>
</aside>

<h2><a id="irc">Aide en ligne en temps réel à l’aide d’IRC</a></h2>

<p>
<a href="http://www.irchelp.org/">IRC</a> (Internet Relay Chat) est une
excellente manière de discuter en temps réel avec des personnes du monde
entier. C'est un système de discussion en mode texte pour la messagerie
instantanée. Sur IRC vous pouvez entrer dans des salons (appelés canaux) ou vous
pouvez discuter directement avec des personnes au moyen de messages privés.
</p>

<p>
Des canaux IRC dédiés à Debian sont disponibles sur
<a href="https://www.oftc.net/">OFTC</a>. Pour avoir la liste comlète des canaux
Debian, veuillez consulter notre <a href="https://wiki.debian.org/IRC">Wiki</a>.
Vous pouvez aussi utiliser un
<a href="https://netsplit.de/channels/index.en.php?net=oftc&chat=debian">moteur de recherche</a> 
pour rechercher les canaux relatifs à Debian.
</p>

<h3>Client IRC</h3>

<p>
Pour vous connecter au réseau IRC, vous pouvez soit utiliser l'interface
<a href="https://www.oftc.net/WebChat/">WebChat</a> d'OFTC dans votre navigateur
préféré ou installer un client sur votre ordinateur. Il y a beaucoup de clients
différents, certains avec une interface graphique d'autres pour la console.
Quelques-uns des clients IRC les plus populaires ont été empaquetés pour Debian,
par exemple :
</p>

<ul>
  <li><a href="https://packages.debian.org/stable/net/irssi">irssi</a> (en mode texte)</li>
  <li><a href="https://packages.debian.org/stable/net/weechat-curses">WeeChat</a> (en mode texte)</li>
  <li><a href="https://packages.debian.org/stable/net/hexchat">HexChat (GTK)</a></li>
  <li><a href="https://packages.debian.org/stable/net/konversation">Konversation</a> (KDE)</li>
</ul> 

<p>
Le Wiki de Debian offre une liste plus complète
<a href="https://wiki.debian.org/IrcClients">des clients IRC</a> qui sont
disponibles comme paquets Debian.
</p>

<h3>Se connecter au réseau</h3>

<p>
Une fois que vous avez installé un client, vous devez lui indiquer de se
connecter au serveur. Dans la plupart des clients, vous pouvez le faire en
tapant :
</p>

<pre>
/server irc.debian.org
</pre>

<p>Le nom d'hôte irc.debian.org est un alias pour irc.oftc.net. Avec quelques
clients (tels que irssi) vous devrez plutôt saisir ceci :</p>

<pre>
/connect irc.debian.org
</pre>

# Note to translators:
# You might want to insert here a paragraph stating which IRC channel is available
# for user support in your language and pointing to the English IRC channel.
# <p>Once you are connected, join channel <code>#debian-foo</code> by typing</p>
# <pre>/join #debian</pre>
# for support in your language.
# <p>For support in English, read on</p>

<h3>Rejoindre un canal</h3>

<p>Une fois connecté, rejoignez le canal <code>#debian-fr</code> en tapant :</p>

<pre>
/join #debian-fr
</pre>
<p>pour de l'assistance en français ou</p>

<pre>
/join #debian
</pre>
<p>pour de l'assistance en anglais</p>

<p>Note : les clients graphiques tels que HexChat ou Konversation ont souvent un
bouton ou une entrée de menu pour se connecter aux serveurs et rejoindre un
canal.
</p>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="https://wiki.debian.org/DebianIRC">Lire notre FAQ sur IRC</a></button></p>

<h2><a id="mail_lists">Listes de diffusion</a></h2>

<p>
Plus d'un millier de <a href="intro/people.en.html#devcont">développeurs</a>
actifs disséminés dans le monde entier travaillent sur Debian pendant leur temps
libre — et dans des fuseaux horaires différents. Par conséquent, nous
communiquons principalement par courriels. De la même façon, la plupart des
conversations entre les développeurs et les utilisateurs se tiennent sur
diverses <a href="MailingLists/">listes de diffusions</a>:
</p>

# Note to translators:
# You might want to adapt the following paragraph, stating which list
# is available for user support in your language instead of English.

<ul>
  <li>Pour l'assistance aux utilisateurs en français, veuillez contacter la
      <a href="https://lists.debian.org/debian-user-french/">liste de
      diffusion debian-user-french</a>.</li>
  <li>Pour l'assistance aux utilisateurs en anglais, veuillez contacter la
      <a href="https://lists.debian.org/debian-user/">liste de
      diffusion debian-user</a>.</li>
  <li>Pour l'assistance aux utilisateurs dans d'autres langues, veuillez
      consulter l'<a href="https://lists.debian.org/users.html">index des
      listes de diffusion pour les utilisateurs</a>.</li>
</ul>

<p>
Bien sûr, il existe aussi beaucoup d'autres listes de diffusion dédiées à
quelque aspect de l'écosystème Linux, non axées spécifiquement sur Debian.
Veuillez utiliser votre moteur de recherche favori pour trouver la liste qui
conviendra le mieux à votre requête.
</p>

<h2><a id="usenet">Groupes de discussion Usenet</a></h2>

<p>
Une grande majorité de nos <a href="#mail_lists">listes de diffusion</a> peuvent
être lues sous la forme de groupes de discussion, dans la hiérarchie
<kbd>linux.debian.*</kbd>. Pour un accès facile, ces groupes peuvent être lus à
l’aide d’une interface web telle que celle de
<a href="https://groups.google.com/forum/">Google Groups</a>.
</p>

<h2><a id="forums">Debian User Forums</h2>

# Note to translators:
# If there is a specific Debian forum for your language you might want to
# insert here a paragraph stating which list is available for user support
# in your language and pointing to the English forums.
# <p><a href="http://someforum.example.org/">someforum</a> is a web portal
# on which you can use your language to discuss Debian-related topics,
# submit questions about Debian, and have them answered by other users.</p>
#
# <p><a href="https://forums.debian.net">Debian User Forums</a> is a web portal
# on which you can use the English language to discuss Debian-related topics,
# submit questions about Debian, and have them answered by other users.</p>

<p>
<a href="https://forums.debian.net">Debian User Forums</a> est un portail sur le
web ou des milliers d'utilisateurs discutent de sujets relatifs à Debian, posent
des questions et s'entraident en répondant aux questions. Vous pouvez lire tous
les sujets traités sans avoir besoins de vous inscrire. Si vous voulez
participer aux discussions et publier des messages, vous devez vous inscrire et
vous connecter.
</p>

<p>
<a href="https://www.debian-fr.org/">debian-fr.org</a> et <a
href="https://debian-fr.xyz">debian-fr.xyz</a> sont les équivalents francophones
de debianHELP et Debian User Forums. Ce sont des portails où vous pouvez poser
vos questions à propos de Debian et obtenir les réponses des autres
utilisateurs. <a href="https://debian-facile.org">debian-facile.org</a> est un
site comportant de la documentation en français sous forme de wiki et un forum.
</p>

<h2><a id="maintainers">Contacter les responsables des paquets Debian</a></h2>

<p>
En gros, il y a deux manières courantes pour contacter le responsable d'un
paquet Debian :
</p>

<ul>
  <li>Si vous souhaitez rapporter un bogue, envoyez simplement un
      <a href="bts">rapport de bogue</a> ; le responsable du paquet en recevra
      une copie.</li>
  <li>Si vous désirez simplement communiquer avec le responsable, vous pouvez
      utiliser les alias spéciaux mis en place pour chaque paquet:<br>
      &lt;<em>nom du paquet</em>&gt;@packages.debian.org</li>
</ul>

<toc-add-entry name="doc" href="doc/">Documentation</toc-add-entry>

<p>Dans tous les systèmes d'exploitation, la documentation, les manuels
techniques qui décrivent le fonctionnement et l'utilisation de programmes
tiennent une place importante. Dans le cadre de ses efforts pour créer un
système d'exploitation libre de qualité élevé, le projet Debian fait tout son
possible pour fournir à tous ses utilisateurs une bonne documentation dans une
forme facilement accessible.</p>

<p>Consultez la <a href="doc/">page sur la documentation</a> pour voir la 
liste des manuels de Debian et d'autres documentations, y compris le Manuel
d'installation, la FAQ Debian et d'autres manuels pour les utilisateurs et
les développeurs.</p>


<toc-add-entry name="consultants" href="consultants/">Consultants</toc-add-entry>

<h2><a id="bts">Le système de suivi des bogues</a></h2>

<p>
La distribution Debian possède son propre <a href="Bugs/">système de suivi des bogues</a>
qui détaille les bogues rapportés par les utilisateurs et les développeurs.
Chaque bogue reçoit un numéro, et il reste fiché jusqu'à ce
qu'il soit marqué comme résolu. Il y a deux manière de rapporter un bogue :
</p>

<ul>
  <li>La manière recommandée est d'utiliser le paquet Debian <em>reportbug</em>.</li>
  <li>Autrement, vous pouvez envoyer un courriel comme cela est décrit sur cette
      <a href="Bugs/Reporting">page</a>.</li>
</ul>

<h2><a id="release">Problèmes connus</a></h2>

<p>Les limitations et, le cas échéant, les problèmes graves de la distribution
stable actuelle se trouvent décrits dans les pages de
<a href="releases/stable/">la version publiée</a>.</p>

<p>Référez-vous particulièrement aux
<a href="releases/stable/releasenotes">notes de publication</a> et aux
<a href="releases/stable/errata">errata</a>.</p>
