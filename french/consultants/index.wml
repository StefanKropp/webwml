#use wml::debian::template title="Conseillers" NOCOMMENTS="yes" GENTIME="yes"
#use wml::debian::translation-check translation="1776cd107720f975fba5147dfb40a13806ae3626" maintainer="Jean-Pierre Giraud"

# Translators:
# Christophe Le Bars, 1998-2000.
# Nicolas Bertolissio, 2003-2007.
# Jean-Pierre Giraud, 2014-2022

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Par courtoisie, Debian
fournit une liste de conseillers pour ses utilisateurs et ne soutient aucune des
personnes ou des entreprises présentées ci-dessous. Les présentations sont sous
la responsabilité exclusive du conseiller correspondant. Veuillez vous référer
à la <a href="info">page d'information des conseillers</a> pour connaître notre
politique d'ajout ou de mise à jours des informations de la liste.</p>
</aside>

<p>
Debian fait partie des logiciels libres et offre gratuitement de l'aide sur ses
diverses <a href="../MailingLists/">listes de diffusion</a>. Certaines
personnes n'ont pas le temps ou ont des besoins particuliers et souhaitent
engager quelqu'un, soit pour maintenir, soit pour ajouter de nouvelles
fonctionnalités à leur système Debian. Nous vous présentons une liste de
personnes, d'organisations et d'entreprises qui tirent au moins une partie de
leurs revenus en fournissant une aide <strong>payante</strong> pour Debian.
</p>

<p>
La liste est triée par pays, les noms sont listés dans chaque pays par ordre de
réception. Si votre pays n'est pas présent, regardez dans les pays voisins dans
la mesure où certains conseillers travaillent au niveau international ou à
distance.
</p> 

<p>Des listes supplémentaires de conseillers existent pour des utilisations
particulières de Debian :</p>

<ul>
<li><a href="https://wiki.debian.org/DebianEdu/Help/ProfessionalHelp">Debian Edu</a> :
utilisation de Debian dans les écoles, universités et autres cadres d’éducation ;
<li><a href="https://wiki.debian.org/LTS/Funding">Debian LTS</a> :
prise en charge de la sécurité à long terme de Debian.
</ul>

<hrline>

<h2>Pays dans lesquels des conseillers sur Debian sont présents :</h2>

#include "../../english/consultants/consultant.data"

<hrline>

# Gardé pour la compatibilité ascendante, au cas improbable où quelqu'un ferait
# référence à #policy

<h2><a name="policy"></a>Charte de la page des conseillers Debian</h2>
<p>Veuillez vous référer à la <a href="info">page d'information pour les
conseillers</a> afin de consulter notre charte pour ajouter ou mettre à jour
des informations.
</p>
