#use wml::debian::translation-check translation="bc8feadbbc686ad5f9ceb695925a329dea1622c0" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Deux vulnérabilités importantes ont été trouvées dans le système de gestion
de versions Mercurial, qui pourraient conduire à un attaque par injection
d’interpréteur et un écrasement de fichiers hors arbre.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-1000115">CVE-2017-1000115</a>

<p>L’évaluation de lien symbolique de Mercurial était incomplète avant 4.3, et
pourrait être dupée pour écrire des fichiers en dehors du dépôt.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-1000116">CVE-2017-1000116</a>

<p>Mercurial ne nettoyait pas les noms d’hôte passés à ssh, permettant une
attaque par injection d’interpréteur sur des clients en précisant un nom d’hôte
débutant par -oProxyCommand. Cette vulnérabilité est similaire à celles dans
Git (<a href="https://security-tracker.debian.org/tracker/CVE-2017-1000117">CVE-2017-1000117</a>)
et Subversion
(<a href="https://security-tracker.debian.org/tracker/CVE-2017-9800">CVE-2017-9800</a>).</p>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 2.2.2-4+deb7u5.</p>

<p>Nous vous recommandons de mettre à jour vos paquets mercurial.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1072.data"
# $Id: $
