#use wml::debian::translation-check translation="16a228d71674819599fa1d0027d1603056286470" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-3136">CVE-2017-3136</a>

<p>Oleg Gorokhov de Yandex a découvert que BIND ne gère pas correctement
certaines requêtes quand DNS64 est utilisé avec l'option « break-dnssec
yes; », permettant à un attaquant distant de provoquer un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-3137">CVE-2017-3137</a>

<p>BIND fait des hypothèses incorrectes sur l'ordre des enregistrements dans
la section réponse d'une réponse contenant des enregistrements de ressources
CNAME ou DNAME, menant à des situations où BIND quitte avec un échec
d'assertion. Un attaquant peut tirer avantage d'une telle situation pour
provoquer un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-3138">CVE-2017-3138</a>

<p>Mike Lalumiere de Dyn, Inc. a découvert que BIND peut quitter avec un
échec d'assertion REQUIRE s'il reçoit une chaîne de commande NULL sur son
canal de contrôle. Notez que ce correctif est seulement appliqué à Debian
comme mesure de renforcement. Des détails sur le problème peuvent être
trouvés à l'adresse
<a href="https://kb.isc.org/article/AA-01471">https://kb.isc.org/article/AA-01471</a> .</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 1:9.8.4.dfsg.P1-6+nmu2+deb7u16.</p>

<p>Nous vous recommandons de mettre à jour vos paquets bind9.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-957.data"
# $Id: $
