#use wml::debian::translation-check translation="1d1c1ba842e225bf68a6fed5744786cc779234f7" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>John Lightsey et Todd Rinaldo ont signalé que le chargement opportuniste
de modules optionnels peut provoquer le chargement involontaire de code par
de nombreux programmes à partir du répertoire de travail courant (qui
pourrait être changé pour un autre répertoire sans que l'utilisateur s'en
rende compte) et éventuellement mener à une élévation de privilèges, comme
cela a été démontré dans Debian avec certaines combinaisons de paquets
installés.</p>

<p>Le problème est lié au chargement de modules par Perl à partir du
tableau de répertoires « includes » (« @INC ») dans lequel le dernier
élément est le répertoire courant (« . »). Cela signifie que, quand
<q>perl</q> souhaite charger un module (lors d'une première compilation ou
du chargement différé d'un module durant l'exécution), Perl cherche
finalement le module dans le répertoire courant, dans la mesure où « . »
est le dernier répertoire inclus dans son tableau de répertoires inclus
à explorer. Le problème vient de la demande de bibliothèques qui sont dans
« . » mais qui ne sont pas autrement installées.</p>

<p>Avec cette mise à jour, le module Sys::Syslog de Perl est mis à jour
pour ne pas charger de modules à partir du répertoire courant.</p>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 0.29-1+deb7u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libsys-syslog-perl.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-584.data"
# $Id: $
