#use wml::debian::translation-check translation="e4abcc6d77492bfee8e493c0eac14a72ac91ced7" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>debian-security-support, le vérificateur de couverture de sécurité de Debian,
a été mis à jour dans Jessie.</p>

<p>Cela marque la fin de vie du paquet Enigmail dans Jessie. Après de nombreux
mois de travail pour rétroporter les diverses modifications et corrections
nécessaires pour assurer un paquet Enigmail sécurisé, compatible avec la
nouvelle version de Thunderbird maintenant fournie avec Jessie, l’équipe LTS
pense que les modifications sont trop importantes pour garantir la stabilité de
la distribution dans son entier.</p>

<p>Bien que Enigmail soit toujours disponible sur addons.mozilla.org, nous ne
recommandons pas que ce paquet soit utilisé, car il télécharge et exécute des
binaires arbitraires sans que l’utilisateur le sache. Il ne respecte pas les
engagements de Debian à propos du logiciel libre, car il n’est pas
constructible à partir du code source.</p>

<p>Nous recommandons plutôt de mettre à niveau les stations de travail ayant
besoin d’Enigmail vers Stretch de Debian où les mises à jour sont disponibles.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>

<p>Pour Debian 8 <q>Jessie</q>, ce problème a été corrigé dans la
version 2019.02.01~deb8u1 de debian-security-support.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1657.data"
# $Id: $
