#use wml::debian::translation-check translation="16ffa8223d9a8de1ec4e5dabe84aede52bacede8" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités de sécurité ont été découvertes dans Zabbix, une
solution de surveillance réseau pour serveurs et clients.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-10742">CVE-2016-10742</a>

<p>Zabbix permettait à des attaquants distants de rediriger vers des liens
externes en utilisant à mauvais escient le paramètre de requête.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-2826">CVE-2017-2826</a>

<p>Une vulnérabilité de divulgation d'informations existe dans la requête de
mandataire iConfig du serveur Zabbix. Une requête de mandataire iConfig
spécialement contrefaite peut faire que le serveur Zabbix envoie les
informations de configuration de n’importe quel mandataire Zabbix, aboutissant
à une divulgation d'informations. Un attaquant peut faire des requêtes à partir
d’un mandataire Zabbix actif pour déclencher cette vulnérabilité.</p></li>

</ul>

<p>Cette mise à jour inclut aussi plusieurs corrections de bogue et
améliorations. Pour plus d’informations, veuillez vous référer au journal de
modifications de l’amont.</p>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans la version 1:2.2.23+dfsg-0+deb8u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets zabbix.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a rel="nofollow
"href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1708.data"
# $Id: $
