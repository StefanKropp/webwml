#use wml::debian::translation-check translation="c4a1c2bdc93f45db86669b5a8ce1e0f17a23954d" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Plusieurs problèmes ont été découverts dans exiv2, un outil de manipulation
de métadonnées EXIF/IPTC/XMP.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-17581">CVE-2018-17581</a>

<p>Un dépassement de pile dû à un appel de fonction récursif causant une
consommation excessive de pile pourrait aboutir à un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-19107">CVE-2018-19107</a>

<p>Une lecture hors limites de tampon basé sur le tas causée par un dépassement
d'entier pourrait aboutir à un déni de service à l'aide d'un fichier contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-19108">CVE-2018-19108</a>

<p>Il pourrait exister une boucle infinie dans une fonction pouvant être
activée par une image contrefaite.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-19535">CVE-2018-19535</a>

<p>Une lecture hors limites de tampon basé sur le tas pourrait aboutir dans un
déni de service à l'aide d'un fichier contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20097">CVE-2018-20097</a>

<p>Une image contrefaite pourrait aboutir à un déni de service.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans la
 version 0.24-4.1+deb8u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets exiv2.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a rel="nofollow
"href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1691.data"
# $Id: $
