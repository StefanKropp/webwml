#use wml::debian::translation-check translation="b08f68b897413bf34ee9763b844cd7d5ef71a35d" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans le noyau Linux qui
pourraient conduire à une élévation des privilèges, un déni de service, ou une
fuite d'informations.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-12207">CVE-2018-12207</a>

<p>Sur les processeurs Intel prenant en charge la virtualisation matérielle avec
des tables de pagination (« Extended Page Tables » – EPT), une machine virtuelle
peut manipuler la gestion de mémoire matérielle pour provoquer une erreur de
« Machine Check » (MCE) et un déni de service (blocage ou plantage).</p>

<p>Le client déclenche cette erreur en modifiant les tables de pagination sans
vidage de la TLB, si bien qu'à la fois des entrées de 4 Ko et 2 Mo sont chargées
pour la même adresse virtuelle dans l'instruction TLB (iTLB). Cette mise à jour
implémente une mitigation dans KVM qui empêche les machines virtuelles clientes
de charger des entrées de 2 Mo dans iTLB. Cela réduira la performance des
machines virtuelles clientes.</p>

<p>Plus d'informations sur la mitigation sont disponibles sur la page
<url "https://www.kernel.org/doc/html/latest/admin-guide/hw-vuln/multihit.html">
ou dans le paquet linux-doc-4.9.</p>

<p>L'explication par Intel de ce problème est disponible sur la page
<a href="https://software.intel.com/security-software-guidance/insights/deep-dive-machine-check-error-avoidance-page-size-change-0">software.intel.com/…/deep-dive-machine-check-error-avoidance-page-size-change-0</a>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-0154">CVE-2019-0154</a>

<p>Intel a découvert que leurs processeurs graphiques de huitième et neuvième
génération, lorsqu'ils lisent certains registres tandis que le processeur
graphique est dans un état de faible puissance, peuvent provoquer un blocage
système. Un utilisateur local autorisé à utiliser le processeur graphique peut
se servir de cela pour un déni de service.</p>

<p>Cette mise à jour atténue le problème grâce à des modifications dans le
pilote des cartes graphiques i915.</p>

<p>Les puces affectées (de huitième et neuvième génération) sont listées sur la
page <a href="https://en.wikipedia.org/wiki/List_of_Intel_graphics_processing_units#Gen8">en.wikipedia.org/wiki/List_of_Intel_graphics_processing_units</a>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-0155">CVE-2019-0155</a>

<p>Intel a découvert que les processeurs graphiques de neuvième génération et
plus récents manquent de vérification de sécurité dans le « Blitter Command
Streamer » (BCS). Un utilisateur local autorisé à utiliser le processeur
graphique pourrait se servir de cela pour accéder à toute la mémoire à laquelle
le processeur graphique a accès, ce qui pourrait avoir pour conséquence un déni
de service (corruption de mémoire ou plantage), la divulgation d'informations
sensibles ou une élévation des privilèges.</p>

<p>Cette mise à jour atténue le problème en ajoutant une vérification de
sécurité au pilote i915.</p>

<p>Les puces affectées (neuvième génération et plus) sont listées sur la page
<a href="https://en.wikipedia.org/wiki/List_of_Intel_graphics_processing_units#Gen9">en.wikipedia.org/wiki/List_of_Intel_graphics_processing_units</a>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11135">CVE-2019-11135</a>

<p>Sur les processeurs Intel prenant en charge la mémoire transactionnelle
(TSX), une transaction qui va être interrompue peut être poursuivie pour une
exécution spéculative, lisant des données sensibles à partir des tampons
internes et les divulguant à travers les opérations dépendantes. Intel appelle
cela <q>TSX Asynchronous Abort</q> (TAA).</p>

<p>Pour les processeurs affectés par les problèmes « Microarchitectural Data
Sampling (MDS) » précédemment publiés
(<a href="https://security-tracker.debian.org/tracker/CVE-2018-12126">CVE-2018-12126</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2018-12127">CVE-2018-12127</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2018-12130">CVE-2018-12130</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2019-11091">CVE-2019-11091</a>),
la mitigation existante atténue aussi ce problème.</p>

<p>Pour les processeurs qui sont vulnérables aux TAA mais pas aux MDS, cette
mise à jour désactive TSX par défaut. Cette mitigation requiert un microcode
de processeur mis à jour. Un paquet mis à jour du paquet intel-microcode (disponible
seulement dans Debian non-free) sera fournit par une DSA future. Le microcode
de processeur mis à jour peut aussi être disponible dans le cadre de la mise
à jour du microprogramme du système (« BIOS »).</p>

<p>Plus d'informations sur la mitigation sont disponibles sur la page
<a href="https://www.kernel.org/doc/html/latest/admin-guide/hw-vuln/tsx_async_abort.html">www.kernel.org/…/tsx_async_abort.html</a>
ou dans le paquet linux-doc-4.9.</p>

<p>L'explication par Intel de ce problème est disponible sur la page
   <a href="https://software.intel.com/security-software-guidance/insights/deep-dive-intel-transactional-synchronization-extensions-intel-tsx-asynchronous-abort">software.intel.com/…/deep-dive-intel-transactional-synchronization-extensions-intel-tsx-asynchronous-abort</a>.</p></li>


</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 4.9.189-3+deb9u2~deb8u1.</p>
<p>Nous vous recommandons de mettre à jour vos paquets linux-4.9.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1990.data"
# $Id: $
