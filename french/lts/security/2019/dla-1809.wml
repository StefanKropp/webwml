#use wml::debian::translation-check translation="8809444c37f315458d8fb702e9dbe557531ad413" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Deux autres problèmes de sécurité ont été corrigés dans plusieurs
démultiplexeurs et décodeurs de libav, une bibliothèque multimédia.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-15822">CVE-2018-15822</a>

<p>La fonction flv_write_paquet dans libavformat/flvenc.c dans libav ne
 vérifiait pas si le paquet audio était vide, conduisant à un échec d’assertion.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11338">CVE-2019-11338</a>

<p>libavcodec/hevcdec.c dans libav gérait incorrectement la détection de
premières découpes (slices) dupliquées. Cela permettait à des attaquants
de provoquer un déni de service (déréférencement de pointeur NULL et accès hors
tableau) ou éventuellement d’avoir un impact non précisé à l’aide de données
HEVC contrefaites.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 6:11.12-1~deb8u7.</p>
<p>Nous vous recommandons de mettre à jour vos paquets libav.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1809.data"
# $Id: $
