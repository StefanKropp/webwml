#use wml::debian::translation-check translation="ab3be4ee01879fd4484c795bbaa824377c218575" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Des chercheurs en sécurité ont identifié deux méthodes d’analyse logicielle
qui, si utilisées dans des buts malveillants, ont le potentiel de rassembler
improprement des données sensibles à partir de plusieurs types de périphériques
informatiques avec des processeurs de fournisseurs différents et de systèmes
d’exploitation différents.</p>

<p>Cette mise à jour nécessite une mise à jour du paquet intel-microcode, qui
n’est pas libre. Les utilisateurs ayant déjà installé la version de
Jessie-backports-sloppy n’ont nul besoin de mettre à niveau.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-3639">CVE-2018-3639</a>

<p>Speculative Store Bypass (SSB) – aussi connu comme Variante 4</p>

<p>Les systèmes avec des microprocesseurs utilisant l’exécution spéculative et
l’exécution spéculative de lectures de mémoire avant que les adresses de toutes
les écritures en mémoire antérieures soient connues, pourraient permettre une
divulgation non autorisée d’informations à un attaquant avec un accès
utilisateur local à l'aide d'une analyse par canal auxiliaire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-3640">CVE-2018-3640</a>

<p>Rogue System Register Read (RSRE) – aussi connu comme Variante 3a</p>

<p>Les systèmes avec des microprocesseurs utilisant l’exécution spéculative et
qui réalisent des lectures spéculatives de registres du système pourraient
permettre une divulgation non autorisée de paramètres du système à un attaquant
avec un accès utilisateur local à l'aide d'une analyse par canal auxiliaire.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 3.20180703.2~deb8u1.</p>
<p>Nous vous recommandons de mettre à jour vos paquets intel-microcode.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify le following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1446.data"
# $Id: $
