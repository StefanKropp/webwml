#use wml::debian::translation-check translation="d7165539bc3d665b54cc1d95d430295cb1b53450" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités de sécurité ont été découvertes dans Wireshark,
un analyseur de trafic réseau. Un attaquant pouvait provoquer un déni de
service (boucle infinie ou plantage d'application) au moyen d'une injection
de paquets ou d'un fichier de capture contrefait. Le traitement incorrect
d'URL dans Wireshark pouvait aussi permettre l'exécution de code à
distance. Un double-clic n'ouvrira plus désormais l'URL dans les fichiers
pcap(ng), mais plutôt le copiera dans le presse-papier où il pourra être
inspecté et collé dans la barre d'adresse du navigateur.</p>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans la
version 2.6.20-0+deb9u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets wireshark.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de wireshark, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/wireshark">\
https://security-tracker.debian.org/tracker/wireshark</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2967.data"
# $Id: $
