#use wml::debian::translation-check translation="7b12e565ad73f9aed0a2abbbf9852252f4b053e2" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Le serveur web HTTP 1.1 Twisted, situé dans le module twisted.web.http,
analysait plusieurs constructions de requêtes HTTP avec plus d'indulgence
que ce qui est permis par la RFC 7230. Cette analyse non conforme peut
conduire à une désynchronisation si les requêtes passent à travers
plusieurs analyseurs HTTP, avec éventuellement pour conséquence une
dissimulation de requête HTTP.</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans la version
16.6.0-2+deb9u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets twisted.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de twisted, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/twisted">
https://security-tracker.debian.org/tracker/twisted</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2991.data"
# $Id: $
