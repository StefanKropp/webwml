#use wml::debian::translation-check translation="b408b21b7691f0759a7feee6dd4473187e442136" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Tobias Stoeckmann a trouvé un problème de dépassement d'entier dans JSON-C,
une bibliothèque C pour manipuler des objets JSON, lors de la lecture de gros fichiers conçus
de manière malveillante. Le problème pouvait être exploité pour provoquer un
déni de service ou éventuellement pour exécuter du code arbitraire.</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans
la version 0.12.1-1.1+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets json-c.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de json-c, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/json-c">https://security-tracker.debian.org/tracker/json-c</a></p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2301.data"
# $Id: $
