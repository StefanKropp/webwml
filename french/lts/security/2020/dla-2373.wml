#use wml::debian::translation-check translation="71d7f5cc1d067152b9cc75428d4e67331644ee75" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Les problèmes de sécurité suivants ont été découverts dans qemu et pourraient
éventuellement aboutir à un déni de service et à une exécution de code arbitraire.</p>

<ul>
<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-1711">CVE-2020-1711</a>

<p>Un défaut d’accès hors limites de tampon de tas a été découvert dans la façon
dont le pilote de blocs iSCSI dans QEMU gérait une réponse provenant d’un
serveur iSCSI lors de la vérification de l’état d’un LBA (Logical Address Block)
dans une routine iscsi_co_block_status(). Un utilisateur distant pourrait
utiliser cela pour planter le processus QEMU, aboutissant à un déni de service
ou à une exécution potentielle de code arbitraire avec les privilèges du
processus QEMU sur l’hôte.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-13253">CVE-2020-13253</a>

<p>Un problème d’accès en lecture hors limites a été découvert dans l’émulateur
de QEMU de la carte mémoire SD. Il survient lors de la réalisation de commandes
d’écriture de blocs au travers de sdhci_write() si un utilisateur client avait
envoyé une <q>address</q> qui est une gestion OOB de « s->wp_groups ». Un
utilisateur ou processus client pourrait utiliser ce défaut pour planter le
processus QEMU aboutissant à un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-14364">CVE-2020-14364</a>

<p>Un problème d’accès en lecture ou écriture hors limites a été découvert dans
l’émulateur USB de QEMU. Il survient lors du traitement de paquets USB d’un client,
quand « USBDevice->setup_len » excède « USBDevice->data_buf[4096] » dans les
routines do_token_{in,out}.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-16092">CVE-2020-16092</a>

<p>Une échec d’assertion pourrait survenir dans le traitement de paquets réseau.
Ce problème affecte les périphériques réseau e1000e et vmxnet3. Un utilisateur
ou processus client malveillant pourrait utiliser ce défaut pour interrompre le
processus QEMU sur l’hôte, aboutissant à une condition de déni de service dans
net_tx_pkt_add_raw_fragment dans hw/net/net_tx_pkt.c</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 1:2.8+dfsg-6+deb9u11.</p>

<p>Nous vous recommandons de mettre à jour vos paquets qemu.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de qemu, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/qemu">https://security-tracker.debian.org/tracker/qemu</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2373.data"
# $Id: $
