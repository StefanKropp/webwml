#use wml::debian::translation-check translation="65d69824b30263dfa47ea1fe7edb3ed314d9e5bb" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans Samba, un serveur de
fichiers SMB/CIFS, d’impression et de connexion pour Unix.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10218">CVE-2019-10218</a>

<p>Un défaut a été découvert dans le client samba par lequel un serveur
malveillant pouvait fournir un nom de chemin avec séparateurs au client. Cela
pourrait permettre d’accéder à des fichiers et des répertoires en dehors des
noms de chemin réseau SMB. Un attaquant pourrait utiliser cette vulnérabilité
pour créer des fichiers en dehors du répertoire de travail en cours en utilisant
les privilèges de l’utilisateur client.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-14833">CVE-2019-14833</a>

<p>Un défaut a été découvert dans la manière dont Samba gére les
modifications de mot de passe utilisateur ou un nouveau mot de passe pour un
utilisateur de samba. Le contrôleur de domaine Active Directory peut être
configuré pour utiliser un script personnalisé de vérification de complexité
de mot de passe. Cette configuration de complexité peut échouer quand des
caractères non ASCII sont utilisés dans le mot de passe. Cela pourrait conduire
à ce que des mots de passe faibles soient définis pour des utilisateurs de samba,
les rendant vulnérables à des attaques par dictionnaire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-14847">CVE-2019-14847</a>

<p>Un défaut a été découvert dans samba par lequel un attaquant peut planter
un serveur LDAP AC DC à l’aide de dirsync, aboutissant à un déni de service. Une
élévation des privilèges n’est pas possible avec ce problème.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-14861">CVE-2019-14861</a>

<p>Samba a un problème par lequel un enchaînement dnsserver RPC fournit des
moyens administratifs de modifier des enregistrements et des zones DNS. Samba,
agissant comme AD DC, stocke les enregistrements DNS dans LDAP. Dans AD, les
permissions par défaut pour la partition DNS permettent la création de nouveaux
enregistrements par des utilisateurs authentifiés. Cela est utilisé, par
exemple, pour permettre à des machines de s’auto-enregistrer comme DNS. Si un
enregistrement DNS était créé et correspondait indépendamment de la casse au
nom de la zone, les routines ldb_qsort() et dns_name_compare() pourraient être
induites en erreur dans la lecture de la mémoire avant la liste d’entrées DNS
lors de la réponse à DnssrvEnumRecords() ou DnssrvEnumRecords2() et ainsi suivre
une mémoire non valable comme pointeur.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-14870">CVE-2019-14870</a>

<p>Samba a un problème par lequel le modèle de délégation S4U (MS-SFU) de
Kerberos inclut une fonctionnalité permettant à un sous-ensemble de clients de
refuser la délégation imposée de toute façon, que ce soit l’authentification
S4U2Self ou normale de Kerberos, en obligeant tous les tickets pour ces clients
a être non retransmissibles. Dans AD, cela est mis en œuvre par un attribut
d’utilisateur delegation_not_allowed (c’est-à-dire pas de délégation), qui
se traduit par disallow-forwardable. Cependant l’AC DC de Samba ne réalise pas
cela pour S4U2Self et ne règle pas l’indicateur forwardable même si le client
usurpateur n’a pas l’indicateur not-delegated défini.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-14902">CVE-2019-14902</a>

<p>Il existe un problème dans samba par lequel la suppression du droit de créer
ou modifier un sous-arbre ne serait pas automatiquement appliquée à tous les
contrôleurs de domaine.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-14907">CVE-2019-14907</a>

<p>Samba a un problème lorsqu’il est réglé avec <q> log level = 3 </q> (ou
supérieur), alors la chaîne obtenue du client, après un échec de conversion de
caractères, est affichée. De telles chaînes peuvent être fournies durant
l’échange d’authentification NTLMSSP. En particulier dans l’AD DC de samba, cela
peut déclencher la fin d’un processus à long terme (tel un serveur RPC). (Dans
le cas d’un serveur de fichiers, la cible la plus probable, smbd, opère selon un
processus par client, aussi un plantage ici est bénin).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-20254">CVE-2021-20254</a>

<p>Un défaut a été découvert dans samba. Le système de fichiers smbd doit mapper
les identités de groupes Windows (SID) dans des identifiants de groupe Unix
(GID). Le code réalisant cela a un défaut qui pourrait permettre de lire des
données après la fin du tableau dans le cas où une entrée de cache négative a
été ajoutée au cache de mappage. Cela pourrait faire que le code appelant
renvoie ces valeurs dans le jeton du processus qui stocke l’appartenance de
groupe d’un utilisateur. Le plus grand risque de cette vulnérabilité concerne
la confidentialité et l’intégrité.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 2:4.5.16+dfsg-1+deb9u4.</p>

<p>Nous vous recommandons de mettre à jour vos paquets samba.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de samba, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/samba">\
https://security-tracker.debian.org/tracker/samba</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2668.data"
# $Id: $
