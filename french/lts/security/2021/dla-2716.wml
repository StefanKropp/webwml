#use wml::debian::translation-check translation="ecb20758207f841ee5fff480a1d6f90cface4cd9" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans pillow (Python Imaging
Library — PIL).</p>

<p>Paquets binaires affectés :</p>

<p>python-imaging
python-pil-dbg
python-pil-doc
python-pil.imagetk-dbg
python-pil.imagetk
python-pil
python3-pil-dbg
python3-pil.imagetk-dbg
python3-pil.imagetk
python3-pil</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-35653">CVE-2020-35653</a>

<p>Pillow jusqu’à la version 8.2.0 et PIL (c’est-à-dire Python Imaging Library)
jusqu’à la version 1.1.7 permettent à un attaquant de passer des paramètres
contrôlés, directement dans une fonction de conversion pour déclencher un
dépassement de tampon dans Convert.c.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-25290">CVE-2021-25290</a>

<p>Un problème a été découvert dans Pillow avant la version 8.1.1. Dans
TiffDecode.c, il existe un « memcpy » avec un décalage négatif d’une taille
non autorisée.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-28676">CVE-2021-28676</a>

<p>Un problème a été découvert dans Pillow avant la version 8.2.0. Pour des
données FLI, FliDecode ne vérifie pas correctement qu’une progression de bloc
ne vaut pas zéro, conduisant éventuellement à une boucle infinie lors du
chargement.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-28677">CVE-2021-28677</a>

<p>Un problème a été découvert dans Pillow avant la version 8.2.0. Pour des
données EPS, l’implémentation de readline utilisée dans EPSImageFile doit gérer
n’importe quel combinaison de \r et \n comme fin de ligne. Il utilisait
malencontreusement une méthode quadratique d’accumulation de lignes lors de la
recherche de fin de ligne. Un fichier EPS malveillant pourrait utiliser cela
pour réaliser un déni de service de Pillow dans la phase d’ouverture, avant
qu’une image soit acceptée pour une ouverture.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-34552">CVE-2021-34552</a>

<p>Pillow jusqu’à la version 8.2.0 et PIL (c’est-à-dire Python Imaging Library)
jusqu’à la version 1.1.7 permettent à un attaquant de passer les paramètres
contrôlés, directement dans une fonction de conversion pour déclencher un
dépassement de tampon dans Convert.c.</p></li>

</ul>

<p>Pour Debian 9 « Stretch », ces problèmes ont été corrigés dans
la version 4.0.0-4+deb9u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets pillow.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de pillow, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/pillow">\
https://security-tracker.debian.org/tracker/pillow</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2716.data"
# $Id: $
