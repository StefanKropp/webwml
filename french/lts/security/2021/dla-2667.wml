#use wml::debian::translation-check translation="2c5894cdf675f55ce511f26a617c7b7f4691d12a" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans djvulibre, une bibliothèque
et un ensemble d’outils pour gérer les documents au format DjVu. Un attaquant
pourrait provoquer le plantage des visualisateurs de document et éventuellement
exécuter du code arbitraire à l’aide de fichiers DjVu contrefaits.</p>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 3.5.27.1-7+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets djvulibre.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de djvulibre, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/djvulibre">\
https://security-tracker.debian.org/tracker/djvulibre</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2667.data"
# $Id: $
