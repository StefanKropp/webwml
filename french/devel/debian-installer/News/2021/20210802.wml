#use wml::debian::translation-check translation="d508253ec6166b5a7c8280ceb8e0c3deae9624ff" maintainer="Jean-Pierre Giraud"
<define-tag pagetitle>Publication de l'installateur Debian Bullseye RC 3</define-tag>
<define-tag release_date>2021-08-02</define-tag>
#use wml::debian::news

<p>
L'<a href="https://wiki.debian.org/DebianInstaller/Team">équipe</a> du
programme d'installation de Debian a le plaisir d'annoncer la parution
de la troisième version candidate pour Debian 11 <q>Bullseye</q>.
</p>

<p>
Commençons par quelques nouvelles concernant la prise en charge du microcode
(<a href="https://bugs.debian.org/989863">nº 989863</a>) :
</p>

<ul>
  <li>Nous ne souhaitons pas laisser les utilisateurs dans le brouillard si
    le système installé ne s'amorce pas correctement (par exemple avec un écran
    noir ou un affichage brouillé). Nous avons 
    <a href="https://www.debian.org/releases/bullseye/amd64/ch02s02">mentionné cette possibilité</a>
    dans le guide d'installation et
    <a href="https://www.debian.org/releases/bullseye/amd64/ch06s04#completing-installed-system">listé quelques solutions</a>
    qui pourraient aider à se connecter quand même.</li>
  <li>Nous avons aussi
    <a href="https://www.debian.org/releases/bullseye/amd64/ch06s04#completing-installed-system">documenté une procédure basée sur isenkram</a>
    qui permet aux utilisateurs de détecter et de corriger l'absence de
    microcode dans leur système de façon automatique. Bien sûr, il faut peser
    le pour et le contre de l'utilisation de cet outil dans la mesure où il
    est probable qu'il nécessitera l'installation de paquets non libres.</li>
</ul>

<p>
  En complément à ces efforts de documentation, les
  <a href="https://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/">images d'installation non libres qui incluent les paquets de microcode</a>
  ont été améliorées pour qu'elles puissent anticiper le besoin de microcode
  sur la machine installée (par exemple, pour les cartes graphiques AMD ou
  Nvidia ou pour les dernières générations de matériel audio d'Intel).
</p>


<h2>Améliorations dans cette version de l'installateur</h2>

<ul>
  <li>cdrom-detect :
    <ul>
      <li>indication à hw-detect de ne pas lancer d'avertissement ni d'invite
        à propos de l'absence de microcode : la découverte et le montage de
        l'image ISO se produit si tôt que rien ne pourrait être fait après la
        découverte de l'absence de microcode à cette étape de toute façon
        (<a href="https://bugs.debian.org/991587">nº 991587</a>).</li>
    </ul>
  </li>
  <li>hw-detect :
    <ul>
      <li>ajout de la prise en charge de l'installation de microcode
        supplémentaire si des objets dans la base de données d'udev
        correspondent à des motifs basés sur modalias, basés sur les
        métadonnées de DEP-11 (<a href="https://bugs.debian.org/989863">nº 989863</a>) ;</li>
      <li>rechargement assuré du module approprié du noyau après introduction
        d'un microcode : il peut y avoir une certaine discordance comme cela
        apparaît avec <code>rtw_8821ce</code> à la place de <code>rtw88_8821ce</code>
        (<a href="https://bugs.debian.org/973733">nº 973733</a>) ;</li>
      <li>amélioration de la journalisation relative au microcode en retirant
        du code obsolète, des redondances et en adaptant les chaînes de
        formatage ;</li>
      <li>ajout de la prise en charge de l'absence de prise en compte de
        certains fichiers particuliers de microcode qui pourraient n'être ni
        effectivement requis ni empaquetés ; démarrage avec
        <code>iwl-debug-yoyo.bin</code>, requis par <code>iwlwifi</code> (<a href="https://bugs.debian.org/969264">nº 969264</a>,
        <a href="https://bugs.debian.org/966218">nº 966218</a>) ;</li>
      <li>possibilité pour les appelants de désactiver les avertissements et
        les invites sur l'absence de microcode.</li>
    </ul>
  </li>
  <li>debian-cd :
    <ul>
      <li>ajout de brltty et espeakup à toutes les images à partir de
        l'image d'installation par le réseau (<a href="https://bugs.debian.org/678065">nº 678065</a>) ;</li>
      <li>génération des métadonnées du microcode à partir des métadonnées
        d'AppStream/DEP-11, utilisées lors de la construction d'un arbre des
        medias (<a href="https://bugs.debian.org/989863">nº 989863</a>).</li>
    </ul>
  </li>
  <li>debian-installer :
    <ul>
      <li>passage de l'ABI du noyau Linux à la version 5.10.0-8.</li>
    </ul>
  </li>
  <li>pcmciautils :
    <ul>
      <li>mise à jour du répertoire <code>/etc/pcmciautils/</code> pour
        <code>/etc/pcmcia/</code> dans le paquet udeb
        (<a href="https://bugs.debian.org/980271">nº 980271</a>).</li>
    </ul>
  </li>
  <li>udpkg :
    <ul>
      <li>ajout du verrouillage du fichier d'état pour éviter les échecs à
        la première étape de l'installation lorsque plusieurs consoles sont
        impliquées (<a href="https://bugs.debian.org/987368">nº 987368</a>).</li>
    </ul>
  </li>
</ul>


<h2>Modifications de la prise en charge matérielle</h2>


<ul>
  <li>linux :
    <ul>
      <li>arm64 : ajout de pwm-rockchip au paquet udeb fb-modules ;</li>
      <li>arm64 : ajout de fusb302, tcpm et typec au paquet udeb usb-modules ;</li>
      <li>armhf : correction de la détection réseau sur diverses cartes i.MX6
        (<a href="https://bugs.debian.org/982270">nº 982270</a>) ;</li>
      <li>armhf : ajout de mdio-aspeed à nic-modules (<a href="https://bugs.debian.org/991262">nº 991262</a>) ;</li>
      <li>s390x : correction du nom de console pour correspondre au
        périphérique (<a href="https://bugs.debian.org/961056">nº 961056</a>).</li>
    </ul>
  </li>
</ul>


<h2>État de la localisation</h2>

<ul>
  <li>78 langues sont prises en charge dans cette version.</li>
  <li>La traduction est complète pour 36 de ces langues.</li>
</ul>


<h2>Problèmes connus dans cette version</h2>

<p>
Veuillez consulter les <a href="$(DEVEL)/debian-installer/errata">errata</a>
pour plus de détails et une liste complète des problèmes connus.
</p>


<h2>Retours d'expérience pour cette version</h2>

<p>
Nous avons besoin de votre aide pour trouver des bogues et améliorer encore
l'installateur, merci de l'essayer. Les CD, les autres supports d'installation,
et tout ce dont vous pouvez avoir besoin sont disponibles sur notre
<a href="$(DEVEL)/debian-installer">site web</a>.
</p>


<h2>Remerciements</h2>

<p>
L'équipe du programme d'installation Debian remercie toutes les personnes ayant
pris part à cette publication.
</p>
