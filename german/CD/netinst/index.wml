#use wml::debian::cdimage title="Netzwerkinstallation von einer Minimal-CD"
#use wml::debian::release_info
#use wml::debian::installer
#include "$(ENGLISHDIR)/releases/images.data"
#use wml::debian::translation-check translation="e01f641181acd20b6afc24e482e4360c075e4712"
# $Id$
# Translator: Gerfried Fuchs <alfie@debian.org> 2002-01-19
# Updated: Holger Wansing <linux@wansing-online.de>, 2012, 2013, 2015, 2016.
# Updated: Holger Wansing <hwansing@mailbox.org>, 2021.

<p>Eine <q>Netzwerkinstallations</q>- oder <q>Netinst</q>-CD ist eine einzelne CD,
mittels derer Sie das gesamte Betriebssystem installieren können.
Diese CD enthält gerade die minimale Menge an Software, um
ein Basis-System zu installieren und die übrigen Pakete über das Internet
zu beziehen.</p>

<p><strong>Was ist besser für mich &ndash; die minimale CD-ROM zum Booten oder die
kompletten CDs?</strong> Es kommt darauf an, aber wir denken, dass in den
meisten Fällen das minimale CD-Image besser ist &ndash; auf jeden Fall laden Sie so nur
die Pakete herunter, die Sie zur Installation auf Ihrem Rechner ausgewählt
haben, was sowohl Zeit als auch Bandbreite spart. Auf der anderen Seite sind
die kompletten CDs besser dafür geeignet, wenn Sie mehr als einen Rechner
installieren wollen, oder auf einem Rechner ohne kostenloser
Internet-Anbindung.</p>

<p><strong>Welche Arten von Netzverbindungen werden während der Installation
unterstützt?</strong>
Für die Netzwerkinstallation wird davon ausgegangen, dass Sie eine Verbindung ins Internet
haben. Es werden dazu mehrere verschiedene Wege unterstützt, wie analoge
Einwahlverbindungen, Ethernet oder WLAN (mit einigen Einschränkungen);
ISDN wird jedoch nicht unterstützt &ndash; Verzeihung!
</p>

<p>Die folgenden, minimalen bootfähigen CD-Images sind zum Herunterladen
   erhältlich:</p>

<ul>

  <li>Offizielle <q>Netinst</q>-Images für die <q>Stable</q>-Veröffentlichung &ndash; <a
  href="#netinst-stable">siehe unten</a></li>

  <li>Images für die <q>Testing</q>-Veröffentlichung, sowohl täglich gebaute als auch
      bekanntermaßen funktionsfähige Schnappschüsse, siehe die <a
  href="$(DEVEL)/debian-installer/">Debian-Installer-Seite</a>.</li>

</ul>


<h2 id="netinst-stable">Offizielle <q>Netinst</q>-Images für die <q>Stable</q>-Veröffentlichung</h2>

<p>Mit einer Größe von bis zu 300&nbsp;MB enthält dieses Image
den Installer und einen kleinen Satz an Paketen, was die
Installation eines (sehr) grundlegenden Systems erlaubt.</p>

<div class="line">
<div class="item col50">
<p><strong>Netinst-CD-Image (typischerweise 150-300 MB, je nach Architektur)</strong></p>
	  <stable-netinst-images />
</div>
<div class="item col50 lastcol">
<p><strong>Netinst-CD-Image (über <a href="$(HOME)/CD/torrent-cd">Bittorrent</a>)</strong></p>
	  <stable-netinst-torrent />
</div>
<div class="clear"></div>
</div>

<p>Informationen über diese Dateien und wie sie benutzt werden, finden
   Sie in der <a href="../faq/">FAQ</a>.</p>

<p>Sobald Sie die Images heruntergeladen haben, sollten Sie auf jeden Fall die
<a href="$(HOME)/releases/stable/installmanual">detaillierten Informationen über
den Installationsprozess</a> lesen.</p>

# Translators: the following paragraph exists (in this or a similar form) several times in webwml,
# so please try to keep translations consistent. See:
# ./CD/http-ftp/index.wml
# ./CD/live/index.wml
# ./CD/netinst/index.wml
# ./CD/torrent-cd/index.wml
# ./distrib/index.wml
# ./distrib/netinst.wml
# ./releases/<release-codename>/debian-installer/index.wml
# ./devel/debian-installer/index.wml
# 
<h2><a name="firmware">Inoffizielle Netinst-Images mit nicht-freier Firmware</a></h2>

<div id="firmware_nonfree" class="important">
<p>
Sollten Sie Hardware verwenden, deren Treiber <strong>das Laden nicht-freier
Firmware erfordert</strong>, können Sie einen der
<a href="https://cdimage.debian.org/cdimage/unofficial/non-free/firmware/stable/current/">\
Tarballs mit häufig verwendeten Firmware-Archiven</a> verwenden oder ein
<strong>inoffizielles</strong> Installations-Image herunterladen, das diese
<strong>nicht-freien</strong> Firmware-Dateien enthält. Eine
Anleitung zur Verwendung der Tarballs sowie allgemeine Informationen über
das Laden von Firmware während der Installation finden Sie auch in der
<a href="../../releases/stable/amd64/ch06s04">Installationsanleitung</a>.
</p>
<p>
<a href="https://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/current/">inoffizielle
Installations-Images für <q>Stable</q> (inklusive Firmware)</a>
</p>
</div>
