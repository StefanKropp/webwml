#use wml::debian::ddp title="Debian Entwickler-Handbücher"
#use wml::debian::translation-check translation="78f856f0bff4125a68adb590b66952d97f183227"
# $Id$
#include "$(ENGLISHDIR)/doc/manuals.defs"
#include "$(ENGLISHDIR)/doc/devel-manuals.defs"
# Translator: Martin Schulze <joey@debian.org>
# Updated: Holger Wansing <linux@wansing-online.de>, 2011 - 2018.
# Updated: Holger Wansing <hwansing@mailbox.org>, 2019, 2020.

<document "Debian Policy-Handbuch" "policy">

<div class="centerblock">
<p>
  Dieses Handbuch beschreibt die Richtlinien für die
  Distribution Debian GNU/Linux. Dazu gehören die Struktur und
  der Inhalt des Debian-Archivs, verschiedene Design-Entscheidungen des
  Betriebssystems sowie technische Anforderungen, die jedes Paket
  erfüllen muss, um in die Distribution aufgenommen zu werden.
</p>
<doctable>
  <authors "Ian Jackson, Christian Schwarz, David A. Morris">
  <maintainer "Die Debian-Policy-Gruppe">
  <status>
  fertig
  </status>
  <availability>
  <inpackage "debian-policy">
  <inddpvcs-debian-policy>
  <p><a href="https://bugs.debian.org/debian-policy">Vorgeschlagene Änderungen</a> für die Policy

  <p>Ergänzende Policy-Dokumentation:</p>
  <ul>
    <li><a href="packaging-manuals/fhs/fhs-3.0.html">Filesystem-Hierarchy-Standard</a>
       [<a href="packaging-manuals/fhs/fhs-3.0.pdf">PDF</a>]
       [<a href="packaging-manuals/fhs/fhs-3.0.txt">Reiner Text</a>]</li>
    <li><a href="debian-policy/upgrading-checklist.html">Upgrading-Checkliste</a></li>
    <li><a href="packaging-manuals/virtual-package-names-list.yaml">Namensliste
      der virtuellen Pakete</a></li>
    <li><a href="packaging-manuals/menu-policy/">Menü-Policy</a>
        [<a href="packaging-manuals/menu-policy/menu-policy.txt.gz">Reiner Text</a>]</li>
    <li><a href="packaging-manuals/perl-policy/">Perl-Policy</a>
        [<a href="packaging-manuals/perl-policy/perl-policy.txt.gz">Reiner Text</a>]</li>
    <li><a href="packaging-manuals/debconf_specification.html">debconf-\
Spezifikation</a></li>
    <li><a href="packaging-manuals/debian-emacs-policy">Emacsen-Policy</a></li>
    <li><a href="packaging-manuals/java-policy/">Java-Policy</a></li>
    <li><a href="packaging-manuals/python-policy/">Python-Policy</a></li>
    <li><a href="packaging-manuals/copyright-format/1.0/">Copyright-Format-Spezifikation</a>
  </ul>
  </availability>
</doctable>
</div>

<hr />

<document "Debian-Entwicklerreferenz" "devref">

<div class="centerblock">
<p>
  Dieses Handbuch beschreibt die Prozeduren und Ressourcen für
  Debian-Entwickler. Es beschreibt, wie man ein neuer Entwickler
  wird, die Upload-Prozedur, wie man die Fehlerdatenbank (Bug-Tracking-System) bedient,
  die Mailinglisten, Internet-Server etc.
</p>

<p>
  Dieses Handbuch ist als <em>Referenzhandbuch</em> für alle
  Debian-Entwickler gedacht (sowohl neue als auch alte).
</p>

<doctable>
  <authors "Ian Jackson, Christian Schwarz, Lucas Nussbaum, Rapha&euml;l Hertzog, Adam Di Carlo, Andreas Barth">
  <maintainer "Lucas Nussbaum, Hideki Yamane, Holger Levsen">
  <status>
  fertig
  </status>
  <availability>
  <inpackage "developers-reference">
  <inddpvcs-developers-reference>
  </availability>
</doctable>
</div>

<hr />

<document "Handbuch für Debian-Paketbetreuer" "debmake-doc">

<div class="centerblock">
<p>
Diese Anleitung beschreibt den Bau eines Debian-Pakets mittels dem
<code>debmake</code>-Befehl. Sie ist für normale Benutzer sowie
angehende Paketbetreuer gedacht.
</p>
<p>
Der Fokus liegt auf dem modernen Paketierungsstil, es sind viele einfache
Beispiele enthalten.
</p>
<ul>
<li>Paketierung über POSIX-Shellskripte</li>
<li>Paketierung über Python3-Skripte</li>
<li>C mit Makefile/Autotools/CMake</li>
<li>mehrere Binärpakete mit einer gemeinsam genutzten Bibliothek usw.</li>
</ul>
<p>
Dieses »Handbuch für Debian-Paketbetreuer« kann als Erbe des
»Debian-Leitfadens für neue Paketbetreuer« betrachtet werden.
</p>

<doctable>
  <authors "Osamu Aoki">
  <maintainer "Osamu Aoki">
  <status>
  fertig
  </status>
  <availability>
  <inpackage "debmake-doc">
  <inddpvcs-debmake-doc>
  </availability>
</doctable>
</div>

<hr>

<document "Debian-Leitfaden für neue Paketbetreuer" "maint-guide">

<div class="centerblock">
<p>
  Dieses Dokument versucht, das Erstellen von Debian-Paketen für
  normale Anwender (und zukünftige Entwickler) in gewöhnlicher Sprache zu
  beschreiben, gut ausgerüstet mit funktionierenden Beispielen.
</p>

<p>
  Anders als frühere Dokumente baut dieses auf
  <code>debhelper</code> sowie neuen Werkzeugen, die einem
  Entwickler zur Verfügung stehen, auf. Der Autor strebt an, vorherige Bemühungen
  mit einzubeziehen und anzugleichen.
</p>

<doctable>
  <authors "Josip Rodin, Osamu Aoki">
  <maintainer "Osamu Aoki">
  <status>
  soll durch das “Handbuch für Debian-Paketbetreuer” (debmake-doc) ersetzt werden
  </status>
  <availability>
  <inpackage "maint-guide">
  <inddpvcs-maint-guide>
  </availability>
</doctable>
</div>

<hr>

<document "Einführung in die Debian-Paketierung" "packaging-tutorial">

<div class="centerblock">
<p>
  Diese Anleitung ist eine Einführung in die Paketierungsarbeit bei Debian.
  Sie lehrt angehende Entwickler, wie man bestehende Pakete modifiziert,
  eigene Pakete erstellt und mit der Debian-Gemeinschaft zusammenarbeitet.
  Zusätzlich zu der Hauptanleitung enthält es drei Praxis-Beispiele
  über das Modifizieren des <code>grep</code>-Pakets, das Paketieren des
  Spiels <code>GNUjump</code> sowie einer Java-Bibliothek.
</p>

<doctable>
  <authors "Lucas Nussbaum">
  <maintainer "Lucas Nussbaum">
  <status>
  fertig
  </status>
  <availability>
  <inpackage "packaging-tutorial">
  <inddpvcs-packaging-tutorial>
  </availability>
</doctable>
</div>

<hr />

<document "Debian Menü-System" "menu">

<div class="centerblock">
<p>
  Dieses Handbuch beschreibt das Menüsystem von Debian und das
  <strong>menu</strong>-Paket.
</p>

<p>
  Das menu-Paket wurde vom Programm install-fvwm2-menu aus dem
  alten fvwm2-Paket inspiriert. Menu versucht eine allgemeinere
  Schnittstelle für die Erzeugung von Menüs zu schaffen. Mit dem
  Befehl update-menus aus diesem Paket muss kein Paket mehr für jeden
  X-Window-Manager modifiziert werden. Es bietet eine einheitliche
  Schnittstelle sowohl für text- als auch für X-orientierte Programme.
</p>

<doctable>
  <authors "Joost Witteveen, Joey Hess, Christian Schwarz">
  <maintainer "Joost Witteveen">
  <status>
  fertig
  </status>
  <availability>
  <inpackage "menu">
  <a href="packaging-manuals/menu.html/">HTML Online</a>
  </availability>
</doctable>
</div>

<hr>

<document "Debian-Installer Internals" "d-i-internals">

<div class="centerblock">
<p>
  Dieses Dokument soll den Debian-Installer leichter für neue
  Entwickler zugänglich machen und ein zentraler Ort zur Dokumentation
  technischer Informationen sein.
</p>

<doctable>
  <authors "Frans Pop">
  <maintainer "Debian-Installer-Team">
  <status>
  fertig
  </status>
  <availability>
  <p><a href="https://d-i.debian.org/doc/internals/">HTML online</a>.</p>
  <p><a href="https://salsa.debian.org/installer-team/debian-installer/tree/master/doc/devel/internals">DocBook-XML-Quellen online</a>.</p>
  </availability>
</doctable>
</div>

<hr>

<document "dbconfig-common-Dokumentation" "dbconfig-common">

<div class="centerblock">
<p>
   Dieses Dokument ist gedacht für Entwickler, die Pakete betreuen und in diesen
   eine funktionierende Datenbank benötigen. Statt die nötige Logik selbst
   zu implementieren, können sie auf dbconfig-common zurückgreifen, um
   während Installation, Upgrade, Neu-Konfiguration und Deinstallation
   die richtigen Fragen zu stellen und die Datenbank zu erstellen und
   zu füllen.

<doctable>
  <authors "Sean Finney and Paul Gevers">
  <maintainer "Paul Gevers">
  <status>
  ready
  </status>
  <availability>
  <inpackage "dbconfig-common">
  <inddpvcs-dbconfig-common>
    Zusätzlich ist auch das
    <a href="/doc/manuals/dbconfig-common/dbconfig-common-design.html">Design-Dokument</a>
    verfügbar.
  </availability>
</doctable>
</div>

<hr>

<document "dbapp-Policy" "dbapp-policy">

<div class="centerblock">
<p>
    Vorschlag einer Policy für Pakete, die eine funktionierende Datenbank
    benötigen.

<doctable>
  <authors "Sean Finney">
  <maintainer "Paul Gevers">
  <status>
  draft
  </status>
  <availability>
  <inpackage "dbconfig-common">
  <inddpvcs-dbapp-policy>
  </availability>
</doctable>
</div>
