#use wml::debian::template title="Secretário(a) do projeto Debian" BARETITLE="true" NOHEADER="true"
#use wml::debian::translation-check translation="965001d7d8f032b9d41bfa609f0cca3bc3c927d3"

    <h1 class="title">O(A) secretário(a) do projeto Debian</h1>

    <p class="initial">
      Com o crescimento do <a href="$(HOME)/">projeto Debian</a>,
      ficou claro que era necessário ter um conjunto de regras
      semiformais para ajudar na resolução de conflitos e, como
      resultado, a constituição foi escrita. A <a href="constitution">\
      constituição</a> do Debian descreve a estrutura organizacional para a
      tomada de decisões formais no projeto. A constituição delineia quem
      toma as decisões e quais os poderes de cada um desses indivíduos ou
      grupos que tomam as decisões. O cargo de secretário(a) do projeto é uma
      das seis entidades enumeradas na constituição do Debian como entidades
      tomadoras de decisão.
    </p>

    <p>
      Qualquer desenvolvedor(a) Debian é elegível para ser considerado(a) para
      o posto de secretário(a) do projeto Debian. Qualquer pessoa pode ter
      vários postos, exceto que o(a) secretário(a) do projeto não pode ser
      também o(a) <a href="leader">líder do projeto Debian</a>, ou o(a)
      presidente do <a href="tech-ctte">Comitê Técnico</a>.
    </p>

    <h2>Apontamento</h2>

    <p>
      Ao contrário dos outros(as) delegados(as), que são apontados pelo(a)
      <a href="leader">líder do projeto</a>, o(a) próximo(a) secretário(a) do
      projeto é apontado(a) pelo(a) líder do projeto e pelo(a) secretário(a) do
      projeto atual. Caso o(a) secretário(a) atual e o(a) líder do projeto
      discordem, eles(as) devem pedir aos(às) desenvolvedores(as) por meio de
      resolução geral que nomeiem um(a) secretário(a).
    </p>

    <p>
      O mandato do(a) secretário(a) do projeto é de 1 ano, após o qual ele ou
      outro(a) secretário(a) precisa ser (re)nomeado(a).
    </p>

    <h2>Tarefas realizadas pelo(a) secretário(a) do projeto</h2>


    <h3>Condução de votações</h3>
    <p>
      A tarefa mais visível realizada pelo(a) secretário(a) é a condução das
      <a href="$(HOME)/vote/">votações</a> para o projeto
      -- notavelmente as eleições do(a) líder do projeto, mas também
      qualquer outra votação que for feita (resoluções gerais, por
      exemplo). Executar uma votação também implica em determinar
      o número e a identidade das pessoas elegíveis à votação,
      para o propósito de calcular o quorum.
    </p>

    <h3>Ocupar a posição de outros cargos</h3>

    <p>
      O(A) secretário(a) do projeto pode ocupar a posição do(a) líder, junto com
      o(a) presidente do comitê técnico. Nessa situação, eles(as) podem tomar
      decisões juntos(as) se considerarem imperativo fazê-lo -- mas somente
      quando for absolutamente necessário e somente quando consistente
      com o consenso dos(as) desenvolvedores(as).
    </p>

    <p>
      Se não houver um(a) secretário(a) do projeto ou o(a) secretário(a) atual
      estiver indisponível e não tiver delegado autoridade para uma decisão,
      então a decisão pode ser feita ou delegada pelo(a) presidente do comitê
      técnico, como secretário(a) em exercício.
    </p>

    <h3>Interpretando a constituição</h3>

    <p>
      O(A) secretário(a) também é responsável por decidir qualquer disputa
      sobre a interpretação da constituição.
    </p>


    <h2>Informação de contato</h2>

    <p>Contate o(a) secretário(a) do projeto Debian enviando uma mensagem em
    inglês para <email "secretary@debian.org">.</p>


    <h1 class="center">Sobre nosso(a) secretário(a) atual</h1>

    <p>
      O atual secretário do projeto é Kurt Roeckx
      &lt;<email "kroeckx@debian.org">&gt;.
      Kurt está envolvido com software livre e linux desde 1995, e tem sido
      um desenvolvedor Debian desde
      <a href='https://lists.debian.org/debian-project/2005/08/msg00283.html'>
      Agosto de 2005</a>.<br /> Kurt tem mestrado em Eletrônica e TIC, concedido
      pela Hogeschool voor Wetenschap &amp; Kunst.
    </p>

    <p>
      Kurt tem sido secretário do projeto desde Fevereiro de 2009; ele assumiu o
      cargo depois que o secretário anterior, Manoj Srivastava, renunciou ao
      cargo. Além de ser secretário do projeto, Kurt faz o porte para o AMD64 e
      mantém
      <a href="https://qa.debian.org/developer.php?login=kurt@roeckx.be">12
      pacotes</a>, incluindo libtool, ntp e openssl.
    </p>

    <h2>O posto de secretário(a) assistente</h2>

    <p>
      Devido ao aumento da quantidade de trabalho (por exemplo: votação) que o
      secretário estava começando a manusear, o DPL (na época, Steve
      McIntyre) e o secretário anterior (Manoj) decidiram que seria uma boa
      ideia nomear um assistente em casos de indisponibilidade.<br />
      Atualmente, Neil McGovern &lt;<email "neilm@debian.org">&gt; está nesse
      cargo.
    </p>

    <hrline>
    <address>
      <a href="mailto:secretary@debian.org">O(A) secretário(a) do projeto Debian</a>
    </address>

## Local variables:
## sgml-default-doctype-name: "HTML"
## End:
