#use wml::debian::template title="Canto dos(as) desenvolvedores(as) Debian" MAINPAGE="true"
#use wml::debian::translation-check translation="fca004f92b97a053bc89c121827943f1eae0531b"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<aside>
<p><span class="fas fa-caret-right fa-3x"></span>Embora todas as informações
desta página e todos os links para outras páginas estejam publicamente
disponíveis, este site é voltado principalmente para desenvolvedores(as)
Debian.</p>
</aside>

<ul class="toc">
<li><a href="#basic">Básico</a></li>
<li><a href="#packaging">Empacotamento</a></li>
<li><a href="#workinprogress">Trabalhos em andamento</a></li>
<li><a href="#projects">Projetos</a></li>
<li><a href="#miscellaneous">Diversos</a></li>
</ul>

<div class="row">

  <!-- left column -->
  <div class="column column-left" id="basic">
    <div style="text-align: center">
      <span class="fa fa-users fa-3x" style="float:left;margin-right:5px;margin-bottom:5px;"></span>
      <h2><a id="basic">Informações gerais</a></h2>
      <p>Uma lista dos(as) atuais desenvolvedores(as) e mantenedores(as), como
      ingressar no projeto e links para o banco de dados dos(as)
      desenvolvedores(as), a constituição, o processo de votação, versões e
      arquiteturas.</p>
    </div>

    <div style="text-align: left">
      <dl>
        <dt><a href="$(HOME)/intro/organization">Organização do Debian</a></dt>
        <dd>Mais de mil voluntários(as) fazem parte do projeto Debian. Esta
        página explica a estrutura organizacional do Debian, lista as equipes
        e seus(suas) membros(as), bem como os endereços de contato.</dd>
        <dt>As pessoas por trás do Debian</dt>
        <dd><a href="https://wiki.debian.org/DebianDeveloper">Desenvolvedores(as) Debian (DD)</a>
        (membros(as) plenos(as) do projeto Debian) e
        <a href="https://wiki.debian.org/DebianMaintainer">mantenedores(as) Debian (DM)</a>
        contribuem com o projeto. Por favor, dê uma olhada na
        <a href="https://nm.debian.org/public/people/dd_all/">lista de desenvolvedores(as) Debian</a>
        e na <a href="https://nm.debian.org/public/people/dm_all/">lista de mantenedores(as) Debian</a>
        para descobrir mais sobre as pessoas envolvidas, incluindo os pacotes
        que eles(as) mantêm. Também temos um <a href="developers.loc"> mapa mundial de desenvolvedores(as) Debian</a>
        e uma <a href="https://gallery.debconf.org/">galeria</a> com imagens de
        vários eventos Debian.</dd>
        <dt><a href="join/">Como ingressar no Debian</a></dt>
        <dd>Você gostaria de contribuir e ingressar no projeto? Estamos sempre
        procurando por novos(as) desenvolvedores(as) ou entusiastas de Software
        Livre com habilidades técnicas. Para obter mais informações, visite esta
        página.</dd>
        <dt><a href="https://db.debian.org/">Banco de dados de desenvolvedores(as)</a></dt>
        <dd>Algumas informações neste banco de dados são acessíveis a todos(as),
        algumas informações são apenas para desenvolvedores(as) que efetuaram
        login. O banco de dados contém informações como
        <a href="https://db.debian.org/machines.cgi">máquinas do projeto</a> e
        <a href="extract_key">chaves GnuPG dos(as) desenvolvedores(as)</a>.
        Os(As) desenvolvedores(as) com uma conta podem
        <a href="https://db.debian.org/password.html">alterar sua senha</a> e
        aprender como configurar o
        <a href="https://db.debian.org/forward.html">encaminhamento de e-mail</a>
        para sua conta Debian. Se você está planejando usar uma das máquinas
        Debian, por favor certifique-se de ler as
        <a href="dmup">políticas de uso de máquinas Debian</a>.</dd>
        <dt><a href="constitution">A constituição</a></dt>
        <dd>Este documento descreve a estrutura organizacional para tomadas de
        decisões formais no projeto.</dd>
        <dt><a href="$(HOME)/vote/">Informações sobre votações</a></dt>
        <dd>Como elegemos nossos(as) líderes, escolhemos nossos logotipos e como
        votamos em geral.</dd>
        <dt><a href="$(HOME)/releases/">Versões</a></dt>
        <dd>Essa página lista as versões atuais
        (<a href="$(HOME)/releases/stable/">estável - stable</a>,
        <a href="$(HOME)/releases/testing/">teste - testing</a> e
        <a href="$(HOME)/releases/unstable/">instável - unstable</a>), e contém
        um sumário de lançamentos antigos e de seus codinomes.</dd>
        <dt><a href="$(HOME)/ports/">Arquiteturas diferentes</a></dt>
        <dd>O Debian roda em vários tipos de arquiteturas. Esta página coleta
        informações sobre vários portes do Debian, alguns baseados no kernel
        Linux, outros nos kernels FreeBSD, NetBSD e Hurd.</dd>
     </dl>
    </div>

  </div>

  <!-- right column -->
  <div class="column column-right" id="software">
    <div style="text-align: center">
      <span class="fa fa-code fa-3x" style="float:left;margin-right:5px;margin-bottom:5px;"></span>
      <h2><a id="packaging">Empacotamento</a></h2>
      <p>Links para nosso manual de políticas e outros documentos relacionados às
      políticas do Debian, procedimentos e outros recursos para desenvolvedores(as)
      Debian, e o guia para novos(as) mantenedores(as).</p>
     </div>

    <div style="text-align: left">
      <dl>
        <dt><a href="$(DOC)/debian-policy/">Manual de políticas Debian</a></dt>
        <dd>Esse manual descreve os requisitos das políticas para a distribuição
        Debian. Isso inclui a estrutura e o conteúdo do repositório Debian,
        várias questões de design do sistema operacional, assim
        como requisitos técnicos que cada pacote deve satisfazer para ser
        incluído na distribuição.

        <p>Resumindo, você <strong>precisa</strong> ler este manual.</p>
        </dd>
      </dl>

      <p>Há vários documentos relacionados à política que devem ser de
      seu interesse, como:</p>

      <ul>
        <li><a href="https://wiki.linuxfoundation.org/lsb/fhs/">Filesystem Hierarchy Standard</a> (FHS)
        <br />O FHS define a estrutura de diretórios e o conteúdo dos diretórios
        (localização dos arquivos); a conformidade com a versão 3.0 é obrigatória
        (veja o <a href="https://www.debian.org/doc/debian-policy/ch-opersys.html#file-system-hierarchy">capítulo 9</a>
        do manual de políticas Debian). </li>
        <li>Lista de <a href="$(DOC)/packaging-manuals/build-essential">pacotes build-essential</a>
        <br />Espera-se que você tenha esses pacotes se quiser compilar
        software, construir um pacote ou um conjunto de pacotes. Você não tem
        que incluí-los na linha <code>Build-Depends</code> quando
        <a href="https://www.debian.org/doc/debian-policy/ch-relationships.html">declarar relacionamentos</a>
        entre pacotes.</li>
        <li><a href="$(DOC)/packaging-manuals/menu-policy/">Sistema de menus</a>
        <br />Estrutura de itens de menu do Debian; verifique também a
        documentação do <a href="$(DOC)/packaging-manuals/menu.html/">sistema de menus</a>.</li>
        <li><a href="$(DOC)/packaging-manuals/debian-emacs-policy">Políticas do Emacs</a>
        <li><a href="$(DOC)/packaging-manuals/java-policy/">Políticas Java</a>
	<li><a href="$(DOC)/packaging-manuals/perl-policy/">Política Perl</a>
        <li><a href="$(DOC)/packaging-manuals/python-policy/">Política Python</a>
        <li><a href="$(DOC)/packaging-manuals/debconf_specification.html">Especificação da Debconf</a>
        <li><a href="https://www.debian.org/doc/manuals/dbapp-policy/">Políticas de aplicações de bancos de dados</a> (rascunho)
	<li><a href="https://people.debian.org/~lbrenta/debian-ada-policy.html">Política Debian para Ada</a>
	<br />Subpolítica que cobre tudo em relação ao empacotamento Ada.</li>
      </ul>

      <p>Por favor dê uma olhada também nas <a
      href="https://bugs.debian.org/debian-policy">propostas de mudanças
      para as políticas Debian</a>.</p>

      <dl>
        <dt><a href="$(DOC)/manuals/developers-reference/">
        Referência dos(as) desenvolvedores(as)</a></dt>

        <dd>
        Visão geral dos procedimentos recomendados e dos recursos disponíveis
        para desenvolvedores(as) Debian -- outra
        <strong>leitura obrigatória</strong>.
        </dd>

        <dt><a href="$(DOC)/manuals/debmake-doc/">Guia para mantenedores(as) Debian</a></dt>

        <dd>
        Como construir um pacote Debian (em linguagem comum) incluindo vários
        exemplos. Se você está planejando se tornar um(a) desenvolvedor(a)
        ou mantenedor(a) Debian, este é um bom ponto de partida.
        </dd>
      </dl>


    </div>

  </div>

</div>

<h2><a id="workinprogress">Trabalho em andamento: links para desenvolvedores(as) e mantenedores(as) Debian ativos(as)</a></h2>

<aside class="light">
  <span class="fas fa-wrench fa-5x"></span>
</aside>

<dl>
  <dt><a href="testing">Debian testing</a></dt>
  <dd>
    Gerado automaticamente a partir da versão &lsquo;unstable&rsquo;:
    aqui é onde você precisa colocar seus pacotes para que sejam considerados
    no próximo lançamento do Debian.
  </dd>

  <dt><a href="https://bugs.debian.org/release-critical/">Bugs críticos ao lançamento</a></dt>
  <dd>
    Uma lista de bugs que podem fazer com que um pacote seja removido da versão
   &lsquo;testing&rsquo;, ou que podem causar um atraso no lançamento da próxima
   versão. Relatórios de bugs com uma severidade maior ou igual a
   &lsquo;serious&rsquo; os qualificam para a lista, então por favor
   certifique-se de corrigir esses bugs nos seus pacotes assim que puder.
  </dd>

  <dt><a href="$(HOME)/Bugs/">O sistema de rastreamento de bugs do Debian (BTS - Bug Tracking System)</a></dt>
    <dd>
    Para relatar, discutir e corrigir bugs. O BTS é útil tanto para usuários(as)
    quanto para desenvolvedores(as).
    </dd>

  <dt>Informações sobre pacotes Debian</dt>
    <dd>
      As páginas web de
      <a href="https://qa.debian.org/developer.php">informações sobre pacotes</a>
      e de <a href="https://tracker.debian.org/">rastreamento de pacotes</a>
      fornecem coleções de informações valiosas para os(as) mantenedores(as).
      Os(As) desenvolvedores(as) que desejam acompanhar outros pacotes podem se
      inscrever (por e-mail) em um serviço que envia cópias de e-mails do BTS e
      notificações de uploads e instalações. Por favor veja o
      <a href="$(DOC)/manuals/developers-reference/resources.html#pkg-tracker">manual do rastreador de pacotes</a>
      para obter mais informações.
    </dd>

  <dt><a href="wnpp/">Pacotes que precisam de ajuda</a></dt>
    <dd>
    Work-Needing and Prospective Packages, WNPP para encurtar (pacotes que
    precisam de trabalho e futuros pacotes), é uma lista de pacotes Debian que
    precisam de um(a) novo(a) mantenedor(a) e pacotes que ainda não foram
    incluídos no Debian.
    </dd>

  <dt><a href="$(DOC)/manuals/developers-reference/resources.html#incoming-system">Sistema incoming</a></dt>
    <dd>
    Servidores do repositório interno: este é o lugar para pacotes recebidos
    após o upload. Pacotes aceitos são quase que imediatamente
    disponibilizados via navegador e propagados para os
    <a href="$(HOME)/mirror/">espelhos</a> quatro vezes por dia.
    <br />
    <strong>Nota</strong>: por causa da natureza do &lsquo;incoming&rsquo;,
    não recomendamos espelhá-lo.
    </dd>

    <dt><a href="https://lintian.debian.org/">Relatórios Lintian</a></dt>
      <dd>
      O <a href="https://packages.debian.org/unstable/devel/lintian">Lintian</a>
      é um programa que checa se um pacote está de acordo com a política.
      Desenvolvedores(as) devem usá-lo antes de cada upload;
      </dd>

    <dt><a href="$(DOC)/manuals/developers-reference/resources.html#experimental">Debian &lsquo;Experimental&rsquo;</a></dt>
      <dd>
      A versão &lsquo;experimental&rsquo; é usada como uma área temporária para
      softwares altamente experimentais. Por favor instale os
      <a href="https://packages.debian.org/experimental/">pacotes da experimental</a>
      somente se você já sabe como usar a &lsquo;unstable&rsquo;.
      </dd>

    <dt><a href="https://wiki.debian.org/HelpDebian">Debian wiki</a></dt>
      <dd>
      A wiki do Debian com conselhos para desenvolvedores(as) e outros(as)
      contribuidores(as).
      </dd>
</dl>

<h2><a id="projects">Projetos: grupos internos e projetos</a></h2>

<aside class="light">
  <span class="fas fa-folder-open fa-5x"></span>
</aside>

<ul>
<li><a href="website/">Páginas web do Debian</a></li>
<li><a href="https://ftp-master.debian.org/">Repositório Debian</a></li>
<li><a href="$(DOC)/ddp">Projeto de documentação Debian (DDP)</a></li>
<li><a href="https://qa.debian.org/">Grupo de controle de qualidade (QA) Debian</a></li>
<li><a href="$(HOME)/CD/">Imagens de CD/DVD</a></li>
<li><a href="https://wiki.debian.org/Keysigning">Assinatura de chaves</a></li>
<li><a href="https://wiki.debian.org/Keysigning/Coordination">Coordenação de assinatura de chaves</a></li>
<li><a href="https://wiki.debian.org/DebianIPv6">Projeto Debian IPv6</a></li>
<li><a href="buildd/">Rede auto-builder</a> e seus <a href="https://buildd.debian.org/">logs de construção</a></li>
<li><a href="$(HOME)/international/l10n/ddtp">Projeto de tradução das descrições Debian (DDTP)</a></li>
<li><a href="debian-installer/">O instalador Debian</a></li>
<li><a href="debian-live/">Debian Live</a></li>
<li><a href="$(HOME)/women/">Debian mulheres</a></li>
<li><a href="$(HOME)/blends/">Debian Pure Blends</a></li>

</ul>

<h2><a id="miscellaneous">Links diversos</a></h2>

<aside class="light">
  <span class="fas fa-bookmark fa-5x"></span>
</aside>

<ul>
<li><a href="https://debconf-video-team.pages.debian.net/videoplayer/">Gravações</a> das nossas palestras em eventos.</li>
<li><a href="passwordlessssh">Configurando o ssh</a> para não solicitar uma senha.</li>
<li>Como <a href="$(HOME)/MailingLists/HOWTO_start_list">solicitar uma nova lista de discussão</a>.</li>
<li>Informações sobre <a href="$(HOME)/mirror/">o espelhamento do Debian</a>.</li>
<li>O <a href="https://qa.debian.org/data/bts/graphs/all.png">gráfico de todos os bugs</a>.</li>
<li><a href="https://ftp-master.debian.org/new.html">Novos pacotes</a> esperando para serem incluídos no Debian (NOVA fila).</li>
<li><a href="https://packages.debian.org/unstable/main/newpkg">Novos pacotes Debian</a> nos últimos 7 dias.</li>
<li><a href="https://ftp-master.debian.org/removals.txt">Pacotes removidos do Debian</a>.</li>
</ul>

