#use wml::debian::template title="Errata do Debian-Installer"
#use wml::debian::recent_list
#include "$(ENGLISHDIR)/devel/debian-installer/images.data"
#use wml::debian::translation-check translation="50ddc8fab8f8142c1e8266a7c0c741f9bfe1b23a"

<h1>Errata para <humanversion /></h1>

<p>
Esta é uma lista de problemas conhecidos na versão <humanversion /> do
instalador do Debian. Se você não ver seu problema listado aqui, por favor
nos envie (em inglês) um
<a href="$(HOME)/releases/stable/amd64/ch05s04#submit-bug">relatório de
instalação</a> descrevendo o problema.
</p>

<dl class="gloss">
     <dt>Modo de recuperação quebrado com o instalador gráfico</dt>
     <dd>Foi descoberto durante o teste da imagem do Bullseye RC 1 que o modo
     de recuperação parece quebrado (<a href="https://bugs.debian.org/987377">#987377</a>).
     Além disso, o rótulo “Rescue” no banner precisa ser ajustado para o tema
     do Bullseye. 
     <br />
     <b>Status:</b> Corrigido no Bullseye RC 2.</dd>
     
     <dt>Firmware amdgpu é necessário para muitas placas de vídeo AMD </dt>
     <dd>Parece haver uma necessidade crescente de instalar o firmware amdgpu
     (por meio do pacote não livre <code>firmware-amd-graphics</code>) para
     evitar uma tela preta ao inicializar o sistema instalado. A partir do
     Bullseye RC 1, mesmo usando uma imagem de instalação que inclui todos os
     pacotes de firmware, o instalador não detecta a necessidade desse
     componente específico. Veja o
     <a href="https://bugs.debian.org/989863">relatório de bug abrangente</a>
     para acompanhar nossos esforços. 
     <br />
     <b>Status:</b>Corrigido no Bullseye RC 3.</dd>
     
     <dt>Firmwares necessários para algumas placas de som</dt>
     <dd>Parece que há várias placas de som que requerem o carregamento de um
     firmware para poderem emitir som. A partir do Bullseye, o instalador não é
     capaz de carregá-los antecipadamente, o que significa que a síntese de voz
     durante a instalação não é possível com essas placas. Uma possível solução
     é conectar outra placa de som que não precise desse firmware.
     Veja o <a href="https://bugs.debian.org/992699">relatório guarda-chuva do bug</a>
     para acompanhar nossos esforços.</dd> 
      
     <dt>Instalações de desktop podem não funcionar usando o CD#1 sozinho</dt>
     <dd>Devido a restrições de espaço no primeiro CD, nem todos os
     pacotes esperados do desktop GNOME cabem no CD#1. Para uma instalação bem
     sucedida, use fontes de pacotes extras (por exemplo, um segundo CD ou um
     espelho de rede), ou use um DVD em vez do CD. <br /> <b>Estado:</b> Não
     é provável que maiores esforços possam ser realizados para encaixar
     mais pacotes no CD#1. </dd>

     <dt>O LUKS2 é incompatível com o suporte ao cryptodisk do GRUB</dt>
     <dd>Só foi descoberto tardiamente que o GRUB não tem suporte para
       LUKS2. Isto significa que os(as) usuários(as) que queiram usar
       <tt>GRUB_ENABLE_CRYPTODISK</tt> e evitar um <tt>/boot</tt>
       separado e não encriptado, não serão capazes de fazê-lo
       (<a href="https://bugs.debian.org/927165">#927165</a>).  De qualquer
       modo, esta configuração não é suportada pelo instalador, mas faria
       sentido ao menos documentar esta limitação mais proeminentemente,
       e ter ao menos a possibilidade de optar pelo
       LUKS1 durante o processo de instalação.
     <br />
     <b>Estado:</b> Algumas ideias foram expressas sobre este bug;
     os(as) mantenedores(as) do cryptsetup escreveram
     <a href="https://cryptsetup-team.pages.debian.net/cryptsetup/encrypted-boot.html">alguma documentação específica</a>.</dd>
</dl>
