#use wml::debian::template title="Como o www.debian.org é feito"
#use wml::debian::toc
#use wml::debian::translation-check translation="7f876037d8c7ab141d9c245fcd09a3b77e25b3a7"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<ul class="toc">
<li><a href="#look">Aparência &amp; comportamento</a></li>
<li><a href="#sources">Fontes</a></li>
<li><a href="#scripts">Scripts</a></li>
<li><a href="#generate">Gerando o site web</a></li>
<li><a href="#help">Como ajudar</a></li>
<li><a href="#faq">Como não ajudar... (FAQ)</a></li>
</ul>

<h2><a id="look">Aparência &amp; comportamento</a></h2>

<p>O site web do Debian é uma coleção de diretórios e arquivos localizados em
<code>/org/www.debian.org/www</code>
em <em>www-master.debian.org</em>. A maioria das páginas são arquivos HTML
estáticos. Eles não contêm elementos dinâmicos como scripts CGI ou PHP porque
o site é espelhado.
</p>

<p>
O site web do Debian usa o Website Meta Language
(<a href="https://packages.debian.org/unstable/web/wml">WML</a>) para gerar as
páginas HTML, incluindo cabeçalhos e rodapés, títulos, índice de conteúdos, etc.
Embora um arquivo <code>.wml</code> possa parecer HTML à primeira vista, o HTML
é apenas um dos tipos de informações extras que podem ser usadas em WML. Você
também pode incluir código Perl dentro de uma página e, portanto, fazer quase
tudo.
</p>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Nossas páginas web atualmente
estão em conformidade com o padrão
<a href="http://www.w3.org/TR/html4/">HTML 4.01 Strict</a>.</p>
</aside>

<p>
Depois que o WML termina de executar seus vários filtros em um arquivo,
o produto final é o verdadeiro HTML. Por favor observe que, embora o WML
verifique (e às vezes corrija automaticamente) a validade básica do seu código
HTML, você deve instalar uma ferramenta como
<a href="https://packages.debian.org/unstable/web/weblint">weblint</a>
e/ou
<a href="https://packages.debian.org/unstable/web/tidy">tidy</a>
como verificador de sintaxe e de estilo mínimo.
</p>

<p>
Qualquer pessoa que esteja contribuindo regularmente para o site web do Debian
deve instalar o WML para testar o código e certificar-se de que as páginas
HTML resultantes pareçam corretas. Se você estiver executando o Debian,
simplesmente instale o pacote <code>wml</code>.
Para obter mais informações, consulte a página <a href="using_wml">usando WML</a>.
</p>

<toc-add-entry name="sources">Fontes</toc-add-entry>

<p>
Usamos Git para armazenar os fontes do site web do Debian. O sistema de
controle de versão nos permite acompanhar todas as alterações e podemos ver
quem fez o commit do quê e quando, e até porque. O Git oferece uma maneira
segura de controlar a edição simultânea de arquivos-fonte por vários(as)
autores(as) — uma tarefa crucial para a equipe web do Debian, porque ela tem
muitos(as) participantes.
</p>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="using_git">Leia mais sobre Git</a></button></p>

<p>
Aqui estão algumas informações básicas sobre como os fontes são estruturados:
</p>

<ul>
  <li>O diretório mais superior no repositório Git (<code>webwml</code>) contém
diretórios nomeados de acordo com o idioma das respectivas páginas,
dois Makefiles e vários scripts. Os nomes de diretório para páginas traduzidas
devem estar em inglês e usar letras minúsculas, por exemplo <code>german</code>
e não <code>Deutsch</code>.</li>

  <li>O arquivo <code>Makefile.common</code> é especialmente importante, pois
contém algumas regras comuns que são aplicadas incluindo este arquivo em outros
Makefiles.</li>

  <li>Todos os subdiretórios para os diferentes idiomas também contêm Makefiles,
vários arquivos-fonte <code>.wml</code> e subdiretórios adicionais. Todos os
nomes de arquivos e diretórios seguem um certo padrão,
para que cada link funcione para todas as páginas traduzidas.
Alguns diretórios também contêm um arquivo de configuração <code>.wmlrc</code>
com comandos e preferências adicionais para WML.</li>

  <li>O diretório <code>webwml/english/template</code> contém arquivos WML
especiais que funcionam como modelos. Eles podem ser referenciados a partir de
todos os outros arquivos com o comando <code>#use</code>.</li>
</ul>

<p>
Por favor, observe: para garantir que as alterações nos templates sejam
propagadas para os arquivos que os incluem, os arquivos têm dependências de
Makefile neles.
A grande maioria dos arquivos usa o modelo <code>template</code>,
a dependência genérica, então eles contêm a seguinte linha na parte superior:
</p>

<p>
<code>#use wml::debian::template</code>
</p>

<p>
Claro, há exceções a essa regra.
</p>

<toc-add-entry name="scripts">Scripts</toc-add-entry>

<p>
Os scripts são escritos principalmente em shell ou Perl. Alguns
deles não funcionam sozinhos, e alguns estão integrados dentro de
arquivos-fonte WML.
</p>

<ul>
  <li><a href="https://salsa.debian.org/webmaster-team/cron.git">webmaster-team/cron</a>:
Este repositório Git contém todos os scripts usados para atualizar o site web do
Debian, ou seja, os fontes para os scripts de reconstrução.
<code>www-master</code>.</li>
  <li><a href="https://salsa.debian.org/webmaster-team/packages">webmaster-team/packages</a>:
Este repositório Git contém os fontes para os scripts de reconstrução do <code>packages.debian.org</code>.</li>
</ul>

<h2><a id="generate">Gerando o site web</a></h2>

<p>
WML, templates e scripts shell ou Perl são todos os ingredientes que você
precisa para gerar o site web do Debian:
</p>

<ul>
  <li>A maioria é gerada usando WML (do <a href="$(DEVEL)/website/using_git">repositório Git</a>).</li>
  <li>A documentação é gerada com DocBook XML (<a href="$(DOC)/vcs">repositório Git <q>ddp</q></a>)
ou com <a href="#scripts" >scripts cron</a> dos pacotes Debian correspondentes.</li>
  <li>Algumas partes do site web são geradas com scripts usando outras fontes,
por exemplo, as páginas de (des)assinatura da lista de discussão.</li>
</ul>

<p>
Uma atualização automática (do repositório Git e outras fontes para a webtree)
está sendo executada seis vezes por dia.
Além disso, realizamos regularmente as seguintes verificações em todo o site:
</p>

<ul>
  <li><a href="https://www-master.debian.org/build-logs/urlcheck/">verificador de URL</a></li>
  <li><a href="https://www-master.debian.org/build-logs/validate/">wdg-html-validator</a></li>
  <li><a href="https://www-master.debian.org/build-logs/tidy/">tidy</a></li>
</ul>

<p>
Os logs atualizados de construção do site web podem ser encontrados em
<url "https://www-master.debian.org/build-logs/">.
</p>

<p>Se você quiser contribuir com o site, simplesmente <strong>não</strong> edite
arquivos no diretório <code>www/</code> ou adicione novos itens. Em vez disso,
por favor primeiro entre em contato com os(as)
<a href="mailto:webmaster@debian.org">webmasters</a>.
</p>

<aside>
<p><span class="fas fa-cogs fa-3x"></span> Em uma nota mais técnica:
todos os arquivos e diretórios são de propriedade do grupo <code>debwww</code>
que tem permissão de escrita.
Dessa forma, a equipe web pode modificar o conteúdo no diretório web.
O modo <code>2775</code> nos diretórios significa que qualquer arquivo criado
aqui herdará o grupo (<code>debwww</code>).
Espera-se que os(as) participantes do grupo definam <code>umask 002</code> para
que os arquivos sejam criados com permissões de escrita de grupo.</p>
</aside>

<toc-add-entry name="help">Como ajudar</toc-add-entry>

<p>
Encorajamos qualquer pessoa a ajudar com o site web do Debian. Se você tem
informações valiosas relacionadas ao Debian que acha que estão faltando,
por favor
<a href="mailto:debian-www@lists.debian.org">entre em contato</a> — veremos se
poderão ser incluídas.
Além disso, por favor, dê uma olhada no mencionado acima sobre
<a href="https://www-master.debian.org/build-logs/">logs de construção</a> e
veja se você tem sugestões para corrigir um problema.
</p>

<p>
Também estamos procurando pessoas que possam ajudar com o design (gráficos,
layouts, etc). Se você é um(a) falante fluente de inglês, por favor considere
revisar nossas páginas e
<a href="mailto:debian-www@lists.debian.org">relatar</a> erros.
Se fala outro idioma, talvez queira ajudar a traduzir páginas existentes
ou corrigir bugs em páginas já traduzidas.
Em ambos os casos, por favor consulte a lista de
<a href="translation_coordinators">coordenadores(as) de tradução</a> e entre em
contato com a pessoa responsável. Para obter mais informações, consulte nossa
<a href="translation">página para tradutores(as)</a>.
</p>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="todo">Leia nossa lista TODO (a fazer)</a></button></p>

<aside class="light">
  <span class="fa fa-question fa-5x"></span>
</aside>

<h2><a id="faq">Como não ajudar... (FAQ)</a></h2>

<p>
<strong>[P] Quero incluir <em>este recurso sofisticado</em> no site do Debian, posso?</strong>
</p>

<p>
[R] Não. Queremos que www.debian.org seja o mais acessível possível, então
</p>

<ul>
     <li>sem "extensões" específicas para navegador web.</li>
     <li>sem depender apenas de imagens. As imagens podem ser usadas para
         esclarecer, mas as informações em www.debian.org devem permanecer
         acessíveis através de um navegador web de modo texto, como o lynx.</li>
</ul>

<p>
<strong>[P] Tenho essa ideia legal que quero enviar. Você pode ativar
<em>foo</em> ou <em>bar</em> no servidor HTTP de www.debian.org, por favor?</strong>
</p>

<p>
[A] Não. Queremos facilitar a vida dos(as) administradores(as) para espelhar
o www.debian.org, então sem recursos HTTPD especiais, por favor.
Não, nem mesmo SSI (Server Side Include). Uma exceção foi feita
para negociação de conteúdo, porque essa é a única maneira robusta de atender a
vários idiomas.
</p>
