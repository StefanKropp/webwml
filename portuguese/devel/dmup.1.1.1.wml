#use wml::debian::template title="Políticas de uso de máquinas Debian" NOHEADER=yes
#use wml::debian::translation-check translation="b8114b588961778dbd04974c1464a2f388a90c28"

<h2>Políticas de uso de máquinas Debian</h2>

<p>Esta é a versão 1.1.1 das <i>políticas de uso de máquinas Debian</i> (em
inglês DMUP - Debian Machine Usage Policies), que foi suplantada pela
<a href="dmup">atual versão 1.1.2</a>, como anunciado em <a
href="https://lists.debian.org/debian-devel-announce/2010/05/msg00001.html">9 de
maio de 2010</a>.</p>

<ol>
<li><strong>Introdução</strong><br>

Este documento descreve as políticas para utilização de
<a href="https://db.debian.org/machines.cgi">máquinas Debian</a> e
todas as regras em torno disso.

<p>Em resumo:
<ul>
<li>Não interfira por nenhum ato intencional, deliberado, imprudente ou
    ilegal no trabalho de outro(a) desenvolvedor(a) ou comprometa a
    integridade das redes de dados, equipamentos de computação, programas de
    sistemas ou outras informações armazenadas

<li>Não use as instalações do Debian para ganho financeiro privado ou para
    propósitos comerciais, incluindo consultoria ou qualquer outro trabalho
    fora do escopo dos deveres ou funções oficiais que estejam acontecendo,
    sem autorização específica para isso.

<li>Não use as instalações do Debian para atividades ilegais, incluindo, mas
    não limitada a, pirataria de software.
</ul>

<p>Este documento contém duas partes: políticas e diretrizes. As regras
nas políticas são obrigatórias e não podem ser violadas. As diretrizes
detalham as regras que podem ser violadas se necessário, mas preferiríamos que
ninguém o faça.

<li><strong>Declarações gerais</strong><br>

<ol>
<li> Privilégio<br>
O acesso às instalações do Debian é um privilégio, não um direito ou um
serviço comercial, e nos reservamos o direito de revogar este privilégio
a qualquer momento, sem aviso prévio. Uma explicação será dada dentro de
48 horas.

<li> Garantias<br>
Não existe garantias de serviço. Apesar de fazermos o nosso melhor para
assegurar que tudo funcione perfeitamente, não podemos oferecer qualquer
garantia.

<li> Privacidade<br>
Se necessário para manter as máquinas funcionando corretamente, é permitido
a DSA editar arquivos de usuário(a) (por exemplo, modificando arquivos
.forward para quebrar loops de e-mail).

<li> Abreviações usadas<br>
  <ul>
  <li>DSA   - Debian Systems Administration (administração de sistemas Debian)
  <li>DMUP  - Debian Machine Usage Policy (este documento)
  <li>DPL   - Debian Project Leader (líder do projeto Debian)
  </ul>
</ol>

<li><strong>Penalidades</strong><br>

Se alguém viola o conjunto de regras deste documento, ele(s) estará sujeito(a)
a uma penalidade. A penalidade depende do número de violações anteriores
e da infração envolvida.

<ol>
<li> Primeira infração<br>

<ol>
<li>As contas do(a) infrator(a) serão suspesas e o acesso não estará disponível.

<li>É exigido ao(à) infrator(a) contatar a administração de sistemas
    Debian e nos convencer de que não haverá violações adicionais do DMUP
    pelo(a) infrator(a).

<li>Se o(a) infrator(a) falhar em contatar a DSA dentro de 14 dias, a conta
    será terminada e o(a) infrator(a) expulso(a) do projeto Debian. Se o(a)
    infrator(a) tiver anunciado que entraria em férias neste período de tempo,
    este período será estendido com a duração anunciada das férias.

<li>Se o(a) infrator(a) é expulso(a) do projeto, ele(a) pode se registrar
    para tornar-se um(a) mantenedor(a) novamente após um período de um mês.
    A infração permanecerá no registro.
</ol>


<li> Segunda infração<br>

<ol>
<li>As contas do(a) infrator(a) serão suspendidas imediatamente e o(a)
    infrator(a) expulso(a) do projeto.
<li>Se o(a) infrator(a) não entrar com um recurso dentro do prazo
    designado, a conta é encerrada.
<li>É proibido ao(à) infrator(a) se registrar como um(a) mantenedor(a) Debian
    novamente.
</ol>


<li> Publicação<br>

<ol>
<li>A infração e a penalidade serão anunciadas somente para desenvolvedores(as)
    Debian.
<li>Se for considerado necessário, na opinião exclusiva do(a) líder do projeto
    Debian, então um anúncio público será feito. Isto pode incluir
    a identidade do(a) infrator(a).
</ol>


<li> Recurso<br>

<ol>
<li>Se o(a) infrator(a) não concordar com a decisão tomada pela DSA, ele(a)
    pode recorrer aos(às) desenvolvedores(as). Isto só é possível nos 14 dias
    diretamente posteriores ao dia que o(a) infrator(a) foi informado(a) da
    sentença. Isto é feito usando-se o procedimento detalhado na seção 4.2
    da constituição do Debian.
<li>Durante o período em que o recurso é processado, a conta permanecerá
    suspensa.
</ol>
</ol>


<li><strong>As políticas</strong><br>

Esta seção lista as políticas. Esta lista não é, e não pode ser, abrangente.


<dl>
<dt>Uso de disco:

<dd>Todas as máquinas executam um daemon de limpeza no /tmp e exclui os arquivos
após uma semana. Algumas máquinas têm partições /scratch específicas para
armazenagem de grandes conjuntos de dados sem receio de serem apagados. Se
você receber uma notificação de e-mail que seu diretório home está grande
e que você precisa de mais espaço, então por favor tome uma atitude de
imediato. A DSA pode achar necessário limpar espaços de usuários(as)
excepcionalmente grandes sem aviso.

<dt>Shell:

<dd>Por favor use ssh/scp se possível, no lugar de alternativas menos
seguras (rsh, telnet ou FTP).

<p>Conexões ociosas serão terminadas após uma hora; isto é fácil de se
contornar, mas por favor não o faça sem um bom motivo.

<p>Espelhamento, via qualquer meio privado, de qualquer parte dos
repositórios públicos dos servidores privados, é estritamente proibido
sem o consentimento prévio do mirror master residente. Desenvolvedores(as)
são livres para usar qualquer forma pública de acesso disponível.

<dt>Processos:

<dd>Não execute qualquer processo de longa execução sem a permissão da DSA.
Executar servidores de qualquer tipo (isto inclui bots IRC) sem prévia
permissão da DSA também é proibido. Evite executar processos que abusam de
CPU ou memória. Se necessário, a DSA tolherá tais processos sem aviso prévio.


<dt>Páginas WWW:

<dd>Em geral, o espaço web em máquinas Debian é fornecido com o propósito de
comunicar ideias e arquivos relacionados ao projeto ou para a comunidade
de Software Livre em geral. Páginas privadas e 'fúteis' em máquinas Debian
são desencorajadas.

<p>Páginas web comerciais não são permitidas.

<p>Você é responsável pelo conteúdo de suas páginas WWW, incluindo
a obtenção de permissão legal para qualquer trabalho que elas incluam e
assegurando que os conteúdos dessas páginas não violem as leis que se
apliquem à localização do servidor.

<p>Você é responsável por, e aceita a responsabilidade por, qualquer material
proprietário e difamatório, confidencial, secreto ou outro, disponível via
suas páginas WWW.

<p>Você não pode fazer propaganda de suas páginas WWW, ou fazer com que outra
pessoa o faça, por técnicas que seriam classificadas como abuso se elas
foram executadas de uma conta Debian. Isto inclui, mas não se limita,
a envio de e-mails em massa e postagem excessiva de notícias. Tais ações podem
ser tratadas sob a apropriada DMUP como se tivessem sido feitas de uma
conta, ou como uma violação deste DMUP, ou ambos.

<dt>E-mail/Notícias:

<dd>Usar máquinas Debian para ler e-mails é OK, por favor escolha uma máquina
pouco carregada (isto é, não uma master). Não apoiamos o uso de métodos
de download de e-mails como POP ou IMAP, use o servidor de e-mail do seu
provedor e encaminhe. Da mesma forma que páginas web, geralmente encoraja-se
o recebimento de e-mails relacionados a Software Livre ou relacionados ao
projeto de alguma forma. A DSA pode achar necessário comprimir, realocar
ou apagar e-mail sem aviso.
</dl>

<p>Se um(a) desenvolvedor(a) torna-se inalcançável por um tempo prolongado,
suas contas, dados e encaminhamento/filtragem/etc de e-mail podem ser
desabilitados até que reapareça.


<p>Não use as instalações do Debian de uma maneira que constitua abuso de
rede. O Debian não tem qualquer servidor de notícias Usenet. Pode acontecer
que alguma das máquinas Debian tenham acesso a tais servidores de notícias,
mas seu uso através de máquinas Debian é estritamente proibido.

<p>Exemplos do que consideramos abuso de rede:

<ul>
<li><em>Correntes e esquemas em pirâmide-vendas Ponzi</em><br>

          Tais mensagens funcionam (ou melhor, não funcionam) do mesmo
          modo que seus primos baseados em papel. O exemplo mais comum deste
          e-mail é GANHE-DINHEIRO-RÁPIDO. Além de ser um
          gasto de recursos, tais mensagens são ilegais em certos
          países.

<li><em>E-mail comercial não solicitado (UCE)</em><br>

          E-mail comercial não solicitado (em inglês, Unsolicited Commercial
          Email - UCE) é um material de propaganda recebido por e-mail sem que
          o(a) destinatário(a) tenha requisitado tal informação ou expresso
          interesse no material divulgado.

          <p>Já que muitos(as) usuários(as) de internet usam conexões discadas
          e pagam por seu tempo on-line, custa dinheiro receber o
          e-mail. O recebimento de propaganda comercial não solicitada,
          portanto, acarreta despesa e é particularmente indesejável.

          <p>Deve-se notar que um(a) usuário(a) não expressou um interesse
          ao meramente postar um artigo de notícias em qualquer grupo de
          notícias particular, a menos, claro, que tenha feito um
          pedido específico por informações a serem enviadas por e-mail.

<li><em>E-mail em massa não solicitada</em><br>

          Em inglês, "Unsolicited Bulk Email - UBE", é similar ao caso acima
          (UCE), mas que não tenta vender nada. Geralmente, seu único
          propósito é perturbar.

<li><em>Cabeçalhos e/ou endereços forjados</em><br>

          Falsificar cabeçalhos ou mensagens significa enviar e-mails tais que
          sua origem pareça ser de um(a) outro(a) usuário(a) ou máquina, ou
          uma máquina não existente.

          <p>Também é falsificação fazer com que quaisquer respostas para o
          e-mail sejam enviadas para algum(a) outro(a) usuário(a) ou máquina.

          <p>Contudo, em todo caso, se uma permissão prévia foi
          garantida para você por outro(a) usuário(a) ou pelo(a)
          administrador(a) de outra máquina, então não há problema e, claro,
          caminhos reversos "null" podem ser usados como definido nos
          RFCs relevantes.

<li><em>Bombardeio de e-mail</em><br>

          Bombardeio de e-mail (mail bombing) é o envio de múltiplos e-mails,
          ou o envio de um grande e-mail, com o único propósito de perturbar
          e/ou se vingar de um(a) usuário(a) da internet. É um desperdício de
          recursos compartilhados da internet, como também não tem valor para
          o(a) destinatário(a).

          <p>Devido ao tempo para baixá-lo, o envio de grandes e-mails para
          sites sem acordo prévio pode levar à negação de serviço ou
          impedimento de acesso ao e-mail no site destinatário. Note que
          se anexos binários são adicionados ao e-mail, o tamanho pode
          aumentar consideravelmente. Se um arranjo prévio não foi
          feito, o e-mail será extremamente indesejado.

<li><em>Ataque de negação de serviço</em><br>

          Negação de serviço (denial of service) é qualquer atividade projetada
          para impedir que um servidor específico na internet realize uma
          utilização plena e efetiva de seus recursos. Isto inclui, mas não
          se limita, a:

          <ul>
          <li>Bombardear com e-mails um endereço de modo a fazer com que seu
            acesso à internet seja impossível, difícil ou custoso.
          <li>Abrir um número excessivo de conexões de e-mail para o
            mesmo servidor.
          <li>Intencionalmente enviar e-mail projetado para prejudicar os
            sistemas destinatários quando interpretados; por exemplo, enviar
            programas maliciosos ou vírus anexados ao e-mail.
          <li>Usar um smarthost ou um relay SMTP sem autorização para tal.
          </ul>

<li><em>Inscrição em listas de discussão</em><br>

          Você não deve inscrever ninguém que não um(a) usuário(a) de seu
          próprio servidor em uma lista de e-mail ou serviço similar sem
          permissão dele(a).

<li><em>Conteúdo ilegal</em><br>

          Você não deve enviar por e-mail qualquer item que seja ilegal para
          enviar ou possuir.

<li><em>Violação de direito autoral ou propriedade intelectual</em><br>

          Você não deve enviar (via e-mail) ou postar material com direitos
          autorais/copyright ou de propriedade intelectual a menos que tenha
          permissão para isso.

<li><em>Postagens de binárias para grupos de não binários</em><br>

          Fora das hierarquias dos grupos de notícias de alt.binaries... e
          alt.pictures..., a postagem de dados binários codificados é
          considerada muito indesejável. A maioria dos sites Usenet e dos
          leitores não tem a capacidade de transmissão seletiva de artigos
          (kill-filing) e tais postagens podem resultar em uma quantidade
          significante de recursos sendo ocupada e gasta no processo de
          transmissão, e assim pode ser considerada um ataque de negação
          de serviço contra múltiplos destinatários. [Exemplo]

<li><em>Excesso do postagens cruzadas</em><br>

          Simplificando, esta forma de comportamento inaceitável ocorre
          quando o mesmo artigo é postado ao mesmo tempo em um grande número de
          grupos de notícias não relacionados.

<li><em>Excesso de múltiplas postagens</em><br>

          Simplificando, esta forma de comportamento inaceitável ocorre
          quando um artigo substantivamente similar (talvez diferenciando
          somente no cabeçalho de Assunto) é postado em um grande número de
          grupos de notícias não relacionados.
</ul>
</ol>
