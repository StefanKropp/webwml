#use wml::debian::consultant name="Linux Information Systems AG"
#use wml::debian::translation-check translation="31222484a77c08496b404d86d418bc66a9c048ae"

<p>
A Linux Information Systems AG é especializada em trabalhar com pequenas e
médias empresas e autoridades. Seu foco é a organização de TI para escritório e
backoffice.
</p>

<p>
Após um estágio preliminar de consultoria, ajudamos principalmente nossos
clientes com a migração parcial ou total de sua TI para Linux e código aberto.
Nosso objetivo é integrar os diferentes mundos de ambientes heterogêneos, tanto
tecnicamente quanto relacionados aos negócios.
</p>

<p>
Abaixo estão alguns tópicos típicos para os quais reunimos competência superior:
</p>

<ul>
<li>Possibilidade e custos de migrações</li>
<li>Estratégias para migração e implementação de projetos</li>
<li>Servidor de arquivos para ambientes heterogêneos (Windows, Linux/Unix,
    Mac)</li>
<li>Groupware com várias interfaces de usuário (Web, Outlook, Linux, Mac)</li>
<li>Soluções individuais e padronizadas para servidores de e-mail em
    substituição ao Exchange</li>
<li>Servidor integrado de fax e SMS</li>
<li>Soluções para o servidor web apache</li>
<li>Aplicativos do office substituindo o Microsoft Office</li>
<li>Segurança (firewalls, virusscanner, VPNs)</li>
<li>Troca e sincronização de dados de subsidiárias de empresas</li>
<li>Bancos de dados (comercial/código aberto)</li>
<li>Distribuição e instalação automática de software nas empresas</li>
<li>Integração de aplicativos Linux e Windows para modo misto</li>
<li>Migrações de servidores Novell, Windows e Unix para Linux</li>
<li>Migrações de estações de trabalho Windows para Linux com aplicativos rodando
    em Windows e Linux</li>
<li>Serviços de diretório com LDAP como alternativa aos serviços Microsoft
    Active Directory</li>
<li>Soluções de backup</li>
<li>Clusters de failover (pacemaker, DRBD)</li>
<li>Integração do servidor de groupware Zarafa</li>
<li>Virtualização (KVM)</li>
</ul>
