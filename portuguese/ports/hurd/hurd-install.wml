#use wml::debian::template title="Debian GNU/Hurd &mdash; Configuração" NOHEADER="yes"
#include "$(ENGLISHDIR)/ports/hurd/menu.inc"
#use wml::debian::translation-check translation="2d3c1493d91eb2a1fc1bfc125b61b5d0623d2f54"

<h1>Configuração do Debian GNU/Hurd</h1>

<p>
Este documento objetiva fornecer um conjunto de instruções fácil e relativamente
indolor de como configurar o Debian GNU/Hurd com mínimo esforço.
</p>

<p>
Ele é baseado no <q>The Hurd Installation Guide</q> de Neal H. Walfield. Muito
obrigado ao Neal por sua contribuição.
</p>

<h2>Visão geral</h2>

<p>
O GNU é similar em natureza a qualquer sistema do tipo Unix: após fazer o login,
o(a) usuário(a) é apresentado(a) ao shell e ao familiar VFS Unix (virtual
filesystem - sistema de arquivo virtual). Embora o GNU tente ser compatível com
POSIX, ele <q>não é Unix</q>. O GNU/Hurd se baseia em muitos conceitos do
Unix e os expande para tanto adicionar novas funcionalidades quanto para
corrigir aquelas que foram entendidas como falhas no projeto original. A
diferença mais notável são os tradutores, programas de espaço de usuário(a)
que interagem com o VFS. Esses sistemas de arquivo não residem no kernel e nem
precisam ser executados como root; eles só precisam de acesso à instância de
apoio (backing store) e ao <code>ponto de montagem</code>. Outra diferença é
que os processos, em vez de possuírem uma única identidade de usuário(a) fixa
no momento de criação, têm tokens de identidade que ficam em separado do
processo, ou seja, elas podem ser adicionadas com a permissão apropriada de
uma autoridade ou destruídas.
</p>

<p>
Estar familiarizado(a) com o ambiente Unix (e especialmente com o espaço de
usuário(a) GNU, encontrado em variantes populares tais como o GNU/Linux) é
um imperativo para se sentir à vontade no GNU. Ter experiência com as
ferramentas do Debian também se revelarão inestimáveis para a configuração e
a manutenção de uma máquina GNU/Hurd.
</p>

<p>
Este guia se empenha em fazer a instalação do GNU/Hurd um processo tão indolor
quanto possível. Se houver erros, eles são certamente erros do(a) autor(a)
deste texto. Por favor relate-os, junto com qualquer outra sugestão ou crítica,
para ele(a); tudo será aceito com satisfação.
</p>

# Nota do tradutor: excerto ignorado na tradução, pelo menos por enquanto
#<h2> 2. Real Estate or Finding A Home </h2>
#
#<p>
#If you do not have an available partition or an extra hard drive, this can be
#the longest step. In this case, you will need to repartition the hard drive.
#One solution is to use GNU's partition editor,
#<a href="https://packages.debian.org/parted">Parted</a>. It features not only
#basic partition editing but also partition resizing and moving functionality.
#The manual is quite complete and includes several tutorials. Note that also
#there is a <code>63 GNU HURD or SysV</code> partition type, it should not be
#used or else the installation CD will not find it. Just use <code>83
#Linux</code>
#</p>
#
#<p>
#The Hurd supports several extensions to the ext2fs filesystem format. Foremost
#among these are passive translators and a fourth set of permission bits for
#unknown users (users without an identity, not the other user). To use these
#extensions, the owner of the partition must be set to <code>hurd</code>.
#<code>mke2fs</code>, unless specifically overridden on the command line, will
#set the owner to whatever kernel it is running on. As the Hurd will diligently
#respect this setting, care must be taken to set this appropriately or the Hurd
#will fail in subtle ways. Be aware that even if a file system is owned by a
#particular kernel, others may still use it; they just may not be able to use
#certain extensions.
#</p>
#
#<p>
#To create a filesystem, use <code>mke2fs</code> and pass it <q><var>-b 4096
#-I 128 -o hurd</var></q> to designate the Hurd as the owner of the new file
#system. For instance, assuming the partition is <tt><q>/dev/hda1</q></tt>:
#</p>
#
#<table><tr><td>&nbsp;</td><td class=example><pre>
# \# mke2fs -b 4096 -I 128 -o hurd /dev/hda1
#</pre></td></tr></table>
#
#
#<h2> 3. The Boot Loader </h2>
#
#<p>
#The bootloader of the GNU system is Grub, the GRand Unified Boot loader,
#and it is its main purpose to boot the GNU system core (Mach and the Hurd).
#Any boot loader that supports the multiboot standard, though, may be used to
#load Mach and the Hurd. At the moment (that we're aware of), GNU Grub is the
#only bootloader that supports such standard.
#</p>
#
#<p>
#A word about Grub. Unlike traditional boot loaders on the x86, such as LILO,
#Grub is very powerful. It has a command line interface, bootp, dummy terminal
#support and a plethora of other features. In addition, it can boot almost any
#kernel. If you have ever booted an alpha or sparc, you will understand what
#Grub can do. Therefore, do not be scared: Grub is better. You will like it.
#You will not go back.
#</p>
#
#<p>
#It is probably better if you install
#<a href="https://packages.debian.org/grub2">Grub</a> before installing the
#Hurd, but you can always install it onto your hard drive at a later date.
#</p>
#
#
#<h2> 4. Cross Install </h2>
#
#<p>
#The next step is to get a base system. There are several alternatives, if you
#are running a Debian system you can use the package crosshurd. Otherwise you
#can get an updated base system tarball from any of the mirrors listed at
#<url "https://wiki.debian.org/DebianPorts/Mirrors" />.
#</p>
#
#<p>
#The tarball is set up to extract everything into the current directory.
#After the filesystem is mounted, the archive can be extracted.
#Assuming that the filesystem is on <tt><q>/dev/hda2</q></tt>, the mount point
#is <tt><q>/gnu</q></tt> and archive is in current user's home directory, the
#following is required:
#</p>
#
#<table><tr><td>&nbsp;</td><td class=example><pre>
# \# mount -t ext2 /dev/hda2 /gnu
# \# cd /gnu
# \# tar --same-owner -xvjpf ~/gnu.tar.bz2
#</pre></td></tr></table>
#
#
#<h2> 5. Booting GNU/Hurd </h2>
#
#<p>
#All is now in readiness to boot GNU/Hurd for the first time. Note that depending
#on your version of GRUB, small details with vary. Please make sure whether you
#have GRUB1 (aka legacy aka 0.95 and later up to 0.97), or GRUB2 (aka 1.95 and
#later). Examples below will provide both versions, make sure to pick the right
#one.
#</p>
#
#<p>
#Please also note that some problems have been reported when booting with only
#128MB memory, because of swap not being setup yet at this stage. Make sure to
#provide at least 256MB memory.
#</p>
#
#<p>
#If your system already has GRUB installed, just reboot it. Else, you need to use
#a Grub boot disk. On Debian, the grub-disk (grub1) or grub-rescue-pc (grub2)
#packages provides CD and Floppy disk images. Make sure that the Grub boot disk
#is in the drive, reboot. If all goes well, either a Grub menu or command line
#will be displayed. If the menu has a <tt><q>GNU/Hurd</q></tt> entry, you will
#probably be able to just reuse it to save a lot of typing burden. For now, if
#presented with a menu, press <kbd>c</kbd> to go to the command line.
#</p>
#
#<p>
#First, GNU Mach needs to be loaded. This requires knowing the filesystem
#and the path to GNU Mach. Grub uses a partition nomenclature that is a bit
#different from both Linux and the Hurd: both IDE and SCSI drives are named
#<tt><q>(hdN,M)</q></tt>. <code>N</code> is the drive number (zero based) as
#enumerated by the BIOS. That is, Grub makes no distinction between IDE and SCSI
#disks. <code>M</code> identifies the partition on the drive. It is zero based
#in grub1 but one based in grub2. If this sounds confusing, relax:  Grub is also
#helpful.
#</p>
#
#<p>
#To determine on which filesystem a particular file resides, Grub provides the
#<code>find</code> command. When this command is issued along with a filename,
#Grub searches on each filesystem for the specified file and prints where it was
#found. For example, to search for the kernel, <tt><q>/boot/gnumach.gz</q></tt>:
#</p>
#
#<table>
#<tr><td>&nbsp;</td><td class=example><pre>
#   grub1&#62; find /boot/gnumach.gz
#   (hd0,0)
#</pre></td></tr>
#<tr><td>&nbsp;</td><td class=example><pre>
#   grub2&#62; search -f /boot/gnumach.gz
#   (hd0,1)
#</pre></td></tr>
#</table>
#
#<p>
#Here, Grub1 indicates that <tt><q>/boot/gnumach.gz</q></tt> is on
#<tt><q>(hd0,0)</q></tt> and Grub2, on <tt><q>(hd0,1)</q></tt> (remember the difference
#in partition numbering between Grub1 and Grub2).  To save you a couple of
#typing, set Grub's root to that value:
#</p>
#
#<table>
#<tr><td>&nbsp;</td><td class=example><pre>
#   grub1&#62; root (hd0,0)
#</pre></td></tr>
#<tr><td>&nbsp;</td><td class=example><pre>
#   grub2&#62; set root=(hd0,1)
#</pre></td></tr>
#</table>
#
#<p>
#Before loading the kernel, at least one option, the root partition, must be
#specified on the command line. This will be used by the Hurd itself (i.e. not
#Grub). As such, it must be in terms that the Hurd can understand.
#</p>
#
#<p>
#GNU Mach enumerates disks starting at zero. IDE drives are prefixed with
#<code>hd</code>, while SCSI disks are prefixed with <code>sd</code>. Like
#Linux, drives are number by their position on the controller. For instance, the
#primary master is <code>hd0</code> and the secondary slave is <code>hd3</code>.
#Partitions use the BSD slice naming convention and append <code>sM</code> to the
#drive name to indicate a given partition. Note that <code>M</code> is a one, not
#zero, based index. The slice number is simple to calculate: if you have Grub2,
#just reuse the same index, if you have Grub1, just increment what was used for
#Grub1 by one.
#</p>
#
#<p>
#Since the Hurd has not yet been configured, it must be started in single user
#mode. Adding a <q><var>-s</var></q> to the kernel command line is all that is
#required.
#</p>
#
#<p>
#To sum it up, assuming that the first drive (i.e. <tt><q>(hd0)</q></tt>) is the
#master on the master controller, we would have:
#</p>
#
#<table>
#<tr><td>&nbsp;</td><td class=example><pre>
#   grub1&#62; kernel /boot/gnumach.gz root=device:hd0s1 -s
#   [Multiboot-elf, ...]
#</pre></td></tr>
#<tr><td>&nbsp;</td><td class=example><pre>
#   grub2&#62; multiboot /boot/gnumach.gz root=device:hd0s1 -s
#</pre></td></tr>
#</table>
#
#<p>
#Next, the root filesystem server and the <code>exec</code> server must be
#loaded. This is done using Grub's boot module capability. The ${var} are
#filled in by GNU Mach. The arguments are used by the Hurd to indicate what
#type of information is being provided. Since the ext2fs command line is very
#long, it can be broken up by escaping the newline character in the normal Unix
#fashion. Be sure that there is not space after the antislash at the end of each
#line. Also be sure to differentiate <kbd>{</kbd> and <kbd>}</kbd> from <kbd>
#(</kbd> and <kbd>)</kbd>. Mind the subttle differences between Grub1 and
#Grub2: Grub2 needs that the filename be repeated and quotes need
#to be used. Note that at this stage the <var>--readonly</var> option of
#<code>ext2fs.static</code> must not be passed.
#</p>
#
#<table>
#<tr><td>&nbsp;</td><td class=example><pre>
#  grub1&#62; module /hurd/ext2fs.static \
#   --multiboot-command-line=${kernel-command-line} \
#   --host-priv-port=${host-port} \
#   --device-master-port=${device-port} \
#   --exec-server-task=${exec-task} -T typed ${root} \
#   $(task-create) $(task-resume)
#    [Multiboot-module  0x1c4000, 0x2cfe6a bytes]
#  grub1&#62; module /lib/ld.so.1 /hurd/exec $(exec-task=task-create)
#    [Multiboot-module  0x494000, 0x27afe bytes]
#</pre></td></tr>
#<tr><td>&nbsp;</td><td class=example><pre>
#  grub2&#62; module /hurd/ext2fs.static ext2fs \
#   --multiboot-command-line='${kernel-command-line}' \
#   --host-priv-port='${host-port}' \
#   --device-master-port='${device-port}' \
#   --exec-server-task='${exec-task}' -T typed '${root}' \
#   '$(task-create)' '$(task-resume)'
#  grub2&#62; module /lib/ld.so.1 exec /hurd/exec '$(exec-task=task-create)'
#</pre></td></tr>
#</table>
#
#<p>
#Alternatively, you can throw these lines into a <tt><q>menu.lst</q></tt>
#(Grub1) or <tt><q>grub.cfg</q></tt> (Grub2) configuration file in the partition,
#and load it by using <tt><q>configfile /path/to/menu.lst</q></tt> (Grub1) or
#<tt><q>configfile /path/to/grub.cfg</q></tt> (Grub2) from the grub prompt. You
#can of course also simply install grub in some MBR and point it to there.
#</p>
#
#<p>
#GNU/Hurd can be now booted:
#</p>
#
#<table><tr><td>&nbsp;</td><td class=example><pre>
#grub&#62; boot
#</pre></td></tr></table>
#
#<p>
#If GNU/Hurd fails to boot, it could be due to shared IRQs: GNU Mach does not
#play well with these. You can verify your situation by looking at, for
#instance, the <tt><q>/proc/interrupts</q></tt> file under GNU/Linux. Also, as GNU
#Mach does not support loadable kernel modules, many of the drivers are compiled
#into the default kernel. If there are old peripherals, this can be a problem: a
#device may incorrectly respond to a probe intended for a completely unrelated
#device and thereby cause a crash. Building a new kernel with only the required
#device drivers will usually solve this problem. GNU Mach is easily cross
#compiled. If you are running Debian, install the <tt><q>mig</q></tt> package,
#and your stock <tt><q>gcc</q></tt> should do.
#</p>
#
#<p>
#If this does not help, ask on the appropriate mailing list.
#</p>
#
#
#<h2> 6. Native Install </h2>
#
#<p>
#Once you are presented with a shell prompt, and any time that the Hurd is in
#single user mode, it is necessary to set the terminal type:
#</p>
#
#<table><tr><td>&nbsp;</td><td class=example><pre>
# \# export TERM=mach
#</pre></td></tr></table>
#
#<p>
#Be warned that <kbd>CONTROL-C</kbd> and family will not work in single user
#mode.
#</p>
#
#<p>
#We can now run the <code>native-install</code> script. This will configure the
#packages and set up several important translators:
#</p>
#
#<table><tr><td>&nbsp;</td><td class=example><pre>
# \# ./native-install
#</pre></td></tr></table>
#
#<p>
#Before the script terminates, it will indicate that you can now reboot and enter
#multi-user mode. Do so, this is the Hurd, welcome!
#</p>


<h2> Instalação </h2>

<p>
Você simplesmente pode usar o instalador do Debian, veja as
<a href="hurd-cd">imagens preparadas de CD</a>.
Então os seguintes passos serão necessários para uma configuração apropriada.
</p>

<p>
Você também pode obter uma imagem pré-instalada e executá-la no qemu:
</p>

<table><tr><td>&nbsp;</td><td class=example><pre>
$ wget https://cdimage.debian.org/cdimage/ports/stable/hurd-i386/debian-hurd.img.tar.gz
$ tar xzf debian-hurd.img.tar.gz
$ kvm -m 1G -drive file=$(echo debian-hurd*.img),cache=writeback
</pre></td></tr></table>

<p>
Para habilitar o acesso à máquina através de ssh, você pode adicionar
</p>

<table><tr><td>&nbsp;</td><td class=example><pre>
-net nic -net user,hostfwd=tcp:127.0.0.1:2222-:22
</pre></td></tr></table>

<p>
e fazer ssh para sua porta TCP local  2222.
</p>

<p>
Você também pode converter a imagem para o formato VDI do virtualbox:
</p>

<table><tr><td>&nbsp;</td><td class=example><pre>
$ VBoxManage convertfromraw debian-hurd-*.img debian-hurd.vdi --format vdi
</pre></td></tr></table>

<h2> Configuração </h2>

<h3> A rede </h3>

<p>
O jeito Debian é suportado, iniciando em sysvinit 2.88dsf-48 e
hurd 1:0.5.git20140320-1: <tt>/etc/network/interfaces</tt> é usado como no
Linux. A única diferença é que as placas de rede aparecem em <tt>/dev</tt> e
as interfaces devem, portanto, ser especificadas como <tt>/dev/eth0</tt>, etc.
</p>

# Nota do tradutor: excerto ignorado na tradução, pelo menos por enquanto
#<p>
#First, make sure that your network card is recognized by GNU Mach:
#</p>
#
#<table><tr><td>&nbsp;</td><td class=example><pre>
# \# devprobe eth0
# eth0
#</pre></td></tr></table>
#
#<p>
#If <code>devprobe eth0</code> does not return <code>eth0</code>, the kernel
#didn't detect your network board and you need to try another board. For
#instance, qemu's e1000 board is not supported, the rtl8139 one should work:
#<code>-net nic,model=rtl8139 -net user</code>
#</p>

#<p>
#Starting from version 20120520-1, the hurd package includes DDE drivers which
#are used to support more recent devices (drivers are taken from Linux
#2.6.32). Translators for that are already set up for you, and all you
#need to do is to replace <code>eth0</code> (the mach driver name) with
#<code>/dev/eth0</code> (the DDE driver path) in the remainder of this document.
#</p>
#
#<p>
#It is possible to try to use the DDE driver even if GNU Mach has a driver:
#passing <code>nonetdev</code> on the gnumach command line will disable the GNU
#Mach driver, and the DDE driver will start working.
#</p>

<p>
Se a rede não funcionar, use o comando seguinte para obter informações de
depuração do driver DDE:
</p>

<table><tr><td>&nbsp;</td><td class=example><pre>
 \# settrans -fga /dev/netdde /hurd/netdde
</pre></td></tr></table>

<p>
e então dê um kill em qualquer processo devnode e pfinet para permitir que
reiniciem com o netdde mais novo. Se ainda não funcionar, por favor poste em um
relatório de bug a saída completa do comando settrans netdde acima, como
também a saída de <code>lspci</code> e <code>lspci -n</code>.
</p>

<p>
Para configurar a rede sem passar por <tt>/etc/network/interfaces</tt>,
o tradutor pfinet deve ser configurado.
Isto pode ser feito usando-se <code>dhclient</code> do
pacote <code>isc-dhcp-client</code>.
Isto também pode ser feito manualmente usando-se <code>inetutils-ifconfig</code>
do pacote <code>inetutils-tools</code>, e <code>ping</code> está
disponível no pacote <code>inetutils-ping</code>.
Por último, mas não menos importante, isto pode ser feito (e gravado para
sempre) manualmente usando-se o comando <code>settrans</code> para anexar um
tradutor para um dado node de sistema de arquivo. Quando os programas
acessarem via node, por exemplo enviando um RPC, o sistema operacional
transparentemente iniciará o servidor para lidar com a requisição.
</p>

<table><tr><td>&nbsp;</td><td class=example><pre>
 \# settrans -fgap /servers/socket/2 /hurd/pfinet -i /dev/eth0 \
   -a a.b.c.d -g e.f.g.h -m i.j.k.l
</pre></td></tr></table>

<p>
Aqui, o <code>settrans</code> é passado com diversas opções. As duas primeiras,
<q><var>fg</var></q>, força qualquer tradutor existente a remover-se. As duas
seguintes, <q><var>ap</var></q>, definem tradutores ativos e passivos. Ao fazer
o tradutor ativo, imediatamente veremos qualquer mensagem de erro em
<tt><q>stderr</q></tt>. A última grava o tradutor e os argumentos no node
para que ele possa ser transparentemente reiniciado mais tarde (isto é,
definindo as configurações como persistentes entre reinicializações). As opções
são seguidas pelo node para o qual o tradutor vai ser anexado, e então o programa
(ou seja, o tradutor) para executar e qualquer argumentos para ele. A opção
<q><var>-i</var></q> é a interface que <code>pfinet</code> ficará escutando,
<q><var>-a</var></q> é o endereço IP, <q><var>-g</var></q> é o
gateway e <q><var>-m</var></q> é a máscara de rede.
</p>

<p>
Certifique-se de adicionar os nomes de servidores em seu arquivo
<tt><q>/etc/resolv.conf</q></tt>:
</p>

<table><tr><td>&nbsp;</td><td class=example><pre>
  nameserver 192.168.1.1
</pre></td></tr></table>

<p>
Para testar a configuração, <code>ping -c2 gateway</code>. A opção
<q><var>-c</var></q> é importante para limitar o número de pings; lembre-se,
<kbd>CONTROL-C</kbd> não funciona no modo de usuário(a) único(a).
</p>

<p>
Ajuda para <code>settrans</code> pode ser obtida ao passar para ele a opção
<q><var>--help</var></q>. Ajuda sobre um tradutor específico pode ser obtida
ao invocá-la na linha de comando com o mesmo argumento, por exemplo:
</p>

<table><tr><td>&nbsp;</td><td class=example><pre>
 \# /hurd/pfinet --help
</pre></td></tr></table>

<p>
Como pode haver uma grande quantidade de saídas, considere fazer um
direcionamento para um paginador como o <code>less</code>.
</p>

<p>
Para também configurar o suporte ao IPv6, a mesma configuração tem que ser
gravada tanto em <tt>/servers/socket/2</tt> como em <tt>/servers/socket/26</tt>,
referenciando um ao outro de modo que somente um seja iniciado, ligando ambos
os nós:
</p>

<table><tr><td>&nbsp;</td><td class=example><pre>
 \# settrans -fgap /servers/socket/2 /hurd/pfinet -6 /servers/socket/26 -i /dev/eth0 \
   -a a.b.c.d -g e.f.g.h -m i.j.k.l
 \# settrans -p /servers/socket/26 /hurd/pfinet -4 /servers/socket/2 -i /dev/eth0 \
   -a a.b.c.d -g e.f.g.h -m i.j.k.l
</pre></td></tr></table>

<p>
O servidor pfinet habilita a autoconfiguração IPv6 por padrão. O estado atual
pode ser obtido de <tt>fsysopts /servers/socket/26</tt>. Endereços também podem
ser definidos manualmente, usando-se, por exemplo,
<tt>-A 2001:123:123::42/64 -G 2001:123:123::1</tt>.
</p>

<p>
A configuração do pfinet também pode ser alterada em tempo real
(sem gravação em disco) usando-se <tt>fsysopts</tt>:
</p>

<table><tr><td>&nbsp;</td><td class=example><pre>
 \# fsysopts /servers/socket/2
 /hurd/pfinet --interface=/dev/eth0 --address=10.3.0.1 --netmask=255.255.0.0 --gateway=10.3.0.128
 \# fsysopts /server/socket/2 -a 10.3.0.2 -m 255.255.0.0 -g 10.3.0.128
</pre></td></tr></table>

<p>
Um firewall pode ser estabelecido pela interposição do tradutor
<tt>eth-filter</tt>; por exemplo, isto previne o acesso à porta 22:
</p>

<table><tr><td>&nbsp;</td><td class=example><pre>
 \# settrans -c /dev/eth0f /hurd/eth-filter -i /dev/eth0 -r "not port 22"
</pre></td></tr></table>

<p>
O dispositivo filtrado, <tt>/dev/eth0f</tt>, pode então ser dado para
<tt>pfinet</tt> ou <tt>dhclient</tt> em vez de /dev/eth0.
</p>


<h3> Layout do teclado </h3>

<p>
O layout do teclado pode ser configurado através do pacote
padrão <code>keyboard-configuration</code>. Certifique-se de que esteja
instalado e execute <code>dpkg-reconfigure keyboard-configuration</code>.
Somente o layout é suportado, variantes (ainda) não. O efeito não será
imediato, já que o console precisa ser reiniciado para considerar o
parâmetro. Reiniciar deve ser suficiente.
</p>

<h3> Outros sistemas de arquivo </h3>

<p>
A seguir, edite <tt><q>/etc/fstab</q></tt> para adicionar quaisquer sistemas
de arquivo como também espaço de swap. É <em>muito importante</em> que um
espaço de swap seja usado; o Hurd ficará em uma ordem de magnitude mais
estável. Note que o Hurd pode, transparentemente, compartilhar uma partição
swap com o Linux, mas alegremente paginará para qualquer dispositivo,
incluindo uma partição não processada como sua partição home. Por padrão,
<code>nano</code> e <code>vi</code> são os únicos editores instalados pela
distribuição base.
</p>

<p>
Aqui está um exemplo do arquivo <tt><q>/etc/fstab</q></tt>:
</p>

<table><tr><td>&nbsp;</td><td class=example><pre>
\# &#60;file system&#62; &#60;mount point&#62;   &#60;type&#62;  &#60;options&#62;  &#60;dump&#62;  &#60;pass&#62;
/dev/hd0s1      /               ext2    rw         0       1
/dev/hd0s2      /home           ext2    rw         0       2
/dev/hd0s3      none            swap    sw         0       0
</pre></td></tr></table>

<p>
Se qualquer entrada de dispositivo <code>/dev</code> estiver faltando,
lembre-se de criá-la usando o comando <code>MAKEDEV</code>:
</p>

<table><tr><td>&nbsp;</td><td class=example><pre>
 \# cd /dev
 \# ./MAKEDEV hd0s1 hd0s2 hd0s3
</pre></td></tr></table>

<p>
Você também pode montar um sistema de arquivo manualmente ao chamar
<code>settrans</code>:
</p>

<table><tr><td>&nbsp;</td><td class=example><pre>
 \# settrans /mnt /hurd/ext2fs /dev/hd0s5
</pre></td></tr></table>

<p>
A ideia por trás deste comando é que você defina no node <code>/mnt</code> o
tradutor <code>/hurd/ext2fs /dev/hd0s5</code>. O <code>/hurd/ext2fs</code>
será executado e iniciará a leitura/escrita de <code>/dev/hd0s5</code> e
exibirá seu conteúdo em <code>/mnt</code>. Mais informações podem ser
encontradas na <a href="hurd-doc-translator">documentação de tradutor</a>.
</p>

<p>
Para montar um sistema de arquivos nfs, o tradutor <code>/hurd/nfs</code> é
usado. Quando executado como não root, o tradutor conectará ao servidor
usando uma porta acima de 1023. Por padrão, o GNU/Linux rejeitará isso. Para
dizer ao GNU/Linux que aceite conexões originadas de uma porta não reservada,
adicione a opção <q><var>insecure</var></q> para linha de exportação. Aqui está
um exemplo do arquivo <tt><q>/etc/exports</q></tt>, assumindo que o endereço
IP do cliente é <tt><q>192.168.1.2</q></tt>:
</p>

<table><tr><td>&nbsp;</td><td class=example><pre>
  /home  192.168.1.2(rw,insecure)
</pre></td></tr></table>

<p>
Para montar de uma máquina GNU e assumindo que o endereço IP do servidor nfs é
<tt><q>192.168.1.1</q></tt>:
</p>

<table><tr><td>&nbsp;</td><td class=example><pre>
\# settrans -cga /mount/point /hurd/nfs 192.168.1.1:/home
</pre></td></tr></table>


<h2> Divirta-se com o Debian GNU/Hurd </h2>

<p>
Agora, que coisas legais nós podemos fazer com o Hurd?
</p>

<h3> Montar imagens de disco </h3>

<p>
Acessar o conteúdo de uma imagem de CD é um pouco tedioso com sistemas padrão
Unix se você não é root. No GNU/Hurd, fica assim:
</p>

<table><tr><td>&nbsp;</td><td class=example><pre>
settrans ~/mnt /hurd/iso9660fs CD_image.iso
</pre></td></tr></table>

<p>
E é completamente seguro: o tradutor <code>iso9660fs</code> está rodando
sob sua identidade, não root. Você pode até mesmo codificar seu próprio
tradutor para qualquer tipo de sistema de arquivos. Sim, isso é como o FUSE.
Sem toda a gambiarra.
</p>

<h3> FTP transparente </h3>

<p>
A seguir define-se um diretório <code>ftp</code> transparente:
</p>

<table><tr><td>&nbsp;</td><td class=example><pre>
settrans -c /ftp: /hurd/hostmux /hurd/ftpfs /
</pre></td></tr></table>

<p>
Agora, faça <code>cd</code> para, por exemplo, <code>/ftp://ftp.gnu.org/</code>
e execute <code>ls</code> lá.
Sim, você pode, de sua home, simplesmente rodar
<code>tar xf ftp://ftp.gnu.org/pub/gnu/gcc/gcc-4.6.0/gcc-4.6.0.tar.bz2</code> !
</p>

<h3> Sub-Hurd </h3>

<p>
Um <a href="https://www.gnu.org/software/hurd/hurd/subhurd.html">sub-Hurd</a>
é um subsistema completo. Muito parecido com containers de virtualização
à primeira vista. Exceto que você não precisa ser root para executá-lo.
</p>

<h3> gdb ext2fs, pfinet, ... </h3>

<p>
Sim, você pode rodar gdb, por exemplo, na implementação ext2fs, na pilha TCP/IP
do <code>pfinet</code>, etc.
</p>

<h3> E muito mais coisas! </h3>

<p>
Alguns trabalhos em andamento incluem <code>mboxfs</code>, <code>tarfs</code>,
<code>xmlfs</code>, <code>gopherfs</code>, ...
</p>

# Nota do tradutor: excerto ignorado na tradução, pelo menos por enquanto
#<h3> 7.3 Rebooting </h3>
#
#<p>
#Finally, reboot into multiuser mode, i.e. in the same way single user mode was
#brought up minus the <q><var>-s</var></q> option when loading the kernel. For
#details, see section 5. Booting GNU/Hurd.
#</p>
#
#<p>
#Happy Hacking!
#</p>

<h2>Palavras finais </h2>

<p>
A seguir estão somente dicas rápidas de instalação, certifique-se de também
ler a documentação para o sistema instalado: a
<a href=hurd-doc>documentação do Debian GNU/Hurd</a>, mas também o
<a href=http://hurd.gnu.org/>site web dos(as) autores(as)</a>.
</p>


#<h3> 8.1 The Grub Menu </h3>
#
#<p>
#Having to always load the kernel by hand can be very tedious. Edit the
#<tt><q>/boot/grub/menu.lst</q></tt> for Grub1 or
#<tt><q>/boot/grub/grub.cfg</q></tt> for Grub2 and tailor it appropriately;
#booting will become much quicker and easier.
#</p>
#
#
#<h3> 8.2 Adding Devices </h3>
#
#<p>
#By default, only a few devices are created in the <tt><q>/dev</q></tt> directory.
##Use the <code>MAKEDEV</code> script to create any needed device nodes.
#</p>

<h3><a name=morepackages>Instalando mais pacotes</a></h3>

<p>
Existem vários modos de se adicionar pacotes. Fazer o download e usar
<code>dpkg -i</code> funciona, mas é muito inconveniente. O método
mais fácil é usar <code>apt</code>.
</p>

<p>
Se você tem usado o Debian GNU/Hurd lançado em 2021, o modo mais
seguro é usar o snapshot desta versão ao criar um arquivo
<code>/etc/apt/apt.conf.d/99ignore-valid-until</code> contendo:
</p>

<table><tr><td>&nbsp;</td><td class=example><pre>
Acquire::Check-Valid-Until "false";
</pre></td></tr></table>

<p>
E então o snapshot pode ser usado com fonte para o apt: edite
<tt><q>/etc/apt/sources.list</q></tt>, adicione a seguinte entrada não lançada.
</p>

<table><tr><td>&nbsp;</td><td class=example><pre>
deb [trusted=yes] https://snapshot.debian.org/archive/debian-ports/20210812T100000Z/ sid main
deb [trusted=yes] https://snapshot.debian.org/archive/debian-ports/20210812T100000Z/ unreleased main
deb-src [trusted=yes check-valid-until=no] https://snapshot.debian.org/archive/debian/20210812T100000Z/ sid main
</pre></td></tr></table>

<p>
Atualize, instale o pacote <code>debian-ports-archive-keyring</code>
e atualize novamente; agora você tem uma versão completa do Debian GNU/Hurd
2021 disponível.
</p>


<p>
Se você tem usado um snapshot mais antigo que o lançamento de 2021, você pode
adicionar essas fontes para obter os pacotes mais recentes:
</p>

<table><tr><td>&nbsp;</td><td class=example><pre>
deb http://deb.debian.org/debian-ports unstable main
deb-src http://deb.debian.org/debian unstable main
deb http://deb.debian.org/debian-ports unreleased main
</pre></td></tr></table>

<p>
Atualize, instale o pacote <code>debian-ports-archive-keyring</code> e
atualize novamente.
</p>

<p>
Se ao fazer o primeiro <code>apt</code>, o <code>dpkg</code> reclamar de
programas ausentes, entre como root no login do shell (isto é,
<code>su -</code>, não somente <code>su</code>).
</p>

<p>
Se o GNU Mach não reconhecer sua placa de rede ou você usa um modem, o único
modo de atualizar será fazer o download dos pacotes e então transferi-los
para o sistema GNU. A maneira mais fácil de fazer isso é usar o apt
off-line. Consulte
<tt><q>/usr/share/doc/apt-doc/offline.text.gz</q></tt> para instruções
detalhadas.
</p>


<h3>O console Hurd</h3>

<p>
Além do console do Mach que você encontrou durante a instalação, o GNU/Hurd
apresenta um console poderoso de espaço de usuário(a), fornecendo terminais
virtuais. Se você instalou em um modo pseudográfico, ele deve iniciar
automaticamente no boot, se não você pode iniciá-lo manualmente com o
seguinte comando:
</p>

<table><tr><td>&nbsp;</td><td class=example><pre>
 \# console -d vga -d pc_mouse --repeat=mouse -d pc_kbd --repeat=kbd -d generic_speaker -c /dev/vcs
</pre></td></tr></table>

<p>
Se confirmar que está funcionando, pode-se habilitá-lo no boot em
<tt>/etc/default/hurd-console</tt>: mude de <tt>ENABLE="false"</tt> para
<tt>ENABLE="true"</tt>.
</p>

<p>
De dentro do console do Hurd, você pode alternar entre terminais virtuais via
<kbd>ALT+F1</kbd>, <kbd>ALT+F2</kbd> e assim por diante.
<kbd>ALT+CTRL+BACKSPACE</kbd> desprende o console do Hurd e te traz de volta
ao console do Mach, de onde você pode reanexá-lo com o comando acima.
</p>


<h3>X.Org </h3>

<p>
O X.Org foi portado e todas as placas de vídeo que ele suporta, e que não
requerem um módulo de kernel ou drm, devem funcionar.
</p>

<p>
Você já deve estar executando o console do Hurd e ter os repetidores
configurados como indicado na seção anterior. Por exemplo, verifique se
<code>echo $TERM</code> mostra <code>hurd</code>, e verifique se
<code>/dev/cons/kbd</code> e <code>/dev/cons/mouse</code> existem.
</p>

<p>
Você precisa rodar <tt>dpkg-reconfigure x11-common xserver-xorg-legacy</tt>
para permitir que qualquer usuário(a) inicie o Xorg, porque o wrapper do X
não conhece os consoles do Hurd e do Mach.
</p>

<p>
Você também precisa criar um <tt>/etc/X11/xorg.conf</tt> para habilitar o
atalho control-alt-backspace:
</p>

<table><tr><td>&nbsp;</td><td class=example><pre>
Section "InputDevice"
   Identifier "Generic Keyboard"
   Driver "kbd"
   Option "XkbOptions" "terminate:ctrl_alt_bksp"
EndSection
</pre></td></tr></table>

<p>
Pode acontecer que, por alguma razão, o Xorg escolha uma resolução 16/9, mas
o tamanho da área de trabalho é 4/3. Culpe o Xorg, não o Hurd :) Para evitar
esse problema, adicione isto ao <tt>/etc/X11/xorg.conf</tt> :
</p>

<table><tr><td>&nbsp;</td><td class=example><pre>
Section "Screen"
   Identifier "myScreen"
   SubSection "Display"
      Virtual 1024 768
   EndSubSection
EndSection
</pre></td></tr></table>

<p>
Você precisará de diversos pacotes do X; <code>xorg</code>,
<code>rxvt</code> e um gerenciador de janelas: <code>twm</code>,
<code>icewm</code>, <code>openbox</code>, ... são um bom começo.
Se quiser que o X inicie no boot, você tem que instalar um
gerenciador de display. O <code>lightdm</code> e o <code>gdm</code> ainda
não funcionam, mas o <code>xdm</code> deve funcionar tranquilamente.
</p>

<p>
Finalmente, execute <code>startx /usr/bin/yourwm</code>
</p>

<p>
Se isso não funcionar, como mencionado pela mensagem de erro, olhe em
<tt>/var/log/Xorg.0.log</tt> (ou poste-a na lista para que as pessoas deem
uma olhada).
</p>

<h3>Atualizand o seu sistema</h3>

<p>
Se você estiver usando uma versão snapshot do Debian, você não terá nenhuma
atualização disponível, uma vez que a versão lançada é congelada na data de
lançamento.
<b>Isso significa que você não terá atualizações de segurança!</b> Você pode
preferir habilitar a versão instável (unstable) conforme descrito na seção
<a href=#morepackages>instalando mais pacotes </a>. 
</p>

<p>
Depois de habilitar a versão instável (unstable), observe que, por ser instável,
ela é afetada por interrupções na transição da biblioteca, portanto, não se
surpreenda que algumas vezes você não será capaz de atualizar alguns pacotes.
Geralmente, você pode usar o procedimento recomendado de atualização do Debian:
primeiro use 
</p>

<table><tr><td>&nbsp;</td><td class=example><pre>
\# apt upgrade --without-new-pkgs
</pre></td></tr></table>

<p>para atualizar o que pode ser atualizado sem alterar a lista de pacotes e, em seguida, use</p>

<table><tr><td>&nbsp;</td><td class=example><pre>
\# apt full-upgrade
</pre></td></tr></table>

<p>para atualizar o resto.</p>

<p>Nota: se você raramente atualiza seu sistema, você pode ter problemas para
atualizar. Certifique-se de primeiro atualizar para a versão snapshot mais
recente (Hurd 2021, consulte a seção <a href=#morepackages>instalando mais pacotes</a>)
antes de atualizar da versão instável (unstable).</p> 

<h3>Últimas palavras</h3>

<p>
Para desligar seu sistema, simplesmente use <code>halt</code>,
<code>poweroff</code> ou <code>reboot</code>. Se algumas vezes acontecer
de travar porque algum daemon não foi terminado apropriadamente, em vez disso
você pode usar <code>halt-hurd</code>, <code>poweroff-hurd</code>,
<code>reboot-hurd</code>, que na verdade não desligam os daemons, mas
sincronizam os dados para o disco de modo apropriado.
</p>
