#use wml::debian::template title="기여: 어떻게 데비안을 도와주나요" MAINPAGE="true"
#use wml::debian::translation-check translation="d20fb9052ff8188ede3f9401187cb5ccfd2cdd43" maintainer="Sebul"
#
<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<div id="toc">
  <ul class="toc">
    <li><a href="#coding">코딩 및 패키지 유지</a></li>
    <li><a href="#testing">테스트 및 버그 줄이기</a></li>
    <li><a href="#documenting">문서 쓰기 및 패키지 태깅</a></li>
    <li><a href="#translating">번역 및 지역화</a></li>
    <li><a href="#usersupport">다른 사용자 돕기</a></li>
    <li><a href="#events">이벤트 조직하기</a></li>
    <li><a href="#donations">금전, 하드웨어 대역폭 기부</a></li>
    <li><a href="#usedebian">데비안 사용</a></li>
    <li><a href="#organizations">조직에서 데비안을 어떻게 지원하나요</a></li>
  </ul>
</div>

<p>
데비안은 단순한 운영 체제가 아니라 커뮤니티입니다. 
다양한 기술을 가진 많은 사람들이 프로젝트에 기여합니다:
우리 소프트웨어, 아트워크, 위키 및 기타 문서는 많은 개인 그룹의 공동 노력의 결과입니다. 
모든 사람이 개발자는 아니며 참여하려면 코딩 방법을 알아야 하는 건 아닙니다. 
데비안을 더 좋게 만드는 데 도움이 될 수 있는 다양한 방법이 있습니다. 
참여하고 싶다면 경험 있는 사용자와 경험 없는 사용자 모두를 위한 몇 가지 제안 사항이 있습니다.</p>

<h2><a id="coding">코딩 및 패키지 유지</a></h2>

<aside class="light">
  <span class="fa fa-code-branch fa-5x"></span>
</aside>

<p>새로운 애플리케이션을 처음부터 작성하고 싶을 수도 있고, 
기존 프로그램에 새로운 기능을 구현하고 싶을 수도 있습니다. 
개발자이고 데비안에 기여하고 싶다면 데비안에서 소프트웨어를 쉽게 설치할 수 있도록 준비하는 데 도움을 줄 수 있습니다. 
이를 "패키징"이라고 합니다. 
시작하는 방법에 대한 몇 가지 아이디어는 다음 목록을 참조하십시오:</p>

<ul>
  <li>패키지 응용 프로그램(예: 데비안에 경험이 있거나 가치 있다고 생각하는 응용 프로그램). 
  패키지 관리자가 되는 방법에 대한 자세한 내용은 <a href="$(HOME)/devel/">데비안 개발자 코너</a> 방문하십시오.</li>
  <li>기존 애플리케이션을 유지하는 것 돕기, 예를 들어
  <a href="https://bugs.debian.org/">Bug Tracking System</a>수정 (패치) 또는 추가 정보 제공. 
 또는, 그룹 유지관리 팀에 참여하거나 <a href="https://salsa.debian.org/">Salsa</a> (우리 자체 GitLab 인스턴스) 프로젝트 참여.
  </li>
  <li>데비안 <a href="$(HOME)/security/">보안 이슈</a>를 
  <a href="https://security-tracker.debian.org/tracker/data/report">추적</a>, 
  <a href="$(HOME)/security/audit/">찾기</a>, 및
  <a href="$(DOC)/manuals/developers-reference/pkgs.html#bug-security">고치기</a>로 우리를 도와주세요.</li>
  <li><a href="https://wiki.debian.org/Hardening">패키지</a>, 
  <a href="https://wiki.debian.org/Hardening/RepoAndImages">저장소 및 이미지</a>, 그리고 
  <a href="https://wiki.debian.org/Hardening/Goals">다른 컴포넌트</a> 강화를 도울 수 있습니다.</li>
  <li>데비안을 당신이 친한 아키텍처로 <a href="$(HOME)/ports/">포팅</a> 하는 데 관심있나요? 
  새 포트를 시작하거나 기존 포트에 기여할 수 있습니다.</li>
  <li>커뮤니티에서 <a href="https://wiki.debian.org/Services#wishlist">제안하거나 요청한</a> 
  데비안 관련 <a href="https://wiki.debian.org/Services">서비스</a>를 개선하거나 만들고, 
  유지하는 것을 도와주세요.</li>
</ul>

<h2><a id="testing">테스트 및 버그 줄이기</a></h2>

<aside class="light">
  <span class="fa fa-bug fa-5x"></span>
</aside>

<p>다른 소프트웨어 프로젝트와 마찬가지로 
데비안에는 운영 체제와 응용 프로그램을 테스트하는 사용자가 필요합니다. 
기여하는 한 가지 방법은 최신 버전을 설치하고 제대로 작동하지 않으면 개발자에게 다시 보고하는 것입니다. 
또한 다른 하드웨어에서 설치 미디어, 보안 부팅 및 U-Boot 부트로더를 테스트할 사람이 필요합니다.</p>

<ul>
  <li><a href="https://bugs.debian.org/">Bug Tracking System</a>을 써서
데비안에서 발견한 문제를 보고할 수 있습니다. 
그 전에, 버그가 이미 보고되었는지 확인하세요.</li>
  <li>버그 추적기를 방문하여 사용하는 패키지와 관련된 버그를 찾아보십시오.
  추가 정보를 제공하고 설명된 문제를 재현할 수 있는지 확인하십시오.
  </li>
  <li>데비안 <a href="https://wiki.debian.org/Teams/DebianCD/ReleaseTesting">설치 프로그램 및 라이브 ISO 이미지</a>, 
  <a href="https://wiki.debian.org/SecureBoot/Testing">보안 부트 지원</a>, 
  <a href="https://wiki.debian.org/LTS/TestSuites">LTS 업데이트</a>, 그리고
  <a href="https://wiki.debian.org/U-boot/Status">U-Boot</a> 부트로더를 테스트하세요.</li>
</ul>

<h2><a id="documenting">문서 쓰기 및 패키지 태깅</a></h2>

<aside class="light">
  <span class="fa fa-file-alt fa-5x"></span>
</aside>

<p>데비안에서 문제가 발생하고 문제를 해결하기 위한 코드를 작성할 수 없으면 
메모를 하고 해결책을 적어 두는 것이 좋습니다. 
그렇게 하면 비슷한 문제가 있는 다른 사용자를 도울 수 있습니다. 
모든 데비안 문서는 커뮤니티 회원이 작성했으며 여러 가지 방법으로 도움을 줄 수 있습니다.</p>

<ul>
  <li><a href="$(HOME)/doc/ddp">데비안 문서 프로젝트</a>에 참여하여 
  공식 데비안 문서를 도우세요.</li>
  <li><a href="https://wiki.debian.org/">데비안 위키</a>에 기여하세요.</li>
  <li><a href="https://debtags.debian.org/">Debtags</a> 웹사이트에서 패키지에 태그를 지정하고 분류 해서, 
  데비안 사용자가 소프트웨어를 쉽게 찾게 도와주세요.</li>
</ul>


<h2><a id="translating">번역 및 지역화</a></h2>

<aside class="light">
  <span class="fa fa-language fa-5x"></span>
</aside>

<p>모국어는 영어가 아니지만 
웹 페이지, 문서 등과 같은 데비안 관련 정보나 소프트웨어를 이해하고 번역할 수 있을 만큼 
충분한 영어 실력을 갖고 있나요? 
번역 팀에 합류하여 데비안 응용 프로그램을 모국어로 변환하지 않으시겠습니까? 
또한, 기존 번역을 확인하고 필요한 경우 버그 보고서를 제출할 사람을 찾고 있습니다.</p>

# Translators, link directly to your group's pages
<ul>
  <li>데비안 국제화 관련 모든 것을 
  <a href="https://lists.debian.org/debian-i18n/">i18n 메일링 리스트</a>에서 
  (한국어 데비안 관련 내용은 <a href="https://lists.debian.org/debian-l10n-korean/">한국어 데비안 메일링 리스트</a>에서)
  논합니다.</li>
  <li>아직 데비안이 지원하지 않는 언어를 쓰나요? 
  <a href="$(HOME)/international/">데비안 국제</a> 페이지를 통해 연락하세요.</li>
</ul>

<h2><a id="usersupport">다른 사용자 돕기</a></h2>

<aside class="light">
  <span class="fas fa-handshake fa-5x"></span>
</aside>

<p>다른 데비안 사용자를 도와 프로젝트에 기여할 수도 있습니다. 
이 프로젝트는 다양한 지원 채널(예: 다른 언어의 메일링 리스트 및 IRC 채널)을 사용합니다.
더 자세한 정보는,
<a href="$(HOME)/support">지원 페이지</a> 참조.</p>

# Translators, link directly to the translated mailing lists and provide
# a link to the IRC channel in the user's language
<ul>
  <li>데비안 프로젝트는 여러 다른 
  <a href="https://lists.debian.org/">메일링 리스트</a>를 씁니다. 
  일부는 개발자용 일부는 사용자용. 
  숙련된 사용자는 <a href="$(HOME)/support#mail_lists">사용자 메일링 리스트</a>를 통해
  다른 사람을 도울 수 있습니다.</li>
  <li>전세계 사람들이 IRC(Internet Relay Chat)에서 실시간 채팅 합니다. 
  채널 <tt>#debian</tt>을 <a href="https://www.oftc.net/">OFTC</a>에서 방문하여 다른 데비안 사용자와 채팅하세요.</li>
</ul>

<h2><a id="events">이벤트 조직하기</a></h2>

<aside class="light">
  <span class="fas fa-calendar-check fa-5x"></span>
</aside>

<p>연례 데비안 회의(DebConf) 외에도 매년 여러 국가에서 소규모 모임과 모임이 있습니다. 
<a href="$(HOME)/events/">이벤트</a>에 참여하거나 주최하는 것은 
다른 데비안 사용자와 개발자를 만날 수 있는 좋은 기회입니다.</p>

<ul>
  <li>매년 <a href="https://debconf.org/">데비안 컨퍼런스</a>, 
  예를 들어 연설 및 프리젠테이션 <a href="https://video.debconf.org/">비디오</a> 녹화, 
  참여자 인사 및 연사 지원, 
  DebConf 중 특별 이벤트 (치즈 및 와인 파티), 
  설정 및 분해 지원, 등을 통해 데비안을 돕습니다.</li>
  <li>게다가, 여러 <a href="https://wiki.debian.org/MiniDebConf">MiniDebConf</a> 
  이벤트, 데비안 프로젝트 멤버가 조직한 지역 모임.</li>
  <li><a href="https://wiki.debian.org/LocalGroups">지역 데비안 그룹</a>을 정기 모임 또는 기타활동을 통해 만들거나 가입할 수 있습니다.</li>
  <li>또한, <a href="https://wiki.debian.org/DebianDay">데비안 데이 파티</a>, 
  <a href="https://wiki.debian.org/ReleaseParty">릴리스 파티</a>, 
  <a href="https://wiki.debian.org/BSP">버그 줄이기 파티</a>, 
  <a href="https://wiki.debian.org/Sprints">개발 스프린트</a>, 또는 전세계의
  <a href="https://wiki.debian.org/DebianEvents">다른 이벤트</a>를 확인하세요.</li>
</ul>

<h2><a id="donations">금전, 하드웨어, 또는 대역폭 기부</a></h2>

<aside class="light">
  <span class="fas fa-gift fa-5x"></span>
</aside>

<p>데비안 프로젝트에 대한 모든 기부는 데비안 프로젝트 리더(DPL)가 관리합니다. 
당신의 지원으로 하드웨어, 도메인, 암호화 인증서 등을 구입할 수 있습니다. 
또한 DebConf 및 MiniDebConf 이벤트, 개발 스프린트, 다른 이벤트 참석 및 기타 활동을 후원하는 데 자금을 씁니다.</p>

<ul>
  <li>데비안 프로젝트에 자금, 장비 및 서비스를 <a href="$(HOME)/donations">기부</a>할 수 있습니다.</li>
  <li>우리는 <a href="$(HOME)/mirror/">미러</a>를 전세계적으로 찾고 있습니다.</li>
  <li>데비안 포트는, <a href="$(HOME)/devel/buildd/">autobuilder network</a>에 의존합니다.</li>
</ul>

<h2><a id="usedebian">데비안 사용 및 이야기하기</a></h2>

<aside class="light">
  <span class="fas fa-bullhorn fa-5x"></span>
</aside>

<p>데비안과 데비안 커뮤니티에 대해 널리 알리고 다른 사람들에게 알리십시오.
다른 사용자에게 운영 체제를 추천하고 설치 방법을 보여주세요.
단순히 사용하고 즐기세요 -- 
이것이 아마도 데비안 프로젝트에 되돌려주는 가장 쉬운 방법일 겁니다.</p>

<ul>
  <li>다른 사용자에게 설명하고 시연하여 데비안을 홍보하는 것을 도우세요.</li>
  <li>우리 <a href="https://www.debian.org/devel/website/">웹사이트</a>에 기여하고 
  데비안의 대중적 면을 개선하는 것을 도우세요.</li>
  <li><a href="https://wiki.debian.org/ScreenShots">스크린샷</a>을 받고 그것을
  <a href="https://screenshots.debian.net/">screenshots.debian.net</a>에
  <a href="https://screenshots.debian.net/upload">업로드</a>하세요 
  그래서 데비안 소프트웨어를 사용하기 전에 데비안에서 어떻게 보이는지 알 수 있도록.</li>
  <li><a href="https://packages.debian.org/popularity-contest">popularity-contest submissions</a>을 가능하게 해서 
  어떤 소프트웨어가 모두에게 인기있고 쓸모 있는지 알 수 있습니다.</li>
</ul>

<h2><a id="organizations">조직에서 데비안을 어떻게 지원하나요</a></h2>

<aside class="light">
  <span class="fa fa-check-circle fa-5x"></span>
</aside>

<p>교육, 상업, 비영리 또는 정부 기관에서 근무하든 당신 자원으로 우리를 지원할 수 있는 다양한 방법이 있습니다.
</p>

<ul>
  <li>예를 들어, 조직에서 금전 또는 하드웨어를 <a href="$(HOME)/donations">기부</a>하세요.</li>
  <li>우리 회의를 <a href="https://www.debconf.org/sponsors/">후원</a>하고 싶을 수 있습니다.</li>
  <li>조직에서 
  <a href="https://wiki.debian.org/MemberBenefits">데비안 기여자를 위한 제품 또는 서비스</a>를 
  제공할 수 있습니다.</li>
  <li>우리는 <a href="https://wiki.debian.org/ServicesHosting#Outside_the_Debian_infrastructure">무료 호스팅</a>도 찾습니다.</li>
  <li>물론, <a href="https://www.debian.org/mirror/ftpmirror">software</a> 미러 설정, <a
href="https://www.debian.org/CD/mirroring/">설치 미디어</a>, 
또는 <a href="https://wiki.debconf.org/wiki/Videoteam/Archive">컨퍼런스 비디오</a>도 고맙습니다.</li>
  <li>데비안 <a href="https://www.debian.org/events/merchandise">상품</a>, 
  <a href="https://www.debian.org/CD/vendors/">설치 미디어</a>, 또는 
  <a href="https://www.debian.org/distrib/pre-installed">미리설치된 시스템</a> 판매도 고려할 수 있습니다.</li>
  <li>조직에서 <a href="https://www.debian.org/consultants/">컨설팅</a> 또는 
  <a href="https://wiki.debian.org/DebianHosting">호스팅</a>을 지원하면, 우리에게 알려주세요.</li>
</ul>

<p>우리는 <a href="https://www.debian.org/partners/">파트너쉽</a>을 형성하는 데 관심있습니다. 
If you can
promote Debian by <a href="https://www.debian.org/users/">기념품 제공</a>으로 데비안을 홍보하거나, 
기관의 서버 또는 데스크톱에서 돌리거나, 
직원이 우리 프로젝트에 참여하도록 권장할 수 있다면, 그것은 환상적입니다.
데비안 운영체제와 커뮤니티에 대해 가르치고,
근무시간 동안 팀이 기여하도록 지시하거나, 
우리 <a href="$(HOME)/events/">이벤트</a>중 하나로 보낼 수 있습니다.</p>
