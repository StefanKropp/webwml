#use wml::debian::translation-check translation="4403b53e0cf3fdf0bdff2722dc29d4b864c7e8f6" mindelta="1"
<define-tag description>sikkerhedsopdatering</define-tag>
<define-tag moreinfo>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-4122">CVE-2021-4122</a>

    <p>Milan Broz, vedligeholderenr, opdagede et problem i cryptsetup, 
    opsætningsværktøjet i Linux til diskkryptering.</p>

    <p>LUKS2's (et på disken-format) online-genkryptering er en valgfri 
    udvidelse, der gør det mulig for en bruger at ændre nøglen til 
    data-genkryptering, samtidigmed at dataenheden er tilgængelig for brug 
    under hele genkrypteringsprocessen.</p>

    <p>En angriber kunne ændre metadata på disken, til at simulere en 
    igangværende dekryptering med et nedbrudt (uafsluttet) genkrypteringstrin 
    og vedvarende dekryptere dele af LUKS2-enheden (LUKS1-enheder er også 
    påvirket indirekte, se nedenfor).</p>

    <p>Angrebet kræver gentagen fysisk adgang til LUKS2-enheden, men ikke noget 
    kendskab til brugeres adgangskoder.</p>

    <p>Dekrypteringstrinnet udføres efter en gyldig bruger aktiverer enheden med 
    en korrekt adgangskode og ændrede metadata.</p>

    <p>Størren på muligvis dekrypterede data pr. angrebstrin er afhængig af den 
    opsatte LUKS2-headerstørrelse (metadatastørrelsen er konfigurérbar med 
    LUKS2).  Med LUKS2-standardparametre (16 MiB-header) og kun ét allokeret 
    keyslot (512 bit-nøgle til AES-XTS), simuleret dekryptering med 
    kontrolsumsrobusthed SHA1 (20 bytes-kontrolsum til 4096 byte-blokke), kan 
    den maksimale dekrypteringsstørrelse være over 3 GiB.</p>

    <p>Angrebet er ikke brugbart mod LUKS1-formatet, men angriberen kan opdatere 
    metadata på stedet til LUKS2-format, som et tyderligere trin.  Til en sådan 
    konverteret LUKS2-header, er keyslotområdet begrænset til en dekrypteret 
    størrelse (med SHA1-kontrolsummer) på over 300 MiB.</p>

    <p>LUKS-enheder, som blev formateret ved hjælp af en binær cryptsetup-fil 
    fra Debian Stretch eller tidligere, anvender LUKS1.  Men siden Debian Buster 
    er LUKS' standard på disk-formatversion LUKS2.  I særdeleshed anvender 
    krypterede enheder, som er formateret af Debian Busters og Bullseyes 
    installeringsprogrammer, LUKS2 som standard.</p></li>

<li>Nøgletrunkering i dm-integrity

    <p>Denne opdatering løser også et problem med nøgletrunkering på 
    selvstændige dm-integrity-enheder, som anvender HMAC-integritetsbeskyttelse. 
    For sådanne eksisterende enheder med ekstra lange HMAC-nøgler (typisk med en 
    længde på mere end 106 bytes), kan man være nødt til manuelt at trunkere 
    nøglen ved hjælp af integritysetup(8)'s valgmulighed 
    <q>--integrity-key-size</q>, for på korrekt vis at mappe enheden under
    2:2.3.7-1+deb11u1 og senere.</p>

    <p>Kun selvstændige dm-integrity-enheder er påvirkede.  dm-crypt-enheder,
    herunder dem der anvender autentificeret diskkryptering, er ikke 
    påvirkede.</p></li>

</ul>

<p>I den gamle stabile distribution (buster), findes dette problem ikke.</p>

<p>I den stabile distribution (bullseye), er dette problem rettet i version 
2:2.3.7-1+deb11u1.</p>

<p>Vi anbefaler at du opgraderer dine cryptsetup-pakker.</p>

<p>For detaljeret sikkerhedsstatus vedrørende cryptsetup, se
dens sikkerhedssporingsside på:
<a href="https://security-tracker.debian.org/tracker/cryptsetup">\
https://security-tracker.debian.org/tracker/cryptsetup</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5070.data"
