#use wml::debian::template title="Debian 9 -- Fejl" BARETITLE=true
#use wml::debian::toc
#use wml::debian::translation-check translation="ba4da87abca9b144e1cc1ff151bf45311e5da2b1"

#include "$(ENGLISHDIR)/releases/info"

<toc-display/>

# <toc-add-entry name="known_probs">Kendte problemer</toc-add-entry>
<toc-add-entry name="security">Sikkerhedsproblemer</toc-add-entry>

<p>Debian Security-holdet udsender opdateringer til pakker i den stabile udgave, 
hvor der er registreret sikkerhedsrelaterede problemer.  Besøg 
<a href="$(HOME)/security/">sikkerhedssiderne</a> for oplysninger om alle 
sikkerhedsproblemer registreret i <q>Stretch</q>.</p>

<p>Hvis du anvender APT, kan følende linje føjes til 
<tt>/etc/apt/sources.list</tt>, for at kunne tilgå de seneste 
sikkerhedsopdateringer:</p>

<pre>
  deb http://security.debian.org/ stretch/updates main contrib non-free
</pre>

<p>Derefter køres <kbd>apt-get update</kbd> efterfulgt af 
<kbd>apt-get upgrade</kbd>.</p>


<toc-add-entry name="pointrelease">Punktudgivelser</toc-add-entry>

<p>Nogle gange, i tilfælde af kritiske problemer eller sikkerhedsopdateringer, 
opdateres den udgivne distribution.  Generelt kaldes disse for punktudgivelser)
(eller <q>point releases</q> på engelsk).</p>

<ul>
    <li>Den første punktudgivelse, 9.1, blev udgivet den
	<a href="$(HOME)/News/2017/20170722">22. juli 2017</a>.</li>
    <li>Den anden punktopdatering, 9.2, blev udgivet den 
	<a href="$(HOME)/News/2017/20171007">7. oktober 2017</a>.</li>
    <li>Den tredje punktopdatering, 9.3, blev udgivet den 
	<a href="$(HOME)/News/2017/2017120902">9. december 2017</a>.</li>
    <li>Den fjerde punktopdatering, 9.4, blev udgivet den
	<a href="$(HOME)/News/2018/20180310">10. marts 2018</a>.</li>
    <li>Den femte punktopdatering, 9.5, blev udgivet den 
	<a href="$(HOME)/News/2018/20180714">14. juli 2018</a>.</li>
    <li>Den sjette punktopdatering, 9.6, blev udgivet den
	<a href="$(HOME)/News/2018/20181110">10. november 2018</a>.</li>
    <li>Den syvende punktopdatering, 9.7, blev udgivet den
	<a href="$(HOME)/News/2019/20190123">23. january 2019</a>.</li>
    <li>Den ottende punktopdatering, 9.8, blev udgivet den
	<a href="$(HOME)/News/2019/20190216">16. february 2019</a>.</li>
    <li>Den niende punktopdatering, 9.9, blev udgivet den
	<a href="$(HOME)/News/2019/20190427">27. april 2019</a>.</li>
    <li>Den tiende punktopdatering, 9.10, blev udgivet den
	<a href="$(HOME)/News/2019/2019090702">7. september 2019</a>.</li>
    <li>Den ellevte punktopdatering, 9.11, blev udgivet den
	<a href="$(HOME)/News/2019/20190908">8. september 2019</a>.</li>
    <li>Den tolvte punktopdatering, 9.12, blev udgivet den
        <a href="$(HOME)/News/2020/2020020802">8. februar 2020</a>.</li>
    <li>Den trettende punktopdtering, 9.13, blev udgivet den
	<a href="$(HOME)/News/2020/20200718">18. juli 2020</a>.</li>
    </ul>
</ul>

<ifeq <current_release_stretch> 9.0 "

<p>Der er endnu ingen punktudgivelser til Debian 9.</p>" "

<p>Se 
<a href="http://http.us.debian.org/debian/dists/stretch/ChangeLog">\
ChangeLog</a> for detaljerede ændringer mellem 9.0 og 
<current_release_stretch/>.</p>"/>

<p>Rettelser til den udgivne stabile distribution gennemgår ofte en udvidet 
testperiode, før de accepteret i arkivet.  Dog er disse ændringer tilgængelige 
i mappen 
<a href="http://ftp.debian.org/debian/dists/stretch-proposed-updates/">\
dists/stretch-proposed-updates</a> på alle Debians arkivfilspejle.</p>

<p>Hvis du anvender APT til at opdatere dine pakker, kan du installere de 
foreslåede opdateringer, ved at føje følgende linje til 
<tt>/etc/apt/sources.list</tt>:</p>

<pre>
  \# proposed additions for a 9 point release
  deb http://ftp.us.debian.org/debian stretch-proposed-updates main contrib non-free
</pre>

<p>Derefter køres <kbd>apt-get update</kbd> efterfulgt af 
<kbd>apt-get upgrade</kbd>.</p>


<toc-add-entry name="installer">Installeringssystem</toc-add-entry>

<p>For oplysninger om fejl og opdateringer til installeringssystemet, se 
siden med <a href="debian-installer/">installeringsoplysninger</a>.</p>
