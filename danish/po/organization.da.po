msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"PO-Revision-Date: 2006-08-12 17:53+0200\n"
"Last-Translator: unknown\n"
"Language-Team: unknown\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/intro/organization.data:15
msgid "delegation mail"
msgstr "delegeringsmail"

#: ../../english/intro/organization.data:16
msgid "appointment mail"
msgstr "udnævnelsesmail"

#. One male delegate
#. Pronoun tags with delegate combinations
#: ../../english/intro/organization.data:18
#: ../../english/intro/organization.data:22
msgid "<void id=\"male\"/>delegate"
msgstr "<void id=\"male\"/>delegat"

#. One female delegate
#: ../../english/intro/organization.data:20
#: ../../english/intro/organization.data:23
msgid "<void id=\"female\"/>delegate"
msgstr "<void id=\"female\"/>delegat"

#: ../../english/intro/organization.data:22
#: ../../english/intro/organization.data:25
msgid "<void id=\"he_him\"/>he/him"
msgstr "<void id=\"he_him\"/>han/ham"

#: ../../english/intro/organization.data:23
#: ../../english/intro/organization.data:26
msgid "<void id=\"she_her\"/>she/her"
msgstr "<void id=\"she_her\"/>hun/hende"

#: ../../english/intro/organization.data:24
msgid "<void id=\"gender_neutral\"/>delegate"
msgstr "<void id=\"gender_neutral\"/>delegat"

#: ../../english/intro/organization.data:24
#: ../../english/intro/organization.data:27
msgid "<void id=\"they_them\"/>they/them"
msgstr "<void id=\"they_them\"/>vedkommende"

#: ../../english/intro/organization.data:30
#: ../../english/intro/organization.data:32
msgid "current"
msgstr "nuværende"

#: ../../english/intro/organization.data:34
#: ../../english/intro/organization.data:36
msgid "member"
msgstr "medlem"

#: ../../english/intro/organization.data:39
msgid "manager"
msgstr "leder"

#: ../../english/intro/organization.data:41
msgid "SRM"
msgstr "SRM"

#: ../../english/intro/organization.data:41
msgid "Stable Release Manager"
msgstr "Udgivelsesansvarlig for stable"

#: ../../english/intro/organization.data:43
msgid "wizard"
msgstr "troldmand"

#. we only use the chair tag once, for techctte, I wonder why it's here.
#: ../../english/intro/organization.data:45
msgid "chair"
msgstr "formand"

#: ../../english/intro/organization.data:48
msgid "assistant"
msgstr "assistent"

#: ../../english/intro/organization.data:50
msgid "secretary"
msgstr "sekretær"

#: ../../english/intro/organization.data:52
msgid "representative"
msgstr "repræsentant"

#: ../../english/intro/organization.data:54
msgid "role"
msgstr "rolle"

#: ../../english/intro/organization.data:62
msgid ""
"In the following list, <q>current</q> is used for positions that are\n"
"transitional (elected or appointed with a certain expiration date)."
msgstr ""
"I den følgende liste anvendes <q>nuværende</q> ved positioner, som er\n"
"foranderlige (valgt eller udnævnt med en fastlagt udløbsdato)."

#: ../../english/intro/organization.data:70
#: ../../english/intro/organization.data:82
msgid "Officers"
msgstr "Bestyrelesesmedlemmer"

#: ../../english/intro/organization.data:71
#: ../../english/intro/organization.data:108
msgid "Distribution"
msgstr "Distribution"

#: ../../english/intro/organization.data:72
#: ../../english/intro/organization.data:203
msgid "Communication and Outreach"
msgstr "Kommunikation og opsøgende arbejde"

#: ../../english/intro/organization.data:74
#: ../../english/intro/organization.data:206
msgid "Data Protection team"
msgstr "Databeskyttelseshold"

#: ../../english/intro/organization.data:75
#: ../../english/intro/organization.data:211
msgid "Publicity team"
msgstr "Publicityhold"

#: ../../english/intro/organization.data:77
#: ../../english/intro/organization.data:284
msgid "Membership in other organizations"
msgstr "Medlemskaber af andre organisationer"

#: ../../english/intro/organization.data:78
#: ../../english/intro/organization.data:312
msgid "Support and Infrastructure"
msgstr "Support og infrastruktur"

#: ../../english/intro/organization.data:85
msgid "Leader"
msgstr "Leder"

#: ../../english/intro/organization.data:87
msgid "Technical Committee"
msgstr "Teknisk komité"

#: ../../english/intro/organization.data:103
msgid "Secretary"
msgstr "Sekretær"

#: ../../english/intro/organization.data:111
msgid "Development Projects"
msgstr "Udviklingsprojekter"

#: ../../english/intro/organization.data:112
msgid "FTP Archives"
msgstr "Ftp-arkiv"

#: ../../english/intro/organization.data:114
msgid "FTP Masters"
msgstr "Ftp-mastere"

#: ../../english/intro/organization.data:120
msgid "FTP Assistants"
msgstr "Ftp-assistenter"

#: ../../english/intro/organization.data:126
msgid "FTP Wizards"
msgstr "Ftp-troldmænd"

#: ../../english/intro/organization.data:130
msgid "Backports"
msgstr "Backports"

#: ../../english/intro/organization.data:132
msgid "Backports Team"
msgstr "Backports-holdet"

#: ../../english/intro/organization.data:136
msgid "Release Management"
msgstr "Udgivelsesstyring"

#: ../../english/intro/organization.data:138
msgid "Release Team"
msgstr "Udgivelseshold"

#: ../../english/intro/organization.data:147
msgid "Quality Assurance"
msgstr "Kvalitetssikring"

#: ../../english/intro/organization.data:148
msgid "Installation System Team"
msgstr "Startdisketter"

#: ../../english/intro/organization.data:149
msgid "Debian Live Team"
msgstr "Debian Live Team"

#: ../../english/intro/organization.data:150
msgid "Release Notes"
msgstr "Udgivelsesbemærkninger"

#: ../../english/intro/organization.data:152
msgid "CD/DVD/USB Images"
msgstr "CD/DVD/USB-aftryk"

#: ../../english/intro/organization.data:154
msgid "Production"
msgstr "Produktion"

#: ../../english/intro/organization.data:161
msgid "Testing"
msgstr "Tester"

#: ../../english/intro/organization.data:163
msgid "Cloud Team"
msgstr "Cloud-holdet"

#: ../../english/intro/organization.data:167
msgid "Autobuilding infrastructure"
msgstr "Autoopbygning og infrastruktur"

#: ../../english/intro/organization.data:169
msgid "Wanna-build team"
msgstr "Wanna-build-hold"

#: ../../english/intro/organization.data:176
msgid "Buildd administration"
msgstr "Buildd-administration"

#: ../../english/intro/organization.data:193
msgid "Documentation"
msgstr "Dokumentation"

#: ../../english/intro/organization.data:198
msgid "Work-Needing and Prospective Packages list"
msgstr "Pakker der skal arbejdes på og fremtidige pakker"

#: ../../english/intro/organization.data:214
msgid "Press Contact"
msgstr "Pressekontakt"

#: ../../english/intro/organization.data:216
msgid "Web Pages"
msgstr "Websider"

#: ../../english/intro/organization.data:228
msgid "Planet Debian"
msgstr "Planet Debian"

#: ../../english/intro/organization.data:233
msgid "Outreach"
msgstr "Opsøgning"

#: ../../english/intro/organization.data:238
msgid "Debian Women Project"
msgstr "Debian Women-projektet"

#: ../../english/intro/organization.data:246
msgid "Community"
msgstr "Fællesskab"

#: ../../english/intro/organization.data:255
msgid ""
"To send a private message to all the members of the Community Team, use the "
"GPG key <a href=\"community-team-pubkey.txt"
"\">817DAE61E2FE4CA28E1B7762A89C4D0527C4C869</a>."
msgstr ""
"For at sende en privat meddelelse til alle Community Teams medlemmer, bruges "
"GPG-nøglen <a href=\"community-team-pubkey.txt"
"\">817DAE61E2FE4CA28E1B7762A89C4D0527C4C869</a>."

#: ../../english/intro/organization.data:257
msgid "Events"
msgstr "Begivenheder"

#: ../../english/intro/organization.data:264
msgid "DebConf Committee"
msgstr "DebConf-komité"

#: ../../english/intro/organization.data:271
msgid "Partner Program"
msgstr "Partnerskabsprogram"

#: ../../english/intro/organization.data:275
msgid "Hardware Donations Coordination"
msgstr "Koordinering af hardwaredonationer"

#: ../../english/intro/organization.data:290
msgid "GNOME Foundation"
msgstr "GNOME Foundation"

#: ../../english/intro/organization.data:292
msgid "Linux Professional Institute"
msgstr "Linux Professional Institute"

#: ../../english/intro/organization.data:294
msgid "Linux Magazine"
msgstr "Linux Magazine"

#: ../../english/intro/organization.data:296
msgid "Linux Standards Base"
msgstr "Linux Standards Base"

#: ../../english/intro/organization.data:298
msgid "Free Standards Group"
msgstr "Free Standards Group"

#: ../../english/intro/organization.data:299
msgid "SchoolForge"
msgstr "SchoolForge"

#: ../../english/intro/organization.data:302
msgid ""
"OASIS: Organization\n"
"      for the Advancement of Structured Information Standards"
msgstr ""
"OASIS: Organization\n"
"      for the Advancement of Structured Information Standards"

#: ../../english/intro/organization.data:305
msgid ""
"OVAL: Open Vulnerability\n"
"      Assessment Language"
msgstr ""
"OVAL: Open Vulnerability\n"
"      Assessment Language"

#: ../../english/intro/organization.data:308
msgid "Open Source Initiative"
msgstr "Open Source Initiative"

#: ../../english/intro/organization.data:315
msgid "Bug Tracking System"
msgstr "Fejlrapporteringssystem"

#: ../../english/intro/organization.data:320
msgid "Mailing Lists Administration and Mailing List Archives"
msgstr "Postlisteadministration og postlistearkiver"

#: ../../english/intro/organization.data:329
msgid "New Members Front Desk"
msgstr "Reception for nye medlemmer"

#: ../../english/intro/organization.data:335
msgid "Debian Account Managers"
msgstr "Debians kontoadministratorer"

#: ../../english/intro/organization.data:339
msgid ""
"To send a private message to all DAMs, use the GPG key "
"57731224A9762EA155AB2A530CA8D15BB24D96F2."
msgstr ""
"For at sende en privat besked til alle DAM'er, benyttes denne GPG-nøgle "
"57731224A9762EA155AB2A530CA8D15BB24D96F2."

#: ../../english/intro/organization.data:340
msgid "Keyring Maintainers (PGP and GPG)"
msgstr "Vedligeholdere af nøglering (PGP og GPG)"

#: ../../english/intro/organization.data:344
msgid "Security Team"
msgstr "Sikkerhedsteam"

#: ../../english/intro/organization.data:355
msgid "Policy"
msgstr "Retningslinjer"

#: ../../english/intro/organization.data:358
msgid "System Administration"
msgstr "Systemadministration"

#: ../../english/intro/organization.data:359
msgid ""
"This is the address to use when encountering problems on one of Debian's "
"machines, including password problems or you need a package installed."
msgstr ""
"Benyt denne adresse hvis der opstår problemer med en af Debians maskiner, "
"inklusive problemer med adgangskoder eller hvis du har brug for at en pakke "
"bliver installeret."

#: ../../english/intro/organization.data:369
msgid ""
"If you have hardware problems with Debian machines, please see <a href="
"\"https://db.debian.org/machines.cgi\">Debian Machines</a> page, it should "
"contain per-machine administrator information."
msgstr ""
"Hvis du har hardware-problemer med Debians maskiner, se <a href=\"https://db."
"debian.org/machines.cgi\">Debians maskineside</a>, den skulle indeholde "
"administratoroplysninger for hver enkelt maskine."

#: ../../english/intro/organization.data:370
msgid "LDAP Developer Directory Administrator"
msgstr "Administrator af LDAP-udvikleroversigten"

#: ../../english/intro/organization.data:371
msgid "Mirrors"
msgstr "Filspejle"

#: ../../english/intro/organization.data:378
msgid "DNS Maintainer"
msgstr "DNS-ansvarlig"

#: ../../english/intro/organization.data:379
msgid "Package Tracking System"
msgstr "Pakkesporingssystem"

#: ../../english/intro/organization.data:381
msgid "Treasurer"
msgstr "Kasserer"

#: ../../english/intro/organization.data:388
msgid ""
"<a name=\"trademark\" href=\"m4_HOME/trademark\">Trademark</a> use requests"
msgstr ""
"Ønsker om at anvende <a name=\"trademark\" href=\"m4_HOME/trademark"
"\">varemærke</a>"

#: ../../english/intro/organization.data:392
msgid "Salsa administrators"
msgstr "Salsa-administratorer"

#~ msgid "Ports"
#~ msgstr "Tilpasninger"

#~ msgid "Special Configurations"
#~ msgstr "Specielle systemer"

#~ msgid "Laptops"
#~ msgstr "Bærbare"

#~ msgid "Firewalls"
#~ msgstr "Firewalls"

#~ msgid "Embedded systems"
#~ msgstr "Embedded systemer"

#~ msgid "User support"
#~ msgstr "Hjælp til brugere"

#~ msgid "Anti-harassment"
#~ msgstr "Bekæmpelse af chikane"

#~ msgid "Debian Pure Blends"
#~ msgstr "Debian Pure Blends"

#~ msgid "Debian for children from 1 to 99"
#~ msgstr "Debian for børn mellem 1 og 99"

#~ msgid "Debian for medical practice and research"
#~ msgstr "Debian til lægepraksiser og forskning"

#~ msgid "Debian for education"
#~ msgstr "Debian til uddannelse"

#~ msgid "Debian in legal offices"
#~ msgstr "Debian på juridiske kontorer"

#~ msgid "Debian for people with disabilities"
#~ msgstr "Debian til handicappede"

#~ msgid "Debian for science and related research"
#~ msgstr "Debian til videnskabelig og relateret forskning"

#~ msgid "Debian for astronomy"
#~ msgstr "Debian til astronomi"

#~ msgid "Alioth administrators"
#~ msgstr "Alioth-administratorer"

#~ msgid "Live System Team"
#~ msgstr "Live System-holdet"

#~ msgid "DebConf chairs"
#~ msgstr "DebConf-ledere"

#~ msgid "current Debian Project Leader"
#~ msgstr "nuværende Debian-projektleder"

#~ msgid "Testing Security Team"
#~ msgstr "Testings Sikkerhedsteam"

#~ msgid "Security Audit Project"
#~ msgstr "Sikkerhedsauditprojektet"

#~ msgid "Alpha (Not active: was not released with squeeze)"
#~ msgstr "Alpha (ikke aktiv: blev ikke udgivet med squeeze)"

#~ msgid "Volatile Team"
#~ msgstr "Volatile-hold"

#~ msgid ""
#~ "The admins responsible for buildd's for a particular arch can be reached "
#~ "at <genericemail arch@buildd.debian.org>, for example <genericemail "
#~ "i386@buildd.debian.org>."
#~ msgstr ""
#~ "De ansvarlige administratorer for buildd'erne til en bestemt arkitektur, "
#~ "kan kontaktes på <genericemail arch@buildd.debian.org>, eksempelvis "
#~ "<genericemail i386@buildd.debian.org>."

#~ msgid ""
#~ "Names of individual buildd's admins can also be found on <a href=\"http://"
#~ "www.buildd.net\">http://www.buildd.net</a>.  Choose an architecture and a "
#~ "distribution to see the available buildd's and their admins."
#~ msgstr ""
#~ "Navne på individuelle buildd'ers administratorer kan også findes på <a "
#~ "href=\"http://www.buildd.net\">http://www.buildd.net</a>.  Vælg en "
#~ "arkitektur og en distribution, for at se de tilgængelige buildd'er og "
#~ "deres administratorer."

#~ msgid "Mailing List Archives"
#~ msgstr "Postlistearkiv"

#~ msgid "Debian GNU/Linux for Enterprise Computing"
#~ msgstr "Debian GNU/Linux til 'enterprise computing'"

#~ msgid "Installation System for ``stable''"
#~ msgstr "Installationssystem til 'stable'"

#~ msgid "Internal Projects"
#~ msgstr "Interne projekter"

#~ msgid "Mailing list"
#~ msgstr "Postliste"

#~ msgid "Installation"
#~ msgstr "Installation"

#~ msgid "Delegates"
#~ msgstr "Delegerede"

#~ msgid "Debian-Med"
#~ msgstr "Debian-Med"

#~ msgid "Debian-Edu"
#~ msgstr "Debian-Edu"

#~ msgid "Debian-Desktop"
#~ msgstr "Debian-Desktop"

#~ msgid "Proposed Debian internal project."
#~ msgstr "Foreslået internt Debian-projekt."

#~ msgid ""
#~ "This is not yet an official Debian internal project but it has announced "
#~ "the intention to be integrated."
#~ msgstr ""
#~ "Dette er endnu ikke et officelt internt Debian-projekt, men ønsket om at "
#~ "blive integreret er blevet annonceret."

#~ msgid "Debian Multimedia Distribution"
#~ msgstr "Debians multimedie-distribution"

#~ msgid "Release Wizard"
#~ msgstr "Udgivelsestroldmand"

#~ msgid "Release Assistants for ``stable''"
#~ msgstr "Udgivelsesassistenter for den \"stabile\" udgave"

#~ msgid "Release Assistants"
#~ msgstr "Udgivelsesassistenter"

#~ msgid "APT Team"
#~ msgstr "APT-team"

#~ msgid "Debian for non-profit organisations"
#~ msgstr "Debian for almennyttige organisationer"

#~ msgid "The Universal Operating System as your Desktop"
#~ msgstr "Det universelle styresystem som din desktop"

#~ msgid "Accountant"
#~ msgstr "Bogholder"

#~ msgid "Release Team for ``stable''"
#~ msgstr "Udgivelseshold vedr. den \"stabile\" udgave"

#~ msgid "Custom Debian Distributions"
#~ msgstr "Debians skræddersyede distribution"

#~ msgid "Key Signing Coordination"
#~ msgstr "Koordinering af nøglesignering"

#~ msgid "Marketing Team"
#~ msgstr "Marketingsholdet"

#~ msgid "Handhelds"
#~ msgstr "Håndholdte"

#~ msgid "Vendors"
#~ msgstr "Forhandlere"

#~ msgid "Summer of Code 2013 Administrators"
#~ msgstr "Summer of Code 2013-administratorer"

#~ msgid "Bits from Debian"
#~ msgstr "Bits fra Debian"

#~ msgid "Debian Maintainer (DM) Keyring Maintainers"
#~ msgstr "Vedligeholdere af Debian Maintainer-nøglering (DM)"

#~ msgid "Publicity"
#~ msgstr "PR"

#~ msgid "Auditor"
#~ msgstr "Revisor"

#~ msgid "Individual Packages"
#~ msgstr "Individuelle pakker"

#~ msgid "CD Vendors Page"
#~ msgstr "Cd-forhandlerside"

#~ msgid "Consultants Page"
#~ msgstr "Konsulentside"
