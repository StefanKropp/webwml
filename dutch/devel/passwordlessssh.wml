#use wml::debian::template title="Hoe u ssh kunt instellen zodat er niet naar een wachtwoord gevraagd wordt" BARETITLE=true
#use wml::debian::translation-check translation="87a9a20b860797210702af5b17db09a362a220e7"

<p>U kunt een RSA-authenticatiesleutel aanmaken om vanuit uw account te kunnen inloggen op een externe site zonder dat u uw wachtwoord dient te typen.</p>

<p>Merk op: eens u dit ingesteld heeft en er een indringer in uw account/site
binnendringt, krijgt deze ook toegang tot de site waartoe u zonder wachtwoord
toegang heeft! Daarom moet u dit <strong>nooit</strong> doen vanuit het root-account.</p>

<ul>
      <li>Voer het commando <code>ssh-keygen(1)</code> uit op uw machine
    en druk gewoon op de enter-toets wanneer u om een wachtwoord gevraagd
    wordt.
	<br>
	Hiermee wordt een private en publieke sleutel aangemaakt. Bij oudere
	SSH-versies, worden deze opgeslagen in
	<code>~/.ssh/identity</code> en in
	<code>~/.ssh/identity.pub</code>; bij recentere versies worden deze
    opgeslagen in <code>~/.ssh/id_rsa</code> en in
	<code>~/.ssh/id_rsa.pub</code>.</li>
      <li>Nadien moet u de inhoud van de publieke sleutel toevoegen aan
	<code>~/.ssh/authorized_keys</code> op de externe site (het bestand
	moet bestandsmodus 600 hebben).
	<br>
	Indien u een ontwikkelaar bent en toegang wilt tot debian.org-systemen met
    een dergelijke sleutel, kan de ontwikkelaarsdatabank uw sleutel
    verspreiden naar alle debian.org-machines. Raadpleeg de
	<a href="https://db.debian.org/doc-mail.html">LDAP gateway
	documentatie</a>.</li>
</ul>

<p>Daarna zou u in staat moeten zijn om met ssh in te loggen op de externe
server zonder dat u om een wachtwoord gevraagd wordt.</p>

<p><strong>Belangrijk:</strong> Merk op dat iedereen die leestoegang heeft tot
de private sleutel, deze ook kan gebruiken om dezelfde wachtwoordvrije toegang
te krijgen tot de externe site. Dit betreft iedere persoon die
beheerderstoegang heeft tot uw lokale machine. Om die reden wordt het stellig
aanbevolen dat u een wachtwoord gebruikt voor uw private sleutel indien u niet
de enige systeembeheerder bent op uw machine. U kunt gebruik maken van
<code>ssh-agent(1)</code> en <code>ssh-add(1)</code> om uw wachtwoord slechts
eenmaal te typen voor alle gebruik van een specifieke sleutel tijdens een
sessie. U kunt al uw sleutels automatisch laden in de agent door de
volgende regels toe te voegen aan uw <code>~/.xsession</code>-bestand:</p>
<pre>
      \# indien use-ssh-agent opgegeven werd in /etc/X11/Xsession.options
      \# (dit is standaard) dan heeft u enkel de tweede regel nodig
      \# eval ssh-agent
      ssh-add &lt;bestandsnaam-van-de-ssh-sleutel&gt;
</pre>
<p>Het pakket <code>ssh-askpass</code> moet geïnstalleerd zijn om <code>ssh-add</code> uit te voeren zonder terminal.</p>
