#
# Damyan Ivanov <dmn@debian.org>, 2010, 2012.
#
msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"PO-Revision-Date: 2012-12-07 15:56+0200\n"
"Last-Translator: Damyan Ivanov <dmn@debian.org>\n"
"Language-Team: Bulgarian <dict@fsa-bg.org>\n"
"Language: bg\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1)\n"
"X-Generator: Gtranslator 2.91.5\n"

#: ../../english/Bugs/pkgreport-opts.inc:17
msgid "in package"
msgstr "в пакет"

#: ../../english/Bugs/pkgreport-opts.inc:20
#: ../../english/Bugs/pkgreport-opts.inc:60
#: ../../english/Bugs/pkgreport-opts.inc:94
msgid "tagged"
msgstr "с етикет"

#: ../../english/Bugs/pkgreport-opts.inc:23
#: ../../english/Bugs/pkgreport-opts.inc:63
#: ../../english/Bugs/pkgreport-opts.inc:97
msgid "with severity"
msgstr "с важност"

#: ../../english/Bugs/pkgreport-opts.inc:26
msgid "in source package"
msgstr "в пакет-източник"

#: ../../english/Bugs/pkgreport-opts.inc:29
msgid "in packages maintained by"
msgstr "в пакети с отговорник"

#: ../../english/Bugs/pkgreport-opts.inc:32
msgid "submitted by"
msgstr "изпратени от"

#: ../../english/Bugs/pkgreport-opts.inc:35
msgid "owned by"
msgstr "собственост на"

#: ../../english/Bugs/pkgreport-opts.inc:38
msgid "with status"
msgstr "със статус"

#: ../../english/Bugs/pkgreport-opts.inc:41
msgid "with mail from"
msgstr "със съобщение от"

#: ../../english/Bugs/pkgreport-opts.inc:44
msgid "newest bugs"
msgstr "най-новите доклади"

#: ../../english/Bugs/pkgreport-opts.inc:57
#: ../../english/Bugs/pkgreport-opts.inc:91
msgid "with subject containing"
msgstr "със заглавие, съдържащо"

#: ../../english/Bugs/pkgreport-opts.inc:66
#: ../../english/Bugs/pkgreport-opts.inc:100
msgid "with pending state"
msgstr "чакащи"

#: ../../english/Bugs/pkgreport-opts.inc:69
#: ../../english/Bugs/pkgreport-opts.inc:103
msgid "with submitter containing"
msgstr "с подател, съдържащ"

#: ../../english/Bugs/pkgreport-opts.inc:72
#: ../../english/Bugs/pkgreport-opts.inc:106
msgid "with forwarded containing"
msgstr "с текст в адреса на препращане"

#: ../../english/Bugs/pkgreport-opts.inc:75
#: ../../english/Bugs/pkgreport-opts.inc:109
msgid "with owner containing"
msgstr "със собственик, съдържащ"

#: ../../english/Bugs/pkgreport-opts.inc:78
#: ../../english/Bugs/pkgreport-opts.inc:112
msgid "with package"
msgstr "в пакет"

#: ../../english/Bugs/pkgreport-opts.inc:122
msgid "normal"
msgstr "нормален"

#: ../../english/Bugs/pkgreport-opts.inc:125
msgid "oldview"
msgstr "стар изглед"

#: ../../english/Bugs/pkgreport-opts.inc:128
msgid "raw"
msgstr "сурови данни"

#: ../../english/Bugs/pkgreport-opts.inc:131
msgid "age"
msgstr "възраст"

#: ../../english/Bugs/pkgreport-opts.inc:137
msgid "Repeat Merged"
msgstr "Повтаряне на обединените"

#: ../../english/Bugs/pkgreport-opts.inc:138
msgid "Reverse Bugs"
msgstr "Обратно подреждане"

#: ../../english/Bugs/pkgreport-opts.inc:139
msgid "Reverse Pending"
msgstr "Първо чакащите"

#: ../../english/Bugs/pkgreport-opts.inc:140
msgid "Reverse Severity"
msgstr "Обратно подреждане по важност"

#: ../../english/Bugs/pkgreport-opts.inc:141
msgid "No Bugs which affect packages"
msgstr "Без доклади, засягащи пакета"

#: ../../english/Bugs/pkgreport-opts.inc:143
msgid "None"
msgstr "---"

#: ../../english/Bugs/pkgreport-opts.inc:144
msgid "testing"
msgstr "тестова"

#: ../../english/Bugs/pkgreport-opts.inc:145
msgid "oldstable"
msgstr "стара стабилна"

#: ../../english/Bugs/pkgreport-opts.inc:146
msgid "stable"
msgstr "стабилна"

#: ../../english/Bugs/pkgreport-opts.inc:147
msgid "experimental"
msgstr "експериментална"

#: ../../english/Bugs/pkgreport-opts.inc:148
msgid "unstable"
msgstr "нестабилна"

#: ../../english/Bugs/pkgreport-opts.inc:152
msgid "Unarchived"
msgstr "Активни"

#: ../../english/Bugs/pkgreport-opts.inc:155
msgid "Archived"
msgstr "Архивирани"

#: ../../english/Bugs/pkgreport-opts.inc:158
msgid "Archived and Unarchived"
msgstr "Активни и архивирани"

#~ msgid "Distribution:"
#~ msgstr "Дистрибуция:"

#~ msgid "Exclude severity:"
#~ msgstr "Изключване на важност:"

#~ msgid "Exclude status:"
#~ msgstr "Изключване на статус:"

#~ msgid "Exclude tag:"
#~ msgstr "Изключване на етикет:"

#~ msgid "Flags:"
#~ msgstr "Флагове"

#~ msgid "Include severity:"
#~ msgstr "Включване на важност:"

#~ msgid "Include status:"
#~ msgstr "Включване на статус:"

#~ msgid "Include tag:"
#~ msgstr "Включване на етикет:"

#~ msgid "Package version:"
#~ msgstr "Версия на пакета:"

#~ msgid "active bugs"
#~ msgstr "активни доклади"

#~ msgid "bugs"
#~ msgstr "доклади"

#~ msgid "confirmed"
#~ msgstr "потвърден"

#~ msgid "critical"
#~ msgstr "критичен"

#~ msgid "d-i"
#~ msgstr "d-i"

#~ msgid "display merged bugs only once"
#~ msgstr "показване на слетите доклади само веднъж"

#~ msgid "don't show statistics in the footer"
#~ msgstr "скриване на статистиките в края на страницата"

#~ msgid "don't show table of contents in the header"
#~ msgstr "скриване на съдържанието в антетката"

#~ msgid "done"
#~ msgstr "приключен"

#~ msgid "etch"
#~ msgstr "etch"

#~ msgid "etch-ignore"
#~ msgstr "etch-ignore"

#~ msgid "fixed"
#~ msgstr "поправен"

#~ msgid "fixed-in-experimental"
#~ msgstr "поправен в експерименталната дистрибуция"

#~ msgid "fixed-upstream"
#~ msgstr "поправен от автора"

#~ msgid "forwarded"
#~ msgstr "препратен"

#~ msgid "grave"
#~ msgstr "много сериозен"

#~ msgid "help"
#~ msgstr "помощ"

#~ msgid "important"
#~ msgstr "важен"

#~ msgid "ipv6"
#~ msgstr "ipv6"

#~ msgid "l10n"
#~ msgstr "локализация"

#~ msgid "lenny"
#~ msgstr "lenny"

#~ msgid "lenny-ignore"
#~ msgstr "lenny-ignore"

#~ msgid "lfs"
#~ msgstr "lfs"

#~ msgid "minor"
#~ msgstr "незначителен"

#~ msgid "moreinfo"
#~ msgstr "повече информация"

#~ msgid "no ordering by status or severity"
#~ msgstr "без подреждане по на статус или важност"

#~ msgid "open"
#~ msgstr "отворен"

#~ msgid "patch"
#~ msgstr "кръпка"

#~ msgid "pending"
#~ msgstr "чакащ"

#~ msgid "potato"
#~ msgstr "potato"

#~ msgid "proposed-updates"
#~ msgstr "предложени обновления"

#~ msgid "sarge-ignore"
#~ msgstr "sarge-ignore"

#~ msgid "security"
#~ msgstr "сигурност"

#~ msgid "serious"
#~ msgstr "сериозен"

#~ msgid "sid"
#~ msgstr "sid"

#~ msgid "testing-proposed-updates"
#~ msgstr "предлагани обновления за тестовата дистрибуция"

#~ msgid "unreproducible"
#~ msgstr "невъзпроизводим"

#~ msgid "upstream"
#~ msgstr "авторски"

#~ msgid "wishlist"
#~ msgstr "пожелание"

#~ msgid "wontfix"
#~ msgstr "отхвърлен"

#~ msgid "woody"
#~ msgstr "woody"
