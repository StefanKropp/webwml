#use wml::debian::translation-check translation="669c87408de3af72c047aaa7ef3786903984b7ba" mindelta="1" maintainer="Lev Lamberov"
<define-tag description>обновление безопасности</define-tag>
<define-tag moreinfo>
<p>В QEMU, быстром эмуляторе процессора, были обнаружены многочисленные проблемы
безопасности:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12829">CVE-2020-12829</a>

    <p>Переполнение целых чисел в коде дисплейного устройства sm501 может приводить к отказу
    в обслуживании.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-14364">CVE-2020-14364</a>

    <p>Запись за пределами выделенного буфера памяти в коде эмуляции USB может приводить
    к выполнению кода из гостевой системы в основной системе.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-15863">CVE-2020-15863</a>

    <p>Переполнение буфера в коде сетевого устройства XGMAC может приводить к отказу
    в обслуживании или выполнению произвольного кода.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-16092">CVE-2020-16092</a>

    <p>Вызываемое извне выражение assert в коде устройств e1000e и vmxnet3 может приводить
    к отказу в обслуживании.</p></li>

</ul>

<p>В стабильном выпуске (buster) эти проблемы были исправлены в
версии 1:3.1+dfsg-8+deb10u8.</p>

<p>Рекомендуется обновить пакеты qemu.</p>

<p>С подробным статусом поддержки безопасности qemu можно ознакомиться на
соответствующей странице отслеживания безопасности по адресу
<a href="https://security-tracker.debian.org/tracker/qemu">\
https://security-tracker.debian.org/tracker/qemu</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4760.data"
