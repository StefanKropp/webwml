# translation of date.ru.po to Russian
# Yuri Kozlov <yuray@komyakino.ru>, 2009, 2011.
msgid ""
msgstr ""
"Project-Id-Version: Debian webwml date\n"
"PO-Revision-Date: 2011-02-06 11:01+0300\n"
"Last-Translator: Yuri Kozlov <yuray@komyakino.ru>\n"
"Language-Team: Russian <debian-l10n-russian@lists.debian.org>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.0\n"
"Plural-Forms:  nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n"
"%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"

#. List of weekday names (used in modification dates)
#: ../../english/template/debian/ctime.wml:11
msgid "Sun"
msgstr "Вск"

#: ../../english/template/debian/ctime.wml:12
msgid "Mon"
msgstr "Пнд"

#: ../../english/template/debian/ctime.wml:13
msgid "Tue"
msgstr "Втр"

#: ../../english/template/debian/ctime.wml:14
msgid "Wed"
msgstr "Срд"

#: ../../english/template/debian/ctime.wml:15
msgid "Thu"
msgstr "Чтв"

#: ../../english/template/debian/ctime.wml:16
msgid "Fri"
msgstr "Птн"

#: ../../english/template/debian/ctime.wml:17
msgid "Sat"
msgstr "Сбт"

#. List of month names (used in modification dates, and may be used in news 
#. listings)
#: ../../english/template/debian/ctime.wml:23
msgid "Jan"
msgstr "Янв"

#: ../../english/template/debian/ctime.wml:24
msgid "Feb"
msgstr "Фев"

#: ../../english/template/debian/ctime.wml:25
msgid "Mar"
msgstr "Мар"

#: ../../english/template/debian/ctime.wml:26
msgid "Apr"
msgstr "Апр"

#: ../../english/template/debian/ctime.wml:27
msgid "May"
msgstr "Май"

#: ../../english/template/debian/ctime.wml:28
msgid "Jun"
msgstr "Июн"

#: ../../english/template/debian/ctime.wml:29
msgid "Jul"
msgstr "Июл"

#: ../../english/template/debian/ctime.wml:30
msgid "Aug"
msgstr "Авг"

#: ../../english/template/debian/ctime.wml:31
msgid "Sep"
msgstr "Сен"

#: ../../english/template/debian/ctime.wml:32
msgid "Oct"
msgstr "Окт"

#: ../../english/template/debian/ctime.wml:33
msgid "Nov"
msgstr "Ноя"

#: ../../english/template/debian/ctime.wml:34
msgid "Dec"
msgstr "Дек"

#. List of long month names (may be used in "spoken" dates and date ranges).
#: ../../english/template/debian/ctime.wml:39
msgid "January"
msgstr "Января"

#: ../../english/template/debian/ctime.wml:40
msgid "February"
msgstr "Февраля"

#: ../../english/template/debian/ctime.wml:41
msgid "March"
msgstr "Марта"

#: ../../english/template/debian/ctime.wml:42
msgid "April"
msgstr "Апреля"

#. The <void> tag is to distinguish short and long forms of May.
#. Do not put it in msgstr.
#: ../../english/template/debian/ctime.wml:45
msgid "<void id=\"fullname\" />May"
msgstr "Мая"

#: ../../english/template/debian/ctime.wml:46
msgid "June"
msgstr "Июня"

#: ../../english/template/debian/ctime.wml:47
msgid "July"
msgstr "Июля"

#: ../../english/template/debian/ctime.wml:48
msgid "August"
msgstr "Августа"

#: ../../english/template/debian/ctime.wml:49
msgid "September"
msgstr "Сентября"

#: ../../english/template/debian/ctime.wml:50
msgid "October"
msgstr "Октября"

#: ../../english/template/debian/ctime.wml:51
msgid "November"
msgstr "Ноября"

#: ../../english/template/debian/ctime.wml:52
msgid "December"
msgstr "Декабря"

#. $dateform: Date format (sprintf) for modification dates.
#. Available variables are: $mday = day-of-month, $monnr = month number,
#. $mon = month string (from @moy), $year = year number.
#. Percent signs are escaped because they are special during pass 2,
#. replace all % by [%]
#: ../../english/template/debian/ctime.wml:60
msgid ""
"q{[%]s, [%]s [%]2d [%]02d:[%]02d:[%]02d [%]s [%]04d}, $wday, $mon, $mday, "
"$hour, $min, $sec, q{UTC}, 1900+$year"
msgstr ""
"q{[%]s, [%]2d [%]s [%]s, [%]02d:[%]02d:[%]02d [%]s}, $wday, $mday, $mon, "
"1900+$year, $hour, $min, $sec, q{UTC}"

#. $newsdateform: Date format (sprintf) for news items.
#. Available variables are: $mday = day-of-month, $mon = month number,
#. $mon_str = month string (from @moy), $year = year number.
#. Percent signs are escaped because they are special during pass 2,
#. replace all % by [%]
#: ../../english/template/debian/ctime.wml:68
msgid "q{[%]02d [%]s [%]04d}, $mday, $mon_str, $year"
msgstr "q{[%]02d.[%]02d.[%]04d}, $mday, $mon, $year"

#. $spokendateform: Date format (sprintf) for "spoken" dates
#. (such as the current release date).
#. Any special cases (such as the st/nd/rd/th suffixes in English) are
#. handled in the spokendate subroutine below.
#. Available variables are: $mday = day-of-month, $mon = month number,
#. $mon_str = month string (from @longmoy), $year = year number.
#. Percent signs are escaped because they are special during pass 2,
#. replace all % by [%]
#: ../../english/template/debian/ctime.wml:79
msgid "q{[%]02d [%]s [%]d}, $mday, $mon_str, $year"
msgstr "q{[%]02d [%]s [%]d}, $mday, $mon_str, $year"

#. $spokendateform_noyear: Date format (sprintf) for "spoken" dates
#. (such as the current release date), without the year.
#. Any special cases (such as the st/nd/rd/th suffixes in English) are
#. handled in the spokendate subroutine below.
#. Available variables are: $mday = day-of-month, $mon = month number,
#. $mon_str = month string (from @longmoy).
#. Percent signs are escaped because they are special during pass 2,
#. replace all % by [%]
#: ../../english/template/debian/ctime.wml:90
msgid "q{[%]d [%]s}, $mday, $mon_str"
msgstr "q{[%]d [%]s}, $mday, $mon_str"

#. $spokendateform_noday: Date format (sprintf) for "spoken" dates
#. (such a conference event), without the day.
#. Available variables are: $mon = month number,
#. $mon_str = month string (from @longmoy), $year = year number.
#. Percent signs are escaped because they are special during pass 2,
#. replace all % by [%]
#: ../../english/template/debian/ctime.wml:99
msgid "q{[%]s [%]s}, $mon_str, $year"
msgstr "q{[%]s [%]s}, $mon_str, $year"

#. $rangeform_samemonth: Date format (sprintf) for date ranges
#. (used mainly for events pages), for ranges within the same month.
#. Any special cases (such as the st/nd/rd/th suffixes in English) are
#. handled in the spokendate subroutine below.
#. Available variables are: $sday = start day-of-month, $eday = end
#. day-of-month, $smon = month number, $smon_str = month string (from @longmoy)
#. Percent signs are escaped because they are special during pass 2,
#. replace all % by [%]
#: ../../english/template/debian/ctime.wml:110
msgid "q{[%]d-[%]d [%]s}, $sday, $eday, $smon_str"
msgstr "q{[%]d-[%]d [%]s}, $sday, $eday, $smon_str"

#. $rangeform_severalmonths: Date format (sprintf) for date ranges
#. (used mainly for events pages), for ranges spanning the end of a month.
#. Any special cases (such as the st/nd/rd/th suffixes in English) are
#. handled in the spokendate subroutine below.
#. Available variables are: $sday = start day-of-month, $eday, end
#. day-of-month, $smon = start month number, $emon = end month number,
#. $smon_str = start month string (from @longmoy), $emon_str = end month string
#. Percent signs are escaped because they are special during pass 2,
#. replace all % by [%]
#: ../../english/template/debian/ctime.wml:122
msgid "q{[%]d [%]s-[%]d [%]s}, $sday, $smon_str, $eday, $emon_str"
msgstr "q{[%]d [%]s-[%]d [%]s}, $sday, $smon_str, $eday, $emon_str"
