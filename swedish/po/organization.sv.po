# translation of organization.po to Swedish
#
# Martin Ågren <martin.agren@gmail.com>, 2010.
msgid ""
msgstr ""
"Project-Id-Version: organization\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: (null)\n"
"PO-Revision-Date: 2020-10-08 19:11+0200\n"
"Last-Translator: Andreas Rönnquist <andreas@ronnquist.net>\n"
"Language-Team: Swedish <debian-l10n-swedish@lists.debian.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.4\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: ../../english/intro/organization.data:15
msgid "delegation mail"
msgstr "delegeringsmail"

#: ../../english/intro/organization.data:16
msgid "appointment mail"
msgstr "utnämningsmeddelande"

#. One male delegate
#. Pronoun tags with delegate combinations
#: ../../english/intro/organization.data:18
#: ../../english/intro/organization.data:22
msgid "<void id=\"male\"/>delegate"
msgstr "<void id=\"male\"/>delegat"

#. One female delegate
#: ../../english/intro/organization.data:20
#: ../../english/intro/organization.data:23
msgid "<void id=\"female\"/>delegate"
msgstr "<void id=\"female\"/>delegat"

#: ../../english/intro/organization.data:22
#: ../../english/intro/organization.data:25
msgid "<void id=\"he_him\"/>he/him"
msgstr "<void id=\"he_him\"/>han/honom"

#: ../../english/intro/organization.data:23
#: ../../english/intro/organization.data:26
msgid "<void id=\"she_her\"/>she/her"
msgstr "<void id=\"she_her\"/>hon/henne"

#: ../../english/intro/organization.data:24
msgid "<void id=\"gender_neutral\"/>delegate"
msgstr "<void id=\"gender_neutral\"/>delegat"

#: ../../english/intro/organization.data:24
#: ../../english/intro/organization.data:27
msgid "<void id=\"they_them\"/>they/them"
msgstr "<void id=\"they_them\"/>de/dem"

#: ../../english/intro/organization.data:30
#: ../../english/intro/organization.data:32
msgid "current"
msgstr "nuvarande"

#: ../../english/intro/organization.data:34
#: ../../english/intro/organization.data:36
msgid "member"
msgstr "medlem"

#: ../../english/intro/organization.data:39
msgid "manager"
msgstr "chef"

#: ../../english/intro/organization.data:41
msgid "SRM"
msgstr "SRM"

#: ../../english/intro/organization.data:41
msgid "Stable Release Manager"
msgstr "Samordning av nya stabila utgåvor"

#: ../../english/intro/organization.data:43
msgid "wizard"
msgstr "mästare"

#. we only use the chair tag once, for techctte, I wonder why it's here.
#: ../../english/intro/organization.data:45
msgid "chair"
msgstr "ordförande"

#: ../../english/intro/organization.data:48
msgid "assistant"
msgstr "assistent"

#: ../../english/intro/organization.data:50
msgid "secretary"
msgstr "sekreterare"

#: ../../english/intro/organization.data:52
msgid "representative"
msgstr "representant"

#: ../../english/intro/organization.data:54
msgid "role"
msgstr "toll"

#: ../../english/intro/organization.data:62
msgid ""
"In the following list, <q>current</q> is used for positions that are\n"
"transitional (elected or appointed with a certain expiration date)."
msgstr ""
"I följande lista används <q>nuvarande</q> för positioner som är\n"
"övergångspositioner (valda eller tilldelade med ett specifikt utgångsdatum)."

#: ../../english/intro/organization.data:70
#: ../../english/intro/organization.data:82
msgid "Officers"
msgstr "Tjänstemän"

#: ../../english/intro/organization.data:71
#: ../../english/intro/organization.data:108
msgid "Distribution"
msgstr "Distributionen"

#: ../../english/intro/organization.data:72
#: ../../english/intro/organization.data:204
msgid "Communication and Outreach"
msgstr "Kommunikation och Utåtriktad verksamhet"

#: ../../english/intro/organization.data:74
#: ../../english/intro/organization.data:207
msgid "Data Protection team"
msgstr "Dataskyddsgruppen"

#: ../../english/intro/organization.data:75
#: ../../english/intro/organization.data:212
msgid "Publicity team"
msgstr "Publicitetsgruppen"

#: ../../english/intro/organization.data:77
#: ../../english/intro/organization.data:279
msgid "Membership in other organizations"
msgstr "Medlemskap i andra organisationer"

#: ../../english/intro/organization.data:78
#: ../../english/intro/organization.data:302
msgid "Support and Infrastructure"
msgstr "Support och infrastruktur"

#: ../../english/intro/organization.data:85
msgid "Leader"
msgstr "Arbetsledare"

#: ../../english/intro/organization.data:87
msgid "Technical Committee"
msgstr "Teknisk kommitté"

#: ../../english/intro/organization.data:103
msgid "Secretary"
msgstr "Sekreterare"

#: ../../english/intro/organization.data:111
msgid "Development Projects"
msgstr "Utvecklingsprojekt"

#: ../../english/intro/organization.data:112
msgid "FTP Archives"
msgstr "Ftp-arkiv"

#: ../../english/intro/organization.data:114
msgid "FTP Masters"
msgstr "FTP Masters"

#: ../../english/intro/organization.data:120
msgid "FTP Assistants"
msgstr "Ftp-assistenter"

#: ../../english/intro/organization.data:126
msgid "FTP Wizards"
msgstr "FTP Wizards"

#: ../../english/intro/organization.data:130
msgid "Backports"
msgstr "Backports"

#: ../../english/intro/organization.data:132
msgid "Backports Team"
msgstr "Backports-teamet"

#: ../../english/intro/organization.data:136
msgid "Release Management"
msgstr "Samordning av nya utgåvor"

#: ../../english/intro/organization.data:138
msgid "Release Team"
msgstr "Utgivningsgruppen"

#: ../../english/intro/organization.data:148
msgid "Quality Assurance"
msgstr "Kvalitetsstyrning"

#: ../../english/intro/organization.data:149
msgid "Installation System Team"
msgstr "Installationssystemgruppen"

#: ../../english/intro/organization.data:150
msgid "Debian Live Team"
msgstr "Debian Live-gruppen"

#: ../../english/intro/organization.data:151
msgid "Release Notes"
msgstr "Versionsfakta"

#: ../../english/intro/organization.data:153
msgid "CD/DVD/USB Images"
msgstr "CD/DVD/USB-avbildnignar"

#: ../../english/intro/organization.data:155
msgid "Production"
msgstr "Produktion"

#: ../../english/intro/organization.data:162
msgid "Testing"
msgstr "Testning"

#: ../../english/intro/organization.data:164
msgid "Cloud Team"
msgstr "Molngruppen"

#: ../../english/intro/organization.data:168
msgid "Autobuilding infrastructure"
msgstr "Infrastruktur för automatiska byggen"

#: ../../english/intro/organization.data:170
msgid "Wanna-build team"
msgstr "Vill-bygga-gruppen"

#: ../../english/intro/organization.data:177
msgid "Buildd administration"
msgstr "Byggserveradministration"

#: ../../english/intro/organization.data:194
msgid "Documentation"
msgstr "Dokumentation"

#: ../../english/intro/organization.data:199
msgid "Work-Needing and Prospective Packages list"
msgstr "Paket med behov av arbete samt framtida paket"

#: ../../english/intro/organization.data:215
msgid "Press Contact"
msgstr "Presskontakt"

#: ../../english/intro/organization.data:217
msgid "Web Pages"
msgstr "Webbsidor"

#: ../../english/intro/organization.data:225
msgid "Planet Debian"
msgstr "Planet Debian"

#: ../../english/intro/organization.data:230
msgid "Outreach"
msgstr "Outreach"

#: ../../english/intro/organization.data:235
msgid "Debian Women Project"
msgstr "Debian Women-projektet"

#: ../../english/intro/organization.data:243
msgid "Community"
msgstr "Gemenskap"

#: ../../english/intro/organization.data:250
msgid ""
"To send a private message to all the members of the Community Team, use the "
"GPG key <a href=\"community-team-pubkey.txt"
"\">817DAE61E2FE4CA28E1B7762A89C4D0527C4C869</a>."
msgstr ""
"För att skicka ett privat meddelande till alla medlemmar i "
"gemenskapsgruppen, använd GPG-nyckeln <a href=\"community-team-pubkey.txt"
"\">817DAE61E2FE4CA28E1B7762A89C4D0527C4C869</a>."

#: ../../english/intro/organization.data:252
msgid "Events"
msgstr "Evenemang"

#: ../../english/intro/organization.data:259
msgid "DebConf Committee"
msgstr "DebConf-kommitté"

#: ../../english/intro/organization.data:266
msgid "Partner Program"
msgstr "Partnerprogrammet"

#: ../../english/intro/organization.data:270
msgid "Hardware Donations Coordination"
msgstr "Samordning av maskinvarudonationer"

#: ../../english/intro/organization.data:285
msgid "GNOME Foundation"
msgstr "GNOME Foundation"

#: ../../english/intro/organization.data:287
msgid "Linux Professional Institute"
msgstr "Linux Professional Institute"

#: ../../english/intro/organization.data:288
msgid "Linux Magazine"
msgstr "Linux Magazine"

#: ../../english/intro/organization.data:290
msgid "Linux Standards Base"
msgstr "Linux Standards Base"

#: ../../english/intro/organization.data:291
msgid "Free Standards Group"
msgstr "Free Standards Group"

#: ../../english/intro/organization.data:292
msgid ""
"OASIS: Organization\n"
"      for the Advancement of Structured Information Standards"
msgstr ""
"OASIS: Organization\n"
"      for the Advancement of Structured Information Standards"

#: ../../english/intro/organization.data:295
msgid ""
"OVAL: Open Vulnerability\n"
"      Assessment Language"
msgstr ""
"OVAL: Open Vulnerability\n"
"      Assessment Language"

#: ../../english/intro/organization.data:298
msgid "Open Source Initiative"
msgstr "Open Source Initiative"

#: ../../english/intro/organization.data:305
msgid "Bug Tracking System"
msgstr "Felrapporteringssystem"

#: ../../english/intro/organization.data:310
msgid "Mailing Lists Administration and Mailing List Archives"
msgstr "Sändlisteadministration och -arkiv"

#: ../../english/intro/organization.data:318
msgid "New Members Front Desk"
msgstr "Nymedlemsreceptionen"

#: ../../english/intro/organization.data:324
msgid "Debian Account Managers"
msgstr "Debiankontoadministratörer"

#: ../../english/intro/organization.data:328
msgid ""
"To send a private message to all DAMs, use the GPG key "
"57731224A9762EA155AB2A530CA8D15BB24D96F2."
msgstr ""
"För att skicka ett privatmeddelande till alla DAMs, använd GPG-nyckeln "
"57731224A9762EA155AB2A530CA8D15BB24D96F2."

#: ../../english/intro/organization.data:329
msgid "Keyring Maintainers (PGP and GPG)"
msgstr "Ansvariga för nyckelring (PGP och GPG)"

#: ../../english/intro/organization.data:333
msgid "Security Team"
msgstr "Säkerhetsgrupp"

#: ../../english/intro/organization.data:346
msgid "Policy"
msgstr "Policy"

#: ../../english/intro/organization.data:349
msgid "System Administration"
msgstr "Systemadministration"

#: ../../english/intro/organization.data:350
msgid ""
"This is the address to use when encountering problems on one of Debian's "
"machines, including password problems or you need a package installed."
msgstr ""
"Det här är adressen som skall användas när problem uppstår med en av Debians "
"datorer, inklusive lösenordsproblem, eller om du behöver få ett paket "
"installerat."

#: ../../english/intro/organization.data:359
msgid ""
"If you have hardware problems with Debian machines, please see <a href="
"\"https://db.debian.org/machines.cgi\">Debian Machines</a> page, it should "
"contain per-machine administrator information."
msgstr ""
"Om du har maskinvaruproblem med en av Debians datorer, se <a href=\"https://"
"db.debian.org/machines.cgi\">Debianmaskinsidan</a>, vilken innehåller "
"information om administratörer för varje enskild maskin."

#: ../../english/intro/organization.data:360
msgid "LDAP Developer Directory Administrator"
msgstr "Administratör för LDAP-utvecklarkatalogen"

#: ../../english/intro/organization.data:361
msgid "Mirrors"
msgstr "Speglingar"

#: ../../english/intro/organization.data:364
msgid "DNS Maintainer"
msgstr "DNS-ansvarig"

#: ../../english/intro/organization.data:365
msgid "Package Tracking System"
msgstr "Paketspårningssystem"

#: ../../english/intro/organization.data:367
msgid "Treasurer"
msgstr "Kassör"

#: ../../english/intro/organization.data:374
msgid ""
"<a name=\"trademark\" href=\"m4_HOME/trademark\">Trademark</a> use requests"
msgstr ""
"Förfrågningar angående <a href=\"m4_HOME/trademark\">varumärkesanvändning</a>"

#: ../../english/intro/organization.data:378
msgid "Salsa administrators"
msgstr "Salsaadministratörer"

#~ msgid "Alioth administrators"
#~ msgstr "Administratörer för Alioth"

#~ msgid "Anti-harassment"
#~ msgstr "Anti-trakasserier"

#~ msgid "Auditor"
#~ msgstr "Revisor"

#~ msgid "Bits from Debian"
#~ msgstr "Bitar från Debian"

#~ msgid "CD Vendors Page"
#~ msgstr "Cd-försäljarsida"

#~ msgid "Consultants Page"
#~ msgstr "Konsultsida"

#~ msgid "DebConf chairs"
#~ msgstr "Debconf-ledare"

#~ msgid "Debian Maintainer (DM) Keyring Maintainers"
#~ msgstr "Ansvariga för nyckelringen för Debian Maintainers (DM)"

#~ msgid "Debian Pure Blends"
#~ msgstr "Debian Pure Blends"

#~ msgid "Debian for astronomy"
#~ msgstr "Debian för astronomi"

#~ msgid "Debian for children from 1 to 99"
#~ msgstr "Debian för barn mellan 1 och 99"

#~ msgid "Debian for education"
#~ msgstr "Debian för utbildning"

#~ msgid "Debian for medical practice and research"
#~ msgstr "Debian för medicinsk praktik och forskning"

# Politiskt korrekt? :-)
#~ msgid "Debian for people with disabilities"
#~ msgstr "Debian för personer med funktionsnedsättningar"

#~ msgid "Debian for science and related research"
#~ msgstr "Debian för vetenskap och forskning"

#~ msgid "Debian in legal offices"
#~ msgstr "Debian för juridiska firmor"

#~ msgid "Embedded systems"
#~ msgstr "Inbyggda system"

#~ msgid "Firewalls"
#~ msgstr "Brandväggar"

#~ msgid "Handhelds"
#~ msgstr "Handhållna"

#~ msgid "Individual Packages"
#~ msgstr "Individuella paket"

#~ msgid "Key Signing Coordination"
#~ msgstr "Samordning av nyckelsignering"

#~ msgid "Laptops"
#~ msgstr "Bärbara"

#~ msgid "Live System Team"
#~ msgstr "Livesystem-gruppen"

#~ msgid "Marketing Team"
#~ msgstr "Marknadsföringsgrupp"

#~ msgid "Ports"
#~ msgstr "Anpassningar"

#~ msgid "Publicity"
#~ msgstr "PR"

#~ msgid "SchoolForge"
#~ msgstr "Schoolforge"

#~ msgid "Security Audit Project"
#~ msgstr "Projekt för säkerhetsgenomgång"

#~ msgid "Special Configurations"
#~ msgstr "Speciella system"

#~ msgid "Testing Security Team"
#~ msgstr "Uttestningsutgåvans säkerhetsgrupp"

#~ msgid "User support"
#~ msgstr "Användarstöd"

#~ msgid "Vendors"
#~ msgstr "Försäljare"

#~ msgid "Volatile Team"
#~ msgstr "Volatilegruppen"

#~ msgid "current Debian Project Leader"
#~ msgstr "Debians nuvarande projektledare"
