#use wml::debian::template title="Come fare donazioni al Progetto Debian" MAINPAGE="true"
#use wml::debian::translation-check translation="0906d2ac0aa2592f96bc55597ca2e65f02e3c945" maintainer="Luca Monducci"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<ul class="toc">
  <li><a href="#spi">Software in the Public Interest</a></li>
  <li><a href="#debianfrance">Debian France</a></li>
  <li><a href="#debianch">debian.ch</a></li>
  <li><a href="#debian">Debian</a></li>
</ul>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Ringraziamo tutti i
nostri donatori per le attrezzature o i servizi: gli <a
href="https://db.debian.org/machines.cgi">sponsor di hosting e hardware</a>,
gli <a href="mirror/sponsors">sponsor dei mirror</a>, i <a
href="partners/">collaboratori per lo sviluppo e servizi</a>.<p>
</aside>

<p>
Le donazioni sono gestite dal
<a href="$(HOME)/devel/leader">Leader del Progetto Debian</a> (DPL)
e permettono a Debian di ricevere
<a href="https://db.debian.org/machines.cgi">computer</a>,
<a href="https://wiki.debian.org/Teams/DSA/non-DSA-HW">hardware</a>,
domini, certificati crittografici; di organizzare
<a href="https://www.debconf.org">la conferenza Debian</a>,
<a href="https://wiki.debian.org/MiniDebConf">le mini-conferenze Debian</a>, gli
<a href="https://wiki.debian.org/Sprints">sprint di sviluppo</a>,
di finanziare la partecipazione ad alcuni eventi e altre cose.
</p>

<p id="default">
Varie <a href="https://wiki.debian.org/Teams/Treasurer/Organizations">organizzazioni</a>
detengono in amministrazione fiduciaria il patrimonio di Debian e possono
ricevere donazioni per conto di Debian. Questa pagina elenca i modi con cui
fare donazioni al Progetto Debian. Il modo più semplice per fare donazioni a
Debian è via PayPal, indirizzandole a <a href="https://www.spi-inc.org/"
title="SPI">Software in the Public Interest</a>, un'organizzazione non-profit
che detiene in amministrazione fiduciaria il patrimonio di Debian.
</p>

<p style="text-align:center"><button type="button"><span class="fab fa-paypal fa-2x"></span> <a
href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&amp;hosted_button_id=86AHSZNRR29UU">Donazione
tramite PayPal</a></button></p>

<aside class="light">
  <span class="fas fa-gifts fa-5x"></span>
</aside>

<table>
<tr>
<th>Organizzazione</th>
<th>Modalità</th>
<th>Note</th>
</tr>
<tr>
<td><a href="#spi"><acronym title="Software in the Public Interest">SPI</acronym></a></td>
<td>
 <a href="#spi-paypal">PayPal</a>,
 <a href="#spi-click-n-pledge">Click &amp; Pledge</a>,
 <a href="#spi-cheque">Assegni</a>,
 <a href="#spi-other">Altro</a>
</td>
<td>USA, non-profit esente da tassazione</td>
</tr>
<tr>
<td><a href="#debianfrance">Debian France</a></td>
<td>
 <a href="#debianfrance-bank">Bonifico</a>,
 <a href="#debianfrance-paypal">PayPal</a>
</td>
<td>Francia, non-profit esente da tassazione</td>
</tr>
<tr>
<td><a href="#debianch">debian.ch</a></td>
<td>
 <a href="#debianch-bank">Bonifico</a>,
 <a href="#debianch-other">Altro</a>
</td>
<td>Svizzera, non-profit</td>
</tr>
<tr>
<td><a href="#debian">Debian</a></td>
<td>
 <a href="#debian-equipment">Attrezzature</a>,
 <a href="#debian-time">Tempo</a>,
 <a href="#debian-other">Altro</a>
</td>
<td></td>
</tr>

# Template:
#<tr>
#<td><a href="#"><acronym title=""></acronym></a></td>
#<td>
# <a href="#"></a>,
# <a href="#"></a> (allows recurring donations),
# <a href="#cheque"></a> (CUR)
#</td>
#<td>, tax-exempt non-profit</td>
#</tr>

</table>

<h2 id="spi">Software in the Public Interest</h2>

<p>
<a href="https://www.spi-inc.org/" title="SPI">Software in the Public
Interest, Inc.</a> è una società non-profit esente da tassazione con sede
negli Stati Uniti d'America, fondata dai membri di Debian nel 1997 per
aiutare le organizzazioni che si occupano di software e hardware libero.
</p>

<h3 id="spi-paypal">PayPal</h3>

<p>
È possibile fare donazioni singole oppure ricorrenti tramite la <a
href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&amp;hosted_button_id=86AHSZNRR29UU">pagina
di SPI</a> sul sito di PayPal. Per fare donazioni ricorrenti selezionare la
casella <em>Make this a monthly donation</em>.
</p>

<h3 id="spi-click-n-pledge">Click &amp; Pledge</h3>

<p>
È possibile fare donazioni singole oppure ricorrenti tramite la <a
href="https://co.clickandpledge.com/advanced/default.aspx?wid=34115">pagina
di SPI</a> sul sito di Click &amp; Pledge. Per fare donazioni ricorrenti
impostare sulla destra la frequenza con cui si vuole fare una donazione,
scorrere la pagina in basso fino a trovare <em>Debian Project Donation</em>,
inserire l'importo che si vuole donare e poi fare clic su <em>Add to
cart</em>, quindi andare in alto a destra per concludere.
</p>

<h3 id="spi-cheque">Assegni</h3>

<p>
È possibile fare donazioni tramite assegni e vaglia in
<abbr title="dollari statunitensi">USD</abbr> e in
<abbr title="dollari canadesi">CAD</abbr>.
Scrivete <q>Debian</q> come causale e inviatelo a SPI all'indirizzo
riportato nella <a href="https://www.spi-inc.org/donations/">pagina
donazioni a SPI</a>.
</p>

<h3 id="spi-other">Altro</h3>

<p>
È possibile fare bonifici anche in altri modi.
In alcune parti del mondo potrebbe essere più semplice fare donazioni
tramite una delle organizzazioni legate a Software in the Public Interest.
Per ulteriori dettagli visitare la pagina
<a href="https://www.spi-inc.org/donations/">donazioni a SPI</a>.
</p>


<h2 id="debianfrance">Debian France</h2>

<p>
La <a href="https://france.debian.net/">Debian France Association</a>
è un'organizzazione registrata in Francia con la <q>legge 1901</q>,
fondata con lo scopo di aiutare e promuovere in Francia il Progetto
Debian.
</p>

<h3 id="debianfrance-bank">Bonifico</h3>

<p>
È possibile fare donazioni tramite bonifico sul conto corrente bancario
riportato nella <a href="https://france.debian.net/soutenir/#compte">pagina
donazioni sul sito Debian France</a>. È possibile chiedere la ricevuta
di ogni donazione inviando un'email a
<a href="mailto:donation@france.debian.net">donation@france.debian.net</a>.
</p>

<h3 id="debianfrance-paypal">PayPal</h3>

<p>
È possibile inviare una donazione dalla
<a href="https://france.debian.net/galette/plugins/paypal/form">pagina
PayPal di Debian France</a>. Questa donazione può essere destinata in
modo specifico a Debian France oppure in generale al Progetto Debian.
</p>


<h2 id="debianch">debian.ch</h2>

<p>
<a href="https://debian.ch/">debian.ch</a> è stata fondata per rappresentare
il progetto Debian all'interno della Confederazione svizzera e del Principato del
Liechtenstein.
</p>

<h3 id="debianch-bank">Bonifico</h3>

<p>
È possibile fare donazioni tramite bonifico dalla Svizzera tramite banche
internazionali sul conto corrente bancario riportato sul
<a href="https://debian.ch/">sito debian.ch</a>.
</p>

<h3 id="debianch-other">Altro</h3>

<p>
È possibile fare donazioni anche con altri metodi, consultare i riferimenti
riportati sul <a href="https://debian.ch/">sito debian.ch</a>.
</p>


# Template:
#<h3 id=""></h3>
#
#<p>
#</p>
#
#<h4 id=""></h4>
#
#<p>
#</p>


<h2 id="debian">Debian</h2>

<p>
Debian accetta donazioni in
<a href="#debian-equipment">attrezzature</a> ma in questo momento non in
<a href="#debian-other">altri modi</a>.
</p>

<h3 id="debian-equipment">Attrezzature e servizi</h3>

<p>
Debian dipende dalle donazioni da parte di aziende e università delle
attrezzature e dei servizi che ci permettono di mantenere Debian collegata
con il resto del mondo.
</p>

<p>
Se la vostra azienda possiede computer inutilizzati o materiale sfuso
(dischi, controller SCSI, schede di rete, ecc.), considerate la
possibilità di donarli a Debian. Mettetevi in contatto con il nostro
<a href="mailto:hardware-donations@debian.org">delegato alle donazioni
hardware</a> per i dettagli.
</p>

<p>
Debian mantiene aggiornato un <a
href="https://wiki.debian.org/Hardware/Wanted">elenco di materiale
richiesto</a> per i vari servizi e gruppi all'interno del Progetto.
</p>


<h3 id="debian-time">Tempo</h3>

<p>
È possibile <a href="$(HOME)/intro/help">aiutare Debian</a> in molti modi
donando il proprio tempo.
</p>

<h3 id="debian-other">Altro</h3>

<p>
Al momento Debian non è in grado di accettare donazioni in nessuna criptomoneta,
stiamo lavorando per poter ricevere queste donazioni.
# Brian Gupta requested we discuss this before including it:
#Chi vuol fare donazioni in criptomoneta oppure ha informazioni da condividere
#può mettersi in contatto con <a href="mailto:madduck@debian.org">Martin
#f. krafft</a>.
</p>
