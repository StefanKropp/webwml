#use wml::debian::template title="Debian in giapponese"
#use wml::debian::translation-check translation="78cd9008340d41673fcea2c66b3a83144e4a9903" maintainer="Giuseppe Sacco"

# This contents of this page is completely the responsibility of
# the translation team

<ul class="toc">
   <li><a href="#jp">Debian JP Project</a>
      <ul>
	<li><a href="#jpml">Mailing list in Giapponese</a></li>
      </ul>
   </li>
   <li><a href="#jpt">Traduzione in Giapponese</a>
      <ul>
	<li><a href="#jptpkg">Pacchetti</a></li>
	<li><a href="#jptdoc">Documentazione</a></li>
      </ul>
   </li>
</ul>


<h2 id="jp">Debian JP Project</h2>
<p><a href="https://www.debian.or.jp/">Debian JP Project</a> è un'associazione
   che ha lo scopo di diffondere l'uso di Debian. Per ulteriori informazioni consultare
   <a href="https://www.debian.or.jp/project/">Debian JP Project</a>.</p>

<h3 id="jpml">Mailing list Debian coordinate da Debian JP Project</h3>
<p>In queste mailing list viene utilizzata principalmente la lingua giapponese.
  Per ulteriori informazioni su come iscriversi alle liste consultare i collegamenti.</p>

<ul>
   <li>Utenti (tutto quello che rientra tra Come si installa Debian, a Come si usa il software y): <a
	href="https://www.debian.or.jp/community/ml/openml.html#usersML">Mailing list debian-users
	</a></li>
   <li>Sviluppatori (hai scoperto un bug? vuoi preparare il tuo pacchetto? ecc.): <a
	href="https://www.debian.or.jp/community/ml/openml.html#develML">Mailing list debian-devel
	</a></li>
   <li>doc (traduzione di documenti e pacchetti,
	Documents introduced on <a href="https://www.debian.org/doc/">/doc/</a>): <a
	href="https://www.debian.or.jp/community/ml/openml.html#docML">Mailing list debian-doc
	</a></li>
   <li>www (gestione e traduzione di siti web: www.debian.org e debian.jp): <a
	href="https://www.debian.or.jp/community/ml/openml.html#wwwML">Mailing list debian-www
	</a></li>
</ul>


<h2 id="jpt">Traduzione in Giapponese</h2>
<p>Di seguito sono riportate tutte le persone coinvolte nelle traduzioni in Giapponese</p>

<p>Per informazioni sulle problematiche specifiche della traduzione giapponese e sulla mancanza di volontari
  consultare <a href="https://wiki.debian.org/ja/L10n/Japanese">Riepilogo dei problemi del Giapponese</a>.</p>

<h3 id="jptwww">Tradurre www.debian.org in Giapponese</h3>
<p>i contenuti di www.debian.org sono gestiti usando <a
   href="https://www.debian.org/devel/website/using_cvs"><abbr
   title="Concurrent Versions System">cvs</abbr></a> e le pagine vengono generate utilizzando
   <a href="https://www.debian.org/devel/website/using_wml">
   <abbr title="Website Meta Language">wml</abbr></a>.
   Per ulteriori informazioni consultare <a
   href="https://www.debian.org/devel/website/">Aiutare con le pagine
   web Debian</a> . Qui ci sono le <a
   href="https://www.debian.org/devel/website/stats/ja">
   Statistiche di traduzione per il sito web</a>.</p>

<p>Fondamentalmente, tutte le traduzioni in Giapponese di www.debian.org e
   dei materiali correlati devono passare attraverso la mailing list debian-www
   (utilizzare la lingua giapponese). Potrebbero essere coinvolti nelle traduzioni
   anche <a href="https://lists.debian.org/debian-www/">debian-www</a>
   e <a href="https://lists.debian.org/">lists.debian.org</a> (in Inglese).
   <abbr title="Request For Review">RFR</abbr> per
   <a href="https://www.debian.org/News/weekly/"><abbr
   title="Debian Project News">DPN</abbr></a> prima della pubblicazione dei contenuti vengono
   inviate su www. Potrebbero riguardare argomenti importanti come problemi di
   licenze, per questo invitiamo tutti a seguirle con attenzione.
   I committer su <a href="https://www.debian.org/devel/website/using_cvs"><abbr
   title="Concurrent Versions System">cvs</abbr></a>
   dovrebbero leggere questo documento.  <a 
   href="https://www.debian.org/devel/website/translation_coordinators">\
   Inoltre, i Coordinatori delle traduzioni</a>, che hanno una funzione di coordinamento, sono tenuti ad iscriversi a
   <a href="https://lists.debian.org/debian-japanese/">debian-japanese</a>
   (principalmente in Inglese).</p>


<h3 id="jptpkg">Pacchetti</h3>
<p>La traduzione dei pacchetti Debian utilizza gettext.  Questo è un poco
   complicato perchè è diviso in due sezioni: tutto quello che riguarda
   la traduzione di pacchetti Debian ed il materiale relativo viene fatto
   su debian-doc (in Giapponese).
   Inoltre se hai dei problemi che non riesci in alcun modo a risolvere scrivi qui.</p>

<p>Materiali che influenzano altre traduzioni potrebbero dover essere fatti su <a
   href="https://lists.debian.org/debian-doc/">debian-doc</a> on <a
   href="https://lists.debian.org/">lists.debian.org</a> (in Inglese).
   Potrebbero riguardare argomenti importanti come problemi di
   licenze, per questo invitiamo a seguire con attenzione.<br />
   Talvolta delle Request for Update per una traduzione giapponese vengono inviate su
   <a href="https://lists.debian.org/debian-japanese/">debian-japanese</a>
   (in Inglese).  A volte delle Request for Update Translation e/o
   Call for New Translation vengono inviate su <a
   href="https://lists.debian.org/debian-i18n/">debian-i18n</a> (in Inglese).</p>

<ul>
   <li>Modelli debconf: <a
	href="https://www.debian.org/international/l10n/po-debconf/README-trans">\
	po-debconf</a>
	(<a href="https://www.debian.org/international/l10n/po-debconf/ja">Translation Stats</a>)</li>
   <li>At daily use: <a href="https://www.debian.org/international/l10n/po/">po</a>
	(<a href="https://www.debian.org/international/l10n/po/ja">Translation Stats</a>) /
	<a href="https://www.debian.org/international/l10n/po4a/">po4a</a>
	(<a href="https://www.debian.org/international/l10n/po4a/ja">Translation Stats</a>)</li>
   <li>Descrizione pacchetti: <a href="https://www.debian.org/international/l10n/ddtp">The
	Debian Description Translation Project (ddtp)</a></li>
</ul>

<h3 id="jptdoc">Documentazione</h3>
<p>La maggior parte della documentazione Debian utilizza gettext.  Tutto
   quello che riguarda la traduzione giapponese di documenti Debian e materiale relativo viene fatto
   su debian-doc (in Giapponese).
   Inoltre se hai dei problemi che non riesci in alcun modo a risolvere scrivi qui.</p>

<p>Materiali che influenzano altre traduzioni potrebbero dover essere fatti su
   <a href="https://lists.debian.org/debian-doc/">debian-doc</a> su <a
   href="https://lists.debian.org/">lists.debian.org</a> (in Inglese).
   Potrebbero riguardare argomenti importanti come problemi di
   licenze, per questo invitiamo a seguire con attenzione.</p>


<p>Tutta la regione è a corto di personale.  Se sei interessato alla traduzione
puoi contattarci direttamente tramite le mailing list.</p>

<div class="warning">
<p><strong>Tutte le mailing mail elencate in questa pagina sono pubbliche.
I messaggi vengono pubblicati su diversi host, la maggior
parte dei quali NON sono sotto il nostro controllo.
Non pubblicare mai informazioni private che non scriveresti su una cartolina.
A seguito dell'iscrizione alle mailing list il rischio di ricevere SPAM aumenterà.
Decliniamo ogni responsabilità in caso di danni causati dall'utilizzo delle mailing list.
Se lo ritenete opportuno, create un indirizzo da utilizzare esclusivamente per le mailing list.
Siate cauti e proteggetevi!</strong></p>
</div>
