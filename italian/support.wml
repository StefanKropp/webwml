#use wml::debian::template title="Supporto utente" MAINPAGE="true"
#use wml::debian::recent_list
#use wml::debian::translation-check translation="2b215c6e7169b28ab15c0eb330ed620ef7b21647" maintainer="Luca Monducci"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<ul class="toc">
  <li><a href="#irc">IRC (supporto in tempo reale)</a></li>
  <li><a href="#mail_lists">Liste di messaggi</a></li>
  <li><a href="#usenet">Newsgroups usenet</a></li>
  <li><a href="#forums">Forum di utenti Debian</a></li>
  <li><a href="#maintainers">Come contattare i manutentori dei pacchetti</a></li>
  <li><a href="#bts">Sistema tracciamento dei bug</a></li>
  <li><a href="#release">Problemi conosciuti</a></li>
</ul>


<aside>
<p><span class="fas fa-caret-right fa-3x"></span> L'assistenza Debian è
gestita da una comunità di volontari. Se l'assistenza fornita dalla
comunità non fosse soddisfacente per le proprie necessità è possibile
leggere la <a href="doc/">documentazione</a> oppure ingaggiare un <a
href="consultants/">consulente</a> per gestire o aggiungere funzionalità
al proprio sistema Debian.</p>
</aside>


<h2><a id="irc">IRC (supporto in tempo reale)</a></h2>

<p>
<a href="http://www.irchelp.org/">IRC (Internet Relay Chat)</a> è un
mezzo per chiacchierare con persone in tutto il mondo in tempo reale. È un
sistema di messaggistica istantanea testuale. Su IRC è possibile entrare in
delle stanze (chiamati canali) oppure chiaccherare direttamente con singole
persone tramite messaggi privati.
</p>

<p>
I canali IRC dedicati a Debian possono essere trovati su <a
href="https://www.oftc.net/">OFTC</a>. Per l'elenco complerto dei canali
Debian fare riferimento al <a href="https://wiki.debian.org/IRC">Wiki</a>.
È possibile usare anche un <a
href="https://netsplit.de/channels/index.en.php?net=oftc&chat=debian">motore
di ricerca</a> per trovare i canali legati a Debian.
</p>

<h3>Client IRC</h3>

<p>
Per collegarsi alla rete IRC è possibile usare la <a
href="https://www.oftc.net/WebChat/">WebChat</a> di OFTC tramite il proprio
browser preferito oppure installare un client IRC. Esistono moltissimi
client, alcuni con l'interfaccia grafica, altri per la console. Per alcuni
dei client IRC più popolati sono stati preparati i pacchetti per Debian,
per esempio:
</p>

<ul>
  <li><a href="https://packages.debian.org/stable/net/irssi">irssi</a> (testuale)</li>
  <li><a href="https://packages.debian.org/stable/net/weechat-curses">WeeChat</a> (testuale)</li>
  <li><a href="https://packages.debian.org/stable/net/hexchat">HexChat (GTK)</a></li>
  <li><a href="https://packages.debian.org/stable/net/konversation">Konversation</a> (KDE)</li>
</ul>

<p>
Su Debian Wiki è presente un <a href="https://wiki.debian.org/IrcClients">elenco
più completo di client IRC</a> disponibili come pacchetti Debian.
</p>

<h3>Collegarsi alla rete</h3>

<p>
Dopo aver avviato il client è necessario indicare a quale server collegarsi,
con la maggior parte dei client si fa digitando:
</p>

<pre>
/server irc.debian.org
</pre>

<p>Con alcuni client (tra i quali irssi) è necessario usare quest'altro comando:</p>

<pre>
/connect irc.debian.org
</pre>

# Note to translators:
# You might want to insert here a paragraph stating which IRC channel is available
# for user support in your language and pointing to the English IRC channel.
# <p>Once you are connected, join channel <code>#debian-foo</code> by typing</p>
# <pre>/join #debian</pre>
# for support in your language.
# <p>For support in English, read on</p>

<h3>Partecipare a un canale</h3>

<p>Una volta connesso è possibile raggiungere il canale <code>#debian</code>
digitando</p>

<pre>
/join #debian
</pre>

<p>Nota: client grafici come HexChat o Konversation hanno un bottone o una
voce nel menu per collegarsi ai server e ai canali.
</p>

<p style="text-align:center"><button type="button"><span
class="fas fa-book-open fa-2x"></span> <a
href="https://wiki.debian.org/DebianIRC">Consultare le FAQ su
IRC</a></button></p>


<h2><a id="mail_lists">Liste di messaggi</a></h2>


<p>Più di mille sviluppatori <a href="intro/people.en.html#devcont">sviluppatori</a>
da ogni parte del mondo lavorano su Debian nel loro tempo libero e ognuno
nel prorio fuso orario. Il mezzo preferito per comunicare e discutere è l'email, la
maggior parte delle conversazioni tra sviluppatori Debian e utenti è effettuata
attraverso le <a href="MailingLists/">liste di messaggi</a>:
</p>

# Note to translators:
# You might want to adapt the following paragraph, stating which list
# is available for user support in your language instead of English.

<ul>
 <li>Per il supporto in lingua italiano, contattare la <a
 href="https://lists.debian.org/debian-italian/">mailing list
 debian-italian</a>.</li>
 <li>Per il supporto in lingua inglese, contattare la <a 
 href="https://lists.debian.org/debian-user/">mailing list
 debian-user</a>.</li>
</ul>

<p>Ovviamente esistono molte altre liste di messaggi, dedicate ai diversi
aspetti di Linux, che non sono specifiche per Debian. Usare il motore di
ricerca preferito per trovare la lista più adatta ai propri scopi.
</p>


<h2><a id="usenet">Newsgroup usenet</a></h2>


<p>
Molte delle nostre <a href="#mail_lists">liste di messaggi</a> sono
consultabili come newsgroup all'interno della gerarchia <kbd>linux.debian.*</kbd>.
La consultazione di questi newsgroup può essere fatta anche utilizzando una
interfaccia web quale <a href="https://groups.google.com/forum/">Google Groups</a>.
</p>


<h2><a id="forums">Forum per utenti Debian</a></h2>


# Note to translators:
# If there is a specific Debian forum for your language you might want to
# insert here a paragraph stating which list is available for user support
# in your language and pointing to the English forums.
# <p><a href="http://someforum.example.org/">someforum</a> is a web portal
# on which you can use your language to discuss Debian-related topics,
# submit questions about Debian, and have them answered by other users.</p>
#
# <p><a href="https://forums.debian.net">Debian User Forums</a> is a web portal
# on which you can use the English language to discuss Debian-related topics,
# submit questions about Debian, and have them answered by other users.</p>

<p>
<a href="https://forums.debian.net">Debian User Forums</a> è un portale
web in cui migliaia di utenti discutono su temi legati a Debian, porre
domande e aiutarsi a vicenda rispondendo a tali domande.  Chi vuole può 
prendere parte alle diuscussioni o porre delle domande, per partecipare
occorre registrarsi.
</p>


<h2><a id="maintainers">Comunicare con i manutentori dei pacchetti</a></h2>


<p>
Ci sono due modi per comunicare con i manutentori dei pacchetti:
</p>


<ul>
  <li>Per segnalare un bug, inviare una <a href="bts">segnalazione del bug</a>;
  il manutentore riceverà una copia della segnalazione.</li>
  <li>Se si vuole semplicemente mettersi in contatto con un manutentore,
  allora si può usare uno speciale alias di posta elettronica disponibile
per ogni pacchetto:<br>
    &lt;<em>nome&nbsp;pacchetto</em>&gt;@packages.debian.org</li>
</ul>

<toc-add-entry name="doc" href="doc/">Documentazione</toc-add-entry>

<p>Un aspetto importante per qualsiasi sistema operativo è la documentazione,
nei manuali tecnici sono descritte le operazioni e come usare i programmi.
Tra le attività del lavoro per creare un sistema operativo di alta-qualità,
il Progetto Debian sta facendo ogni sforzo per fornire a tutti i propri utenti
la documentazione corretta in modo semplice e accessibile.</p>

<p>Consultare la <a href="doc/">pagina sulla documentazione</a> per la
lista dei manuali Debian e altri documenti, compresa la Guida
all'Installazione, le FAQ Debian e altra documentazione per gli utenti e
per gli sviluppatori.</p>


<toc-add-entry name="consultants" href="consultants/">Consulenti</toc-add-entry>


<h2><a id="bts">Sistema di Tracciamento dei Bug</a></h2>


<p>
La distribuzione Debian dispone di un sistema di <a href="Bugs/">tracciamento
dei bug</a> che registra in modo dettagliato i bug segnalati da utenti e
sviluppatori. A ogni bug è assegnato un numero che lo identifica anche
quando viene in qualche modo gestito e marcato come chiuso. È possibile 
segnalare un bug in due modi:
</p>

<ul>
  <li>si raccomanda l'uso del pacchetto Debian <q>reportbug</q>.</li>
  <li>in alternativa, è possibile segnalare un bug via email come descritto
  in questa <a href="Bugs/Reporting">pagina</a>.</li>
</ul>


<h2><a id="release">Problemi conosciuti</a></h2>


<p>Limiti e problemi gravi dell'attuale distribuzione stabile (se presenti)
sono descritti nella <a href="releases/stable/">pagina del rilascio</a>.</p>

<p>Fare particolare attenzione alle <a
href="releases/stable/releasenotes">note di rilascio</a> e alla <a
href="releases/stable/errata">errata</a>.</p>
