#use wml::debian::template title="Situación de los desarrolladores" MAINPAGE="true"
#use wml::debian::translation-check translation="bd2e76d96db915ccd0dee367a373417db7422565"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> ¿Dónde están situados los desarrolladores de Debian (DD)? Si un o una DD ha introducido las coordenadas de su domicilio en la base de datos de desarrolladores, este aparecerá en nuestro mapamundi.</p>
</aside>

<p>
El mapa mostrado más abajo está generado a partir de una <a href="developers.coords">lista de coordenadas de desarrolladores</A>
(de la que han sido eliminados los nombres) utilizando el
programa <a href="https://packages.debian.org/stable/graphics/xplanet">
xplanet</a>.
</p>

<img src="developers.map.jpeg" alt="Mapa del mundo">

<h2>Cómo añadir sus coordenadas</h2>

<p>
Si desea añadir sus coordenadas a su entrada de la base de datos, entre
en la <a href="https://db.debian.org">base de datos de desarrolladores de Debian</a>
y modifique su entrada. Si no conoce las coordenadas de su localidad,
puede usar <a href="https://www.openstreetmap.org">OpenStreetMap</a> para
averiguarlas. Busque su ciudad y pulse el icono que muestra unas flechas junto al
campo de búsqueda. Arrastre el marcador verde al mapa OSM y las coordenadas
se mostrarán en el campo <em>From</em> («Desde»).
</p>

<p>El formato para las coordenadas es uno de los siguientes:</p>

<dl> 
  <dt>Grados decimales</dt>
    <dd>El formato es <code>+-GGG.GGGGGGGGGGGGGGG</code>. Lo usan programas
    como Xearth y muchas otras webs de posicionamiento. La precisión está
    limitada a 4 ó 5 decimales.</dd>
  <dt>Grados Minutos (DGM)</dt>
    <dd>El formato es <code>+-GGGMM.MMMMMMMMMMMMM</code>. No es
    aritmético, sino una representación de dos unidades separadas: grados y
    minutos. Esta es la salida típica de algunos tipos de dispositivos GPS
    de mano y de los mensajes GPS en formato NMEA.</dd>
  <dt>Grados Minutos Segundos (DGMS)</dt>
    <dd>El formato es +-GGGMMSS.SSSSSSSSSSS. Como en DGM, no
    es aritmético sino una representación empaquetada de tres unidades separadas:
    grados, minutos y segundos. Esta salida se deriva típicamente
    de sitios web que devuelven tres valores para cada posición. Por ejemplo,
    <code>34:50:12.24523 Norte</code> podría ser la posición devuelta, y en
    DGMS sería <code>+0345012.24523</code>.</dd>
</dl> 

<p> 
<strong>Nota:</strong> <code>+</code> es Norte para la latitud y
Este para la longitud. Es importante especificar suficientes
ceros al comienzo para no hacer ambiguo el formato que se use si su posición
está a menos de 2 grados de un punto cero. 
</p>
