# Status: [open-for-edit]
# $Rev$
#use wml::debian::translation-check translation="9c0ebe940eaf29e78367427aea8e02f46fb70bcd"
<define-tag pagetitle>El soporte a largo plazo para Debian 8 llega al final de su ciclo de vida</define-tag>
<define-tag release_date>2020-07-09</define-tag>
#use wml::debian::news

##
## Translators:
## - if translating while the file is in publicity-team/announcements repo,
##   please ignore the translation-check header. Publicity team will add it
##   including the correct translation-check header when moving the file
##   to the web repo
##
## - if translating while the file is in webmaster-team/webwml repo,
##   please use the copypage.pl script to create the page in your language
##   subtree including the correct translation-check header.
##

<p>El equipo de soporte a largo plazo de Debian («Long Term Support» o LTS) anuncia que el soporte
a Debian 8 <q>jessie</q> ha alcanzado el final de su ciclo de vida el 30 de junio de 2020,
cinco años después de su publicación inicial el 26 de abril de 2015.</p>

<p>Debian no proporcionará más actualizaciones de seguridad para Debian 8.
Terceras partes soportarán un subconjunto de los paquetes de <q>jessie</q>. Se puede
encontrar información detallada en <a href="https://wiki.debian.org/LTS/Extended">
Extended LTS</a>.</p>

<p>El equipo LTS preparará la transición a Debian 9 <q>stretch</q>, que es la
actual versión «antigua estable». El equipo LTS se hizo cargo del soporte, relevando al
equipo de seguridad, el 6 de julio de 2020, y la última versión de <q>stretch</q> se
publicará el 18 de julio de 2020.</p>

<p>Debian 9 también recibirá soporte a largo plazo durante cinco años tras su
publicación inicial, finalizando su soporte el 30 de junio de 2022. Siguen estando
soportadas las arquitecturas amd64, i386, armel y armhf. Y, además, nos
complace anunciar que, por primera vez, se ampliará el soporte para
incluir la arquitectura arm64.</p>

<p>Para más información sobre cómo usar <q>stretch</q> LTS y cómo actualizar desde <q>jessie</q>
LTS, consulte <a href="https://wiki.debian.org/LTS/Using">LTS/Using</a>.</p>

<p>Debian y su equipo LTS agradecen sus contribuciones a los usuarios,
desarrolladores y patrocinadores que hacen posible extender la vida
de versiones «estables» antiguas y que han hecho de LTS un éxito.</p>

<p>Si depende de Debian LTS, por favor, considere
<a href="https://wiki.debian.org/LTS/Development">unirse al equipo</a>,
proporcionar parches, hacer pruebas o 
<a href="https://wiki.debian.org/LTS/Funding">financiar estas actividades</a>.</p>

<h2>Acerca de Debian</h2>

<p>
El proyecto Debian fue fundado en 1993 por Ian Murdock para ser un proyecto comunitario 
verdaderamente libre. Desde entonces el proyecto ha crecido hasta ser uno de 
los proyectos más grandes e importantes de software libre. Miles de voluntarios 
de todo el mundo trabajan juntos para crear y mantener programas para Debian. 
Se encuentra traducido a 70 idiomas y soporta una gran cantidad de arquitecturas 
de ordenadores, por lo que el proyecto se refiere a sí mismo como 
<q>el sistema operativo universal</q>. 
</p>

<h2>Más información</h2>
<p>Puede encontrar más información sobre el soporte a largo plazo de Debian en
<a href="https://wiki.debian.org/LTS/">https://wiki.debian.org/LTS/</a>.</p>

<h2>Información de contacto</h2>

<p>Para más información, visite las páginas web de Debian en
<a href="$(HOME)/">https://www.debian.org/</a> o envíe un correo
electrónico a &lt;press@debian.org&gt;.</p>
