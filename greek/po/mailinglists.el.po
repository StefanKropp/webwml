# Emmanuel Galatoulas <galas@tee.gr>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"PO-Revision-Date: 2019-07-14 01:47+0200\n"
"Last-Translator: Emmanuel Galatoulas <galas@tee.gr>\n"
"Language-Team: Greek <debian-www@lists.debian.org>\n"
"Language: el\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 2.0\n"

#: ../../english/MailingLists/mklist.tags:6
msgid "Mailing List Subscription"
msgstr "Συνδρομή σε κατάλογο"

#: ../../english/MailingLists/mklist.tags:9
msgid ""
"See the <a href=\"./#subunsub\">mailing lists</a> page for information on "
"how to subscribe using e-mail. An <a href=\"unsubscribe\">unsubscription web "
"form</a> is also available, for unsubscribing from mailing lists. "
msgstr ""
"Δείτε τη σελίδα  <a href=\"./#subunsub\">Συνδρομητικοί κατάλογοι</a> για "
"πληροφορίες εγγραφής μέσω ηλ. ταχυδρομείου. Υπάρχει επίσης διαθέσιμη μια "
"σελίδα <a href=\"unsubscribe\">διαγραφής</a> για την κατάργηση συνδρομής. "

#: ../../english/MailingLists/mklist.tags:12
msgid ""
"Note that most Debian mailing lists are public forums. Any mails sent to "
"them will be published in public mailing list archives and indexed by search "
"engines. You should only subscribe to Debian mailing lists using an e-mail "
"address that you do not mind being made public."
msgstr ""
"Σημειώστε ότι οι περισσότερες λίστες αλληλογραφίας του Debian είναι δημόσια "
"φόρουμ. Οποιαδήποτε μηνύματα στέλνονται σ' αυτές θα δημοσιευθούν σε δημόσια "
"αρχειά λιστών αλληλογραφίας και θα καταλογογραφηθούν από μηχανές αναζήτησης."
"Θα πρέπει να εγγράφεστε στις λίστες αλληλογραφίας του Debian χρησιμοποιώντας "
"μόνο μια ηλεκτρονική διεύθυνση που δεν σας νοιάζει να φαίνεται δημόσια."

#: ../../english/MailingLists/mklist.tags:15
msgid ""
"Please select which lists you want to subscribe to (the number of "
"subscriptions is limited, if your request doesn't succeed, please use <a "
"href=\"./#subunsub\">another method</a>):"
msgstr ""
"Παρακαλούμε επιλέξτε τις λίστες στις οποίες θέλετε να εγγραφείτε (ο αριθμός "
"εγγραφών είναι περιορισμένος, αν το αίτημά σας δεν είναι επιτυχές, "
"παρακαλούμε χρησιμοποιείστε μια <a href=\"./#subunsub\">άλλη μέθοδο</a>):"

#: ../../english/MailingLists/mklist.tags:18
msgid "No description given"
msgstr "Δεν δόθηκε περιγραφή"

#: ../../english/MailingLists/mklist.tags:21
msgid "Moderated:"
msgstr "Ελεγχόμενη:"

#: ../../english/MailingLists/mklist.tags:24
msgid "Posting messages allowed only to subscribers."
msgstr "Αποστολή μηνυμάτων μόνο από συνδρομητές."

#: ../../english/MailingLists/mklist.tags:27
msgid ""
"Only messages signed by a Debian developer will be accepted by this list."
msgstr ""
"Αυτή η λίστα λαμβάνει μηνύματα που είναι υπογεγραμένα μόνο από Debian "
"Προγραμματιστή."

#: ../../english/MailingLists/mklist.tags:30
msgid "Subscription:"
msgstr "Συνδρομή:"

#: ../../english/MailingLists/mklist.tags:33
msgid "is a read-only, digestified version."
msgstr "είναι μια αποδελτιωμένη εκδοχή μόνο για ανάγνωση."

#: ../../english/MailingLists/mklist.tags:36
msgid "Your E-Mail address:"
msgstr "Η Ηλεκτρονική σας Διεύθυνση:"

#: ../../english/MailingLists/mklist.tags:39
msgid "Subscribe"
msgstr "Συνδρομή"

#: ../../english/MailingLists/mklist.tags:42
msgid "Clear"
msgstr "Καθαρισμός"

#: ../../english/MailingLists/mklist.tags:45
msgid ""
"Please respect the <a href=\"./#ads\">Debian mailing list advertising "
"policy</a>."
msgstr ""
"Παρακαλούμε σεβαστείτε την <a href=\"./#ads\">Πολιτική διαφημίσεων "
"συνδρομητικών καταλόγων του Debian</a>."

#: ../../english/MailingLists/mklist.tags:48
msgid "Mailing List Unsubscription"
msgstr "Κατάργηση Συνδρομής"

#: ../../english/MailingLists/mklist.tags:51
msgid ""
"See the <a href=\"./#subunsub\">mailing lists</a> page for information on "
"how to unsubscribe using e-mail. An <a href=\"subscribe\">subscription web "
"form</a> is also available, for subscribing to mailing lists. "
msgstr ""
"Δείτε τη σελίδα <a href=\"./#subunsub\">Συνδρομητικοί κατάλογοι</a> για "
"πληροφορίες διαγραφής μέσω ηλ. ταχυδρομείου. Υπάρχει επίσης διαθέσιμη μια "
"σελίδα <a href=\"subscribe\">εγγραφής</a> για την δημιουργία συνδρομής. "

#: ../../english/MailingLists/mklist.tags:54
msgid ""
"Please select which lists you want to unsubscribe from (the number of "
"unsubscriptions is limited, if your request doesn't succeed, please use <a "
"href=\"./#subunsub\">another method</a>):"
msgstr ""
"Παρακαλούμε επιλέξτε τις λίστες από τις οποίες θέλετε να διαγραφείτε (ο "
"αριθμός των διαγραφών είναι  περιορισμένος οπότε αν το αίτημά σας δεν είναι "
"επιτυχές παρακαλούμε χρησιμοποιήστε μια <a href=\"./#subunsub\">άλλη μέθοδο</"
"a>):"

#: ../../english/MailingLists/mklist.tags:57
msgid "Unsubscribe"
msgstr "Κατάργηση"

#: ../../english/MailingLists/mklist.tags:60
msgid "open"
msgstr "ανοικτό"

#: ../../english/MailingLists/mklist.tags:63
msgid "closed"
msgstr "κλειστό"

#~ msgid "Please select which lists you want to subscribe to:"
#~ msgstr "Επιλέξτε σε ποιους καταλόγους θέλετε συνδρομή:"

#~ msgid "Please select which lists you want to unsubscribe from:"
#~ msgstr "Παρακαλούμε επιλέξτε τους καταλόγους για κατάργηση συνδρομής:"
