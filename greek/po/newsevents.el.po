# Emmanuel Galatoulas <galas@tee.gr>, 2019.
# EG <galatoulas@cti.gr>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"PO-Revision-Date: 2021-01-22 03:14+0200\n"
"Last-Translator: EG <galatoulas@cti.gr>\n"
"Language-Team: Greek <debian-l10n-greek@lists.debian.org>\n"
"Language: el\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 20.12.0\n"

#: ../../english/News/news.rdf.in:16
msgid "Debian News"
msgstr "Νέα του Debian"

#: ../../english/News/news.rdf.in:19
msgid "Debian Latest News"
msgstr "Τελευταία Νέα του Debian"

#: ../../english/News/press/press.tags:11
msgid "p<get-var page />"
msgstr "p<get-var page />"

#: ../../english/News/weekly/dwn-to-rdf.pl:143
msgid "The newsletter for the Debian community"
msgstr "Το ενημερωτικό δελτίο για την κοινότητα του Debian"

#: ../../english/events/talks.defs:9
msgid "Title:"
msgstr "Τίτλος:"

#: ../../english/events/talks.defs:12
msgid "Author:"
msgstr "Συγγραφέας:"

#: ../../english/events/talks.defs:15
msgid "Language:"
msgstr "Γλώσσα:"

#: ../../english/events/talks.defs:19
msgid "Date:"
msgstr "Ημερομηνία:"

#: ../../english/events/talks.defs:23
msgid "Event:"
msgstr "Γεγονός:"

#: ../../english/events/talks.defs:26
msgid "Slides:"
msgstr "Διαφάνειες:"

#: ../../english/events/talks.defs:29
msgid "source"
msgstr "πηγή"

#: ../../english/events/talks.defs:32
msgid "PDF"
msgstr "PDF"

#: ../../english/events/talks.defs:35
msgid "HTML"
msgstr "HTML"

#: ../../english/events/talks.defs:38
msgid "MagicPoint"
msgstr "MagicPoint"

#: ../../english/events/talks.defs:41
msgid "Abstract"
msgstr "Περίληψη"

#: ../../english/template/debian/events_common.wml:8
msgid "Upcoming Attractions"
msgstr "Προσεχή ενδιαφέροντα γεγονότα"

#: ../../english/template/debian/events_common.wml:11
msgid "link may no longer be valid"
msgstr "Ο σύνδεσμος ίσως να μην είναι πλέον έγκυρος"

#: ../../english/template/debian/events_common.wml:14
msgid "When"
msgstr "Πότε"

#: ../../english/template/debian/events_common.wml:17
msgid "Where"
msgstr "Πού"

#: ../../english/template/debian/events_common.wml:20
msgid "More Info"
msgstr "Περισσότερες πληροφορίες"

#: ../../english/template/debian/events_common.wml:23
msgid "Debian Involvement"
msgstr "Συμμετοχή του Debian"

#: ../../english/template/debian/events_common.wml:26
msgid "Main Coordinator"
msgstr "Κύριος συντονιστής"

#: ../../english/template/debian/events_common.wml:29
msgid "<th>Project</th><th>Coordinator</th>"
msgstr "<th>Συντονιστής της</th><th>εργασίας</th>"

#: ../../english/template/debian/events_common.wml:32
msgid "Related Links"
msgstr "Σχετικοί σύνδεσμοι"

#: ../../english/template/debian/events_common.wml:35
msgid "Latest News"
msgstr "Τελευταία Νέα"

#: ../../english/template/debian/events_common.wml:38
msgid "Download calendar entry"
msgstr "Λήψη ημερολογιακής εγγραφής"

#: ../../english/template/debian/news.wml:9
msgid ""
"Back to: other <a href=\"./\">Debian news</a> || <a href=\"m4_HOME/\">Debian "
"Project homepage</a>."
msgstr ""
"Πίσω στην: άλλα <a href=\"./\">Νέα του Debian</a> || <a href=\"m4_HOME/"
"\">Κεντρική Σελίδα του Σχεδίου Debian </a>."

#. '<get-var url />' is replaced by the URL and must not be translated.
#. In English the final line would look like "<http://broken.com (dead.link)>"
#: ../../english/template/debian/news.wml:17
msgid "<get-var url /> (dead link)"
msgstr "<get-var url /> (μη ενεργός σύνδεσμος)"

#: ../../english/template/debian/projectnews/boilerplates.wml:35
msgid ""
"Welcome to this year's <get-var issue /> issue of DPN, the newsletter for "
"the Debian community. Topics covered in this issue include:"
msgstr ""
"Καλώς ήρθαtε στο φετινό τεύχος <get-var issue /> τεύχος των Νέων του Σχεδίου "
"Debian, του ενημερωτικού δελτίου για την κοινό  του Debian. Θέματα που "
"καλύπτονται στο παρόν τεύχος περιλαμβάνουν:"

#: ../../english/template/debian/projectnews/boilerplates.wml:43
msgid ""
"Welcome to this year's <get-var issue /> issue of DPN, the newsletter for "
"the Debian community."
msgstr ""
"Καλώς ήρθαtε στο φετινό τεύχος <get-var issue /> τεύχος των Νέων του Σχεδίου "
"Debian, του ενημερωτικού δελτίου για την κοινότητα του Debian."

#: ../../english/template/debian/projectnews/boilerplates.wml:49
msgid "Other topics covered in this issue include:"
msgstr "Άλλα θέματα που καλύπτονται στο παρόν τεύχος περιλαμβάνουν:¨"

#: ../../english/template/debian/projectnews/boilerplates.wml:69
#: ../../english/template/debian/projectnews/boilerplates.wml:90
msgid ""
"According to the <a href=\"https://udd.debian.org/bugs.cgi\">Bugs Search "
"interface of the Ultimate Debian Database</a>, the upcoming release, Debian  "
"<q><get-var release /></q>, is currently affected by <get-var testing /> "
"Release-Critical bugs. Ignoring bugs which are easily solved or on the way "
"to being solved, roughly speaking, about <get-var tobefixed /> Release-"
"Critical bugs remain to be solved for the release to happen."
msgstr ""
"Σύμφωνα με τη σελίδα <a href=\"https://udd.debian.org/bugs.cgi\">Bugs Search "
"interface of the Ultimate Debian Database</a>, η επικείμενη έκδοση, Debian "
"<q><get-var release /></q>, επηρεάζεται αυτή τη στιγμή από  <get-var testing/"
"> κρίσιμα-για-την-έκδοση (Release-Critical) σφάλματα. Αγνοώντας σφάλματα που "
"είναι εύκολο να επιλυθούν ή κοντά στο να επιλυθούν, αδρά μιλώντας, "
"παραμένουν προς επίλυση περίπου <get-var tobefixed /> κρίσιμα-για-την-έκδοση "
"σφάλματα για να μπορέσει να κυκλοφορήσει η έκδοση."

#: ../../english/template/debian/projectnews/boilerplates.wml:70
msgid ""
"There are also some <a href=\"https://wiki.debian.org/ProjectNews/RC-Stats"
"\">hints on how to interpret</a> these numbers."
msgstr ""
"Υπάρχουν επίσης και κάποιες <a href=\"https://wiki.debian.org/ProjectNews/RC-"
"Stats\"> υποδείξεις σχετικά με το πώς να ερμηνεύσετε </a> αυτούς τους "
"αριθμούς."

#: ../../english/template/debian/projectnews/boilerplates.wml:91
msgid ""
"There are also <a href=\"<get-var url />\">more detailed statistics</a> as "
"well as some <a href=\"https://wiki.debian.org/ProjectNews/RC-Stats\">hints "
"on how to interpret</a> these numbers."
msgstr ""
"Υπάρχουν επίσης <a href=\"<get-var url />\">πιο λεπτομερείς στατιστικές</"
"a>καθώς και κάποιες <a href=\"https://wiki.debian.org/ProjectNews/RC-Stats"
"\">υποδείξεις για το πώς να ερμηνεύσετε </a> αυτούς τους αριθμούς."

#: ../../english/template/debian/projectnews/boilerplates.wml:115
msgid ""
"<a href=\"<get-var link />\">Currently</a> <a href=\"m4_DEVEL/wnpp/orphaned"
"\"><get-var orphaned /> packages are orphaned</a> and <a href=\"m4_DEVEL/"
"wnpp/rfa\"><get-var rfa /> packages are up for adoption</a>: please visit "
"the complete list of <a href=\"m4_DEVEL/wnpp/help_requested\">packages which "
"need your help</a>."
msgstr ""
"<a href=\"<get-var link />\">Αυτή τη στιγμή</a> <a href=\"m4_DEVEL/wnpp/"
"orphaned\"><get-var orphaned /> πακέτα είναι ορφανά</a> και <a href="
"\"m4_DEVEL/wnpp/rfa\"><get-var rfa /> πακέτα είναι διαθέσιμα για υιοθεσία </"
"a>: παρακαλούμε επισκεφθείτε την πλήρη λίστα με τα <a href=\"m4_DEVEL/wnpp/"
"help_requested\">πακέτα που χρειάζονται τη βοήθειά σας</a>."

#: ../../english/template/debian/projectnews/boilerplates.wml:127
msgid ""
"Please help us create this newsletter. We still need more volunteer writers "
"to watch the Debian community and report about what is going on. Please see "
"the <a href=\"https://wiki.debian.org/ProjectNews/HowToContribute"
"\">contributing page</a> to find out how to help. We're looking forward to "
"receiving your mail at <a href=\"mailto:debian-publicity@lists.debian.org"
"\">debian-publicity@lists.debian.org</a>."
msgstr ""
"Παρακαλούμε βοηθήστε μας στη δημιουργία αυτού του ενημερωτικού δελτίου. "
"Χρειαζόμαστε περισσότερους εθελοντές συγγραφείς να παρακολουθούν την "
"κοινότητα του Debian και να κάνουν αναφορές σχετικά με αυτά που συμβαίνουν. "
"Παρακαλούμε δείτε τη σελίδα <a href=\"https://wiki.debian.org/ProjectNews/"
"HowToContribute\">Πώς να συνεισφέρετε</a> για να βρείτε πώς μπορείτε να "
"βοηθήσετε. Περιμένουμε με χαρά να λάβουμε το μήνυμά σας στη λίστα "
"αλληλογραφίας <a href=\"mailto:debian-publicity@lists.debian.org\">debian-"
"publicity@lists.debian.org</a>."

#: ../../english/template/debian/projectnews/boilerplates.wml:188
msgid ""
"Please note that these are a selection of the more important security "
"advisories of the last weeks. If you need to be kept up to date about "
"security advisories released by the Debian Security Team, please subscribe "
"to the <a href=\"<get-var url-dsa />\">security mailing list</a> (and the "
"separate <a href=\"<get-var url-bpo />\">backports list</a>, and <a href="
"\"<get-var url-stable-announce />\">stable updates list</a>) for "
"announcements."
msgstr ""
"Παρακαλούμε σημειώστε ότι αυτή είναι μόνο μια συλλογή από τις πιο σημαντικές "
"συμβουλές ασφαλείας των τελευταίων εβδομάδων. Αν χρειάζεστε να είστε πάντα "
"ενήμεροι σχετικά με τις συμβουλές ασφαλείας που εκδίδει η ομάδα ασφαλείας "
"του Debian, παρακαλούμε εγγραφείτε στη <a href=\"<get-var url-dsa />\">λίστα "
"αλληλογραφίας για την ασφάλεια</a> (και την ξεχωριστή <a href=\"<get-var url-"
"bpo />\">λίστα backports</a>, και την <a href=\"<get-var url-stable-"
"announce />\">λίστα αναβαθμίσεων της σταθερής διανομής</a>) για ανακοινώσεις."

#: ../../english/template/debian/projectnews/boilerplates.wml:189
msgid ""
"Please note that these are a selection of the more important security "
"advisories of the last weeks. If you need to be kept up to date about "
"security advisories released by the Debian Security Team, please subscribe "
"to the <a href=\"<get-var url-dsa />\">security mailing list</a> (and the "
"separate <a href=\"<get-var url-bpo />\">backports list</a>, and <a href="
"\"<get-var url-stable-announce />\">stable updates list</a> or <a href="
"\"<get-var url-volatile-announce />\">volatile list</a>, for <q><get-var old-"
"stable /></q>, the oldstable distribution) for announcements."
msgstr ""
"Παρακαλούμε σημειώστε ότι αυτή είναι μόνο μια επιλογή από τις σημαντικότερες "
"συμβουλές ασφαλείας των τελευταίων εβδομάδων. Αν θέλετε να είστε πάντα "
"ενημερωμένοι για τις συμβουλές ασφαλείας που εκδίδονται από την ομάδα "
"ασφαλείας του Debian, παρακαλούμε εγγραφείτε στη <a href=\"<get-var url-dsa /"
">\">λίστα αλληλογραφίας για την ασφάλεια</a> (και την ξεχωριστή <a href="
"\"<get-var url-bpo />\">λίστα των backports</a>, και τη <a href=\"<get-var "
"url-stable-announce />\">λίστα αναβαθμίσεων της σταθερής διανομής</a> ή τη "
"<a href=\"<get-var url-volatile-announce />\">λίστα volatile</a>, για "
"<q><get-var old-stable /></q>, την διανομή oldstable) για ανακοινώσεις."

#: ../../english/template/debian/projectnews/boilerplates.wml:198
msgid ""
"Debian's Stable Release Team released an update announcement for the "
"package: "
msgstr ""
"Η Ομάδα κυκλοφορίας της Σταθερής έκδοσης του Debian εξέδωσε μια ανακοίνωση "
"επικαιροποίησης για το πακέτο: "

#: ../../english/template/debian/projectnews/boilerplates.wml:200
#: ../../english/template/debian/projectnews/boilerplates.wml:213
#: ../../english/template/debian/projectnews/boilerplates.wml:226
#: ../../english/template/debian/projectnews/boilerplates.wml:357
#: ../../english/template/debian/projectnews/boilerplates.wml:371
msgid ", "
msgstr ", "

#: ../../english/template/debian/projectnews/boilerplates.wml:201
#: ../../english/template/debian/projectnews/boilerplates.wml:214
#: ../../english/template/debian/projectnews/boilerplates.wml:227
#: ../../english/template/debian/projectnews/boilerplates.wml:358
#: ../../english/template/debian/projectnews/boilerplates.wml:372
msgid " and "
msgstr " και "

#: ../../english/template/debian/projectnews/boilerplates.wml:202
#: ../../english/template/debian/projectnews/boilerplates.wml:215
#: ../../english/template/debian/projectnews/boilerplates.wml:228
msgid ". "
msgstr ". "

#: ../../english/template/debian/projectnews/boilerplates.wml:202
#: ../../english/template/debian/projectnews/boilerplates.wml:215
#: ../../english/template/debian/projectnews/boilerplates.wml:228
msgid "Please read them carefully and take the proper measures."
msgstr "Παρακαλούμε διαβάστε τις προσεκτικά και πάρτε τα κατάλληλα μέτρα."

#: ../../english/template/debian/projectnews/boilerplates.wml:211
msgid "Debian's Backports Team released advisories for these packages: "
msgstr "Η Ομάδα των  Backport εξέδωσε συμβουλές ασφαλείας γι' αυτά τα πακέτα: "

#: ../../english/template/debian/projectnews/boilerplates.wml:224
msgid ""
"Debian's Security Team recently released advisories for these packages "
"(among others): "
msgstr ""
"Η Ομάδα ασφαλείας του Debian εξέδωσε πρόσφατα συμβουλές ασφαλείας γι' αυτά "
"τα πακέτα (μεταξύ άλλων): "

#: ../../english/template/debian/projectnews/boilerplates.wml:253
msgid ""
"<get-var num-newpkg /> packages were added to the unstable Debian archive "
"recently."
msgstr ""
"<get-var num-newpkg /> πακέτα προστέθηκαν στην αρχειοθήκη της ασταθούς "
"διανομής του Debian πρόσφατα."

#: ../../english/template/debian/projectnews/boilerplates.wml:255
msgid " <a href=\"<get-var url-newpkg />\">Among many others</a> are:"
msgstr " <a href=\"<get-var url-newpkg />\">Μεταξύ πολλών άλλων</a> είναι:"

#: ../../english/template/debian/projectnews/boilerplates.wml:282
msgid "There are several upcoming Debian-related events:"
msgstr "Υπάρχουν αρκετά επικείμενα γεγονότα που σχετίζονται με το Debian:"

#: ../../english/template/debian/projectnews/boilerplates.wml:288
msgid ""
"You can find more information about Debian-related events and talks on the "
"<a href=\"<get-var events-section />\">events section</a> of the Debian web "
"site, or subscribe to one of our events mailing lists for different regions: "
"<a href=\"<get-var events-ml-eu />\">Europe</a>, <a href=\"<get-var events-"
"ml-nl />\">Netherlands</a>, <a href=\"<get-var events-ml-ha />\">Hispanic "
"America</a>, <a href=\"<get-var events-ml-na />\">North America</a>."
msgstr ""
"Μπορείτε να βρείτε περισσότερες πληροφορίες για γεγονότα σχετικά με το "
"Debian και ομιλίες στην ενότητα των<a href=\"<get-var events-section />"
"\">Γεγονότων</a> στον ιστότοπο του Debian, ή να εγγραφείτε σε μια από τις "
"λίστες αλληλογραφίας μας για γεγονότα και εκδηλώσεις σε διάφορες περιοχές: "
"<a href=\"<get-var events-ml-eu />\">Ευρώπη</a>, <a href=\"<get-var events-"
"ml-nl />\">Ολλανδία</a>, <a href=\"<get-var events-ml-ha />\">Ισπανόφωνη "
"Αμερική</a>, <a href=\"<get-var events-ml-na />\">Βόρεια Αμερική</a>."

#: ../../english/template/debian/projectnews/boilerplates.wml:313
msgid ""
"Do you want to organise a Debian booth or a Debian install party? Are you "
"aware of other upcoming Debian-related events? Have you delivered a Debian "
"talk that you want to link on our <a href=\"<get-var events-talks />\">talks "
"page</a>? Send an email to the <a href=\"<get-var events-team />\">Debian "
"Events Team</a>."
msgstr ""
"Θέλεις να οργανώσεις ένα περίπτερο για το Debian ή ένα πάρτυ εγκατάστασης "
"του Debian; Έχεις υπόψιν σου άλλα επικείμενα γεγονότα σχετικά με το Debian; "
"Έχεις δώσει κάποια ομιλίας σχετικά με το Debian για την οποία θα ήθελες να "
"μπει ένας σύνδεσμος στη σελίδα μας <a href=\"<get-var events-talks />"
"\">Ομιλίες</a>; Στείλε ένα μήνυμα στη λίστα αλληλογραφίας <a href=\"<get-var "
"events-team />\">Ομάδα Εκδηλώσεων του Debian</a>."

#: ../../english/template/debian/projectnews/boilerplates.wml:335
msgid ""
"<get-var dd-num /> applicants have been <a href=\"<get-var dd-url />"
"\">accepted</a> as Debian Developers"
msgstr ""
"<get-var dd-num /> υποψήφιοι έχουν γίνει  <a href=\"<get-var dd-url />"
"\">δεκτοί</a> ως προγραμματιστές του Debian"

#: ../../english/template/debian/projectnews/boilerplates.wml:342
msgid ""
"<get-var dm-num /> applicants have been <a href=\"<get-var dm-url />"
"\">accepted</a> as Debian Maintainers"
msgstr ""
"<get-var dm-num /> αιτούντες/σες έχουν γίνει <a href=\"<get-var dm-url />"
"\">δεκτοί</a> ως Συντηρητές/τριες του Debian"

#: ../../english/template/debian/projectnews/boilerplates.wml:349
msgid ""
"<get-var uploader-num /> people have <a href=\"<get-var uploader-url />"
"\">started to maintain packages</a>"
msgstr ""
"<get-var uploader-num />άτομα έχουν <a href=\"<get-var uploader-url />"
"\">αρχίσει να συντηρούν πακέτα</a>"

#: ../../english/template/debian/projectnews/boilerplates.wml:394
msgid ""
"<get-var eval-newcontributors-text-list /> since the previous issue of the "
"Debian Project News. Please welcome <get-var eval-newcontributors-name-list /"
"> into our project!"
msgstr ""
"<get-var eval-newcontributors-text-list /> από το προηγούμενο τεύχος "
"τουΔελτίου Ειδήσεων του Σχεδίου Debian. Παρακαλούμε, καλωσορίστε <get-var "
"eval-newcontributors-name-list /> στο Σχέδιό μας!"

#: ../../english/template/debian/projectnews/boilerplates.wml:407
msgid ""
"The <get-var issue-devel-news /> issue of the <a href=\"<get-var url-devel-"
"news />\">miscellaneous news for developers</a> has been released and covers "
"the following topics:"
msgstr ""
"Έχει κυκλοφορήσει το <get-var issue-devel-news /> τεύχος με <a href=\"<get-"
"var url-devel-news />\">διάφορες ειδήσεις για τους προγραμματιστές</a> και "
"περιέχει τα παρακάτω θέματα:"

#: ../../english/template/debian/projectnews/footer.wml:6
msgid ""
"To receive this newsletter in your mailbox, <a href=\"https://lists.debian."
"org/debian-news/\">subscribe to the debian-news mailing list</a>."
msgstr ""
"Για να λαμβάνετε αυτό το ενημερωτικό δελτίο στο ηλεκτρονικό ταχυδρομείο σας "
"<a href=\"https://lists.debian.org/debian-news/\"> εγγραφείτε στη λίστα "
"αλληλογραφίας debian-news</a>."

#: ../../english/template/debian/projectnews/footer.wml:10
#: ../../english/template/debian/weeklynews/footer.wml:10
msgid "<a href=\"../../\">Back issues</a> of this newsletter are available."
msgstr ""
"υπάρχουν διαθέσιμα <a href=\"../../\">Παλαότερα τεύχη</a> αυτού του "
"ενημερωτικού δελτίου."

#. One editor name only
#: ../../english/template/debian/projectnews/footer.wml:15
msgid ""
"<void id=\"singular\" />Debian Project News is edited by <a href=\"mailto:"
"debian-publicity@lists.debian.org\">%s</a>."
msgstr ""
"<void id=\"singular\" />Τα Νέα του Σχεδίου Debian συντάσσονται από τον/την "
"<a href=\"mailto:debian-publicity@lists.debian.org\">%s</a>."

#. Two or more editors
#: ../../english/template/debian/projectnews/footer.wml:20
msgid ""
"<void id=\"plural\" />Debian Project News is edited by <a href=\"mailto:"
"debian-publicity@lists.debian.org\">%s</a>."
msgstr ""
"<void id=\"plural\" />Τα Νέα του Σχεδίου Debian συντάσσονται από τους/τις <a "
"href=\"mailto:debian-publicity@lists.debian.org\">%s</a>."

#. One editor name only
#: ../../english/template/debian/projectnews/footer.wml:25
msgid ""
"<void id=\"singular\" />This issue of Debian Project News was edited by <a "
"href=\"mailto:debian-publicity@lists.debian.org\">%s</a>."
msgstr ""
"<void id=\"singular\" />Το παρόν τεύχος των Νέων του Σχεδίου Debian "
"συντάχθηκε από τον/την <a href=\"mailto:debian-publicity@lists.debian.org\">"
"%s</a>."

#. Two or more editors
#: ../../english/template/debian/projectnews/footer.wml:30
msgid ""
"<void id=\"plural\" />This issue of Debian Project News was edited by <a "
"href=\"mailto:debian-publicity@lists.debian.org\">%s</a>."
msgstr ""
"<void id=\"plural\" /> Το παρόν τεύχος των Νέων του Σχεδίου Debian "
"συντάχθηκε από τους/τις <a href=\"mailto:debian-publicity@lists.debian.org\">"
"%s</a>."

#. One translator only
#. One translator only
#: ../../english/template/debian/projectnews/footer.wml:35
#: ../../english/template/debian/weeklynews/footer.wml:35
msgid "<void id=\"singular\" />It was translated by %s."
msgstr "<void id=\"singular\" />Μεταφράστηκε από τον/την %s."

#. Two ore more translators
#. Two ore more translators
#: ../../english/template/debian/projectnews/footer.wml:40
#: ../../english/template/debian/weeklynews/footer.wml:40
msgid "<void id=\"plural\" />It was translated by %s."
msgstr "<void id=\"plural\" />Μεταφράστηκε από τους/τις %s."

#. One female translator only
#. One female translator only
#: ../../english/template/debian/projectnews/footer.wml:45
#: ../../english/template/debian/weeklynews/footer.wml:45
msgid "<void id=\"singularfemale\" />It was translated by %s."
msgstr "<void id=\"singularfemale\" />Μεταφράστηκε από την %s."

#. Two ore more female translators
#. Two ore more female translators
#: ../../english/template/debian/projectnews/footer.wml:50
#: ../../english/template/debian/weeklynews/footer.wml:50
msgid "<void id=\"pluralfemale\" />It was translated by %s."
msgstr "<void id=\"pluralfemale\" />Μεταφράστηκε από τις %s."

#: ../../english/template/debian/weeklynews/footer.wml:6
msgid ""
"To receive this newsletter weekly in your mailbox, <a href=\"https://lists."
"debian.org/debian-news/\">subscribe to the debian-news mailing list</a>."
msgstr ""
"Για να λαμβάνετε αυτό το ενημερωτικό δελτίο κάθε εβδομάδα στο ηλεκτρονικό "
"σας ταχυδρομείο <a href=\"https://lists.debian.org/debian-news/\">εγγραφείτε "
"στην λίστα αλληλογραφίας debian-news</a>."

#. One editor name only
#: ../../english/template/debian/weeklynews/footer.wml:15
msgid ""
"<void id=\"singular\" />Debian Weekly News is edited by <a href=\"mailto:"
"dwn@debian.org\">%s</a>."
msgstr ""
"<void id=\"singular\" />Τα Εβδομαδιαία Νέα του Debian συντάσσονται από τον/"
"την <a href=\"mailto:dwn@debian.org\">%s</a>."

#. Two or more editors
#: ../../english/template/debian/weeklynews/footer.wml:20
msgid ""
"<void id=\"plural\" />Debian Weekly News is edited by <a href=\"mailto:"
"dwn@debian.org\">%s</a>."
msgstr ""
"<void id=\"plural\" />Τα Εβδομαδιαία Νέα του Debian συντάσσονται απο τους/"
"τις <a href=\"mailto:dwn@debian.org\">%s</a>."

#. One editor name only
#: ../../english/template/debian/weeklynews/footer.wml:25
msgid ""
"<void id=\"singular\" />This issue of Debian Weekly News was edited by <a "
"href=\"mailto:dwn@debian.org\">%s</a>."
msgstr ""
"<void id=\"singular\" />Το παρόν τεύχος των Εβδομαδιαίων Νέων του Debian "
"συντάχθηκε από τον/την <a href=\"mailto:dwn@debian.org\">%s</a>."

#. Two or more editors
#: ../../english/template/debian/weeklynews/footer.wml:30
msgid ""
"<void id=\"plural\" />This issue of Debian Weekly News was edited by <a href="
"\"mailto:dwn@debian.org\">%s</a>."
msgstr ""
"<void id=\"plural\" />Το παρόν τεύχος των Εβδομαδιαίων Νέων του Debian "
"συντάχθηκε από τους/τις <a href=\"mailto:dwn@debian.org\">%s</a>."

#~ msgid "Back to the <a href=\"./\">Debian speakers page</a>."
#~ msgstr "Πίσω στην σελίδα των <a href=\"./\">ομιλητών του Debian</a>."

#~ msgid "Email:"
#~ msgstr "Ηλ. Διεύθυνση:"

#~ msgid "Languages:"
#~ msgstr "Γλώσσες:"

#~ msgid "List of Speakers"
#~ msgstr "Κατάλογος Ομιλητών"

#~ msgid "Location:"
#~ msgstr "Τοποθεσία:"

#~ msgid "Name:"
#~ msgstr "Όνομα:"

#~ msgid "Previous Talks:"
#~ msgstr "Προηγούμενς Ομιλίες:"

#, fuzzy
#~| msgid ""
#~| "To receive this newsletter weekly in your mailbox, <a href=\"https://"
#~| "lists.debian.org/debian-news/\">subscribe to the debian-news mailing "
#~| "list</a>."
#~ msgid ""
#~ "To receive this newsletter bi-weekly in your mailbox, <a href=\"https://"
#~ "lists.debian.org/debian-news/\">subscribe to the debian-news mailing "
#~ "list</a>."
#~ msgstr ""
#~ "Για να λαμβάνετε αυτό το φυλλάδιο καθε εβδομάδα, κάντε <a href=\"https://"
#~ "lists.debian.org/debian-news/\">συνδρομή</a> στον κατάλογο συνδρομητών "
#~ "debian-news."

#~ msgid "Topics:"
#~ msgstr "Θέματα:"
