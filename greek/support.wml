#use wml::debian::template title="Υποστήριξη"
#use wml::debian::toc
#use wml::debian::translation-check translation="76ce7e7f5cfd0e2f3e8931269bdcd1f9518ee67f" maintainer="galaxico"

<define-tag toc-title-formatting endtag="required" whitespace="delete">
<h2>%body</h2>
</define-tag>

Το Debian και η υποστήριξή του γίνονται από μια κοινότητα εθελοντών.

Αν αυτή η προερχόμενη από την κοινότητα υποστήριξη δεν πληροί τις ανάγκες σας, 
μπορείτε να διαβάσετε την <a href="doc/">τεμκηρίωσή μας</a> ή να προσλάβετε 
έναν <a href="consultants/">σύμβουλο</a>.

<toc-display />

<toc-add-entry name="irc">Βοήθεια σε πραγματικό χρόνο στο διαδίκτυο με τη χρήση 
του IRC</toc-add-entry>

<p><a href="http://www.irchelp.org/">Το IRC (Internet Relay Chat)</a> είναι 
ένας τρόπος να μιλά κανείς με ανθρώπους από ολόκληρο τον κόσμο σε πραγματικό 
χρόνο. Τα κανάλια IRC που είναι αφιερωμένα στο Debian μπορούν να βρεθούν στον 
ιστότοπο
<a href="https://www.oftc.net/">OFTC</a>.</p>

<p>Για να συνδεθείτε χρειάζεστε έναν πελάτη IRC. Μερικοί από τους πιο 
δημοφιλείς είναι οι:
<a href="https://packages.debian.org/stable/net/hexchat">HexChat</a>,
<a href="https://packages.debian.org/stable/net/ircii">ircII</a>,
<a href="https://packages.debian.org/stable/net/irssi">irssi</a>,
<a href="https://packages.debian.org/stable/net/epic5">epic5</a> and
<a href="https://packages.debian.org/stable/net/kvirc">KVIrc</a>,
όλοι από τους οποίους υπάρχουν σε μορφή πακέτου για το Debian. Το OFTC 
προσφέρει επίσης και μια διεπαφή web <a 
href="https://www.oftc.net/WebChat/">WebChat</a>
σας επιτρέπει να συνδεθείτε στο IRC μέσω ενός φυλλομετρητή/περιηγή ιστοσελίδων 
χωρίς να χρειάζεστε να εγκαταστήσετε έναν τοπικό πελάτη.</p>

<p>Αφού έχετε εγκαταστήσει τον πελάτη IRC θα πρέπει να του πείτε να 
συνδεθεί στον εξυπηρετητή. Για τους περισσότερους πελάτες. μπορείτε να το 
κάνετε αυτό πληκτρολογώντας:</p>

<pre>
/server irc.debian.org
</pre>

<p>Σε μερικούς πελάτες (όπως ο irssi) πρέπει αντί γι' αυτό να 
πληκτρολογήσετε:</p>

<pre>
/connect irc.debian.org
</pre>

# Note to translators:
# You might want to insert here a paragraph stating which IRC channel is available
# for user support in your language and pointing to the English IRC channel.
# <p>Once you are connected, join channel <code>#debian-foo</code> by typing</p>
# <pre>/join #debian</pre>
# for support in your language.
# <p>For support in English, read on</p>

<p>Αφού συνδεθείτε, μπείτε στο κανάλι <code>#debian</code> πληκτρολογώντας</p>

<pre>
/join #debian
</pre>

<p>Σημείωση: πελάτες όπως ο HexChat έχουν συχνά μια διαφορετική διεπαφή 
γραφικών για την σύνδεση σε εξυπηρετητές ή κανάλια.</p>

<p>Σ' αυτό το σημείο θα βρεθείτε ανάμεσα σε ένα φιλικό πλήθος των θαμώνων του 
<code>#debian</code>. Εδώ είστε ευπρόσδεκτοι να κάνετε ερωτήσεις σχετικά με το
Debian. Μπορείτε να βρείτε τις συχνές ερωτήσεις του faq στο 
<url "https://wiki.debian.org/DebianIRC" />.</p>


<p>Υπάρχει ένας αριθμός άλλων δικτύων IRC στα οποία επίσης μπορείτε να μιλήσετε 
για το Debian.
</p>


<toc-add-entry name="mail_lists" href="MailingLists/">Λίστες 
αλληλογραφίας</toc-add-entry>

<p>Το Debian αναπτύσσεται μέσα από ένα κατανεμημένο μοντέλο σ' ολόκληρο τον 
κόσμο. Συνεπώς, το ηλεκτρονικό ταχυδρομείο προτιμάται για τη συζήτηση διαφόρων 
θεμάτων. Μεγάλο μέρος της συζήτησης μεταξύ των προγραμματιστών και χρηστών του 
Debian γίνεται μέσω αρκετών λιστών αλληλογραφίας.</p>

<p>Υπάρχουν αρκετές δημόσια διαθέσιμες λίστες αλληλογραφίας. Για περισσότερες 
πληροφορίες δείτε τη σελίδα <a href="MailingLists/">Debian mailing 
lists</a>.</p>

# Note to translators:
# You might want to adapt the following paragraph, stating which list
# is available for user support in your language instead of English.
<p>
Για υποστήριξή στα Αγγλικά, παρακαλούμε επικοινωνήστε με την λίστα
<a href="https://lists.debian.org/debian-user/">debian-user
</a>.</p>

<p>
Για υποστήριξη σε άλλες γλώσσες, παρακαλούμε ελέγξτε 
<a href="https://lists.debian.org/users.html">τον κατάλογο με τις λίστες 
χρηστών.</a>.
</p>

<p>Υπάρχουν φυσικά πολλές άλλες λίστες αλληλογραφίας αφιερωμένες σε κάποιες από 
τις πτυχές του τεράστιου οικοσυστήματος του Linux που δεν αφορούν συγκεκριμένα 
το Debian. Χρησιμοποιείστε τη μηχανή αναζήτησης της προτίμησής σας για να 
βρείτε την πιο κατάλληλη λίστα για τον σκοπό σας.</p>


<toc-add-entry name="usenet">Ομάδες ειδήσεων στο Usenet</toc-add-entry>

<p>Πολλές από τις <a href="#mail_lists">λίστες αλληλογραφίας μας</a> μπορείτε 
να τις περιηγηθείτε ως ομάδες ειδήσεων (newsgroups), στην ιεραρχία
<kbd>linux.debian.*</kbd>. Αυτό μπορεί επίσης να γίνει με τη χρήση μιας 
διεπαφής web όπως η <a href="https://groups.google.com/forum/">Google Groups</a>.


<toc-add-entry name="web">Ιστότοποι</toc-add-entry>

<h3 id="forums">Φόρουμ</h3>

# Note to translators:
# If there is a specific Debian forum for your language you might want to
# insert here a paragraph stating which list is available for user support
# in your language and pointing to the English forums.
# <p><a href="http://someforum.example.org/">someforum</a> is a web portal
# on which you can use your language to discuss Debian-related topics,
# submit questions about Debian, and have them answered by other users.</p>
#
# <p><a href="https://forums.debian.net">Debian User Forums</a> is a web portal
# on which you can use the English language to discuss Debian-related topics,
# submit questions about Debian, and have them answered by other users.</p>

<p>Το <a href="https://forums.debian.net">Debian User Forums</a> είναι μια 
δικτυακή πύλη στην οποία μπορείτε να συζητήσετε ζητήματα σχετικά με το 
Debian, να υποβάλλετε ερωτήσεις σχετικά με το Debian και να πάρετε απαντήσεις 
από άλλους χρήστες.</p>


<toc-add-entry name="maintainers">Απευθυνόμενοι στους Συντηρητές των 
Πακέτων</toc-add-entry>

<p>Υπάρχουν δύο τρόποι για να απευθυνθείτε στους συντηρητές των πακέτων. Αν 
χρειάζεστε να επικοινωνήσετε με τον συντηρητή ενός πακέτου εξαιτίας ενός 
σφάλματος, απλά υποβάλετε μια αναφορά σφάλματος (δείτε την ενότητα για το  the
Bug Tracking System στη συνέχεια). Ο συντηρητής θα λάβει ένα αντίγραφο της 
αναφοράς.</p>

<p>Αν θέλετε απλά να επικοινωνήσετε με τον συντηρητή ενός πακέτου, τότε 
μπορείτε να χρησιμοποιήσετε το ειδικό ψευδώνυμο αλληλογραφίας που έχει οριστεί 
για κάθε πακέτο. Οποιοδήποτε μήνυμα στέλνεται στη διεύθυνση
&lt;<em>package name</em>&gt;@packages.debian.org θα προωθηθεί στον συντηρητή 
που είναι υπεύθυνος γι' αυτό το πακέτο.</p>


<toc-add-entry name="bts" href="Bugs/">Tο Σύστημα Παρακολούθησης Σφαλμάτων (Bug 
Tracking System)</toc-add-entry>

<p>Η διανομή του Debian διαθέτει ένα σύστημα παρακολούθησης των σφαλμάτων που 
παρέχει λεπτομέρειες σχετικά με τα σφάλματα από χρήστες και προγραμματιστές. 
Σε κάθε σφάλμα αποδίδεται ένας αριθμός και κρατείται στο αρχείο μέχρι να 
σημανθεί ότι έχει αντιμετωπιστεί.</p>

<p>Για να αναφέρετε ένα σφάλμα, μπορείτε να χρησιμοποιήσετε μια από τις σελίδες 
σφαλμάτων που αναφέρονται στη λίστα παρακάτω ή να χρησιμοποιήσετε το πακέτο του 
Debian <q>reportbug</q> για την αυτόματη υποβολή μιας αναφοράς σφάλματος.</p>

<p>Πληροφορίες σχετικά με την υποβολή σφαλμάτων, την θεώρηση των ενεργών αυτή 
τη στιγμή σφαλμάτων και για το σύστημα παρακολούθησης σφαλμάτων γενικά, μπορούν 
να βρεθούν στις ιστοσελίδες
<a href="Bugs/">Σύστημα Παρακολούθησης Σφαλμάτων</a>.</p>


<toc-add-entry name="consultants" href="consultants/">Σύμβουλοι</toc-add-entry>

<p>Το Debian είναι ελεύθερο λογισμικό και προσφέρει δωρεάν βοήθεια μέσα από τις 
λίστες αλληλογραφίας. Κάποιος κόσμος δεν έχει τον χρόνο ή τις εξειδικευμένες 
ανάγκες και είναι διατεθιμένος να προσλάβει κάποιον για τη  συντήρηση ή την 
προσθήκη επιπλέον λειτουργικότητας στο Debian σύστημά τους. Δείτε τη σελίδα <a 
href="consultants/">Σύμβουλοι</a> για μια λίστα ατόμων/εταιρειών.</p>


<toc-add-entry name="release" href="releases/stable/">Γνωστά 
προβλήματα</toc-add-entry>

<p>Περιορισμο και σοβαρά προβλήματα της τρέχουσας σταθερής διανομής (αν 
υπάρχουν) περιγράφονται στις σελίδες της <a 
href="releases/stable/">Έκδοσης</a>.</p>

<p>Δώστε ιδιαίτερη προσοχή στις <a 
href="releases/stable/releasenotes">Σημειώσεις της έκδοσης</a> και τα <a 
href="releases/stable/errata">Παροράματα</a>.</p>
