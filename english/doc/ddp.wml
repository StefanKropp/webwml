#use wml::debian::template title="Debian Documentation Project (DDP)" MAINPAGE="true"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<ul class="toc">
  <li><a href="#ddp_work">Our Work</a></li>
</ul>


<aside>
<p><span class="fas fa-caret-right fa-3x"></span> The Debian Documentation Project (DDP) takes care of Debian's documentation, for example the users' and developers' manuals, various handbooks, FAQs, and release notes. This page offers a quick overview of the DDP's work.</p>
</aside>

<h2><a id="ddp_work">Our Work</h2>

<p>
<div class="line">
  <div class="item col50">

    <h3>Manuals</h3>
    <ul>
      <li><strong><a href="user-manuals">Users' manuals</a></strong></li>
      <li><strong><a href="devel-manuals">Developers' manuals</a></strong></li>
      <li><strong><a href="misc-manuals">Miscellaneous manuals</a></strong></li>
      <li><strong><a href="obsolete">Problematic manuals</a></strong></li>
    </ul>

  </div>

  <div class="item col50 lastcol">

    <h3>Documentation Policy</h3>
    <ul>
      <li>Manual licenses comply with DFSG.</li>
      <li>We use Docbook XML for our documents.</li>
      <li>All sources are availble in our <a href="https://salsa.debian.org/ddp-team">Git reposity</a>.</li>
      <li><tt>www.debian.org/doc/&lt;manual-name&gt;</tt> will be the official URL.</li>
      <li>Every document should be actively maintained.</li>
    </ul>

    <h3>Git Access</h3>
    <ul>
      <li>How to access the <a href="vcs">DDP's Git repository</a></li>
    </ul>

  </div>


</div>
</p>

<p style="text-align:center"><button type="button"><span class="fas fa-envelope fa-2x"></span> Get in touch with the team on <a href="https://lists.debian.org/debian-doc/">debian-doc</a></button></p>
