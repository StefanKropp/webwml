<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were discovered in spice, a SPICE protocol
client and server library. The Common Vulnerabilities and Exposures
project identifies the following problems:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9577">CVE-2016-9577</a>

    <p>Frediano Ziglio of Red Hat discovered a buffer overflow
    vulnerability in the main_channel_alloc_msg_rcv_buf function. An
    authenticated attacker can take advantage of this flaw to cause a
    denial of service (spice server crash), or possibly, execute
    arbitrary code.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9578">CVE-2016-9578</a>

    <p>Frediano Ziglio of Red Hat discovered that spice does not properly
    validate incoming messages. An attacker able to connect to the
    spice server could send crafted messages which would cause the
    process to crash.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
0.11.0-1+deb7u4.</p>

<p>We recommend that you upgrade your spice packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-825.data"
# $Id: $
