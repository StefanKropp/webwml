<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several issues have been discovered in the MySQL database server. The
vulnerabilities are addressed by upgrading MySQL to the new upstream
version 5.5.54, which includes additional changes, such as performance
improvements, bug fixes, new features, and possibly incompatible
changes. Please see the MySQL 5.5 Release Notes and Oracle's Critical
Patch Update advisory for further details:</p>

<ul>
<li><url "https://dev.mysql.com/doc/relnotes/mysql/5.5/en/news-5-5-54.html"></li>
<li><a href="http://www.oracle.com/technetwork/security-advisory/cpujan2017-2881727.html">http://www.oracle.com/technetwork/security-advisory/cpujan2017-2881727.html</a></li>
</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
5.5.54-0+deb7u1.</p>

<p>We recommend that you upgrade your mysql-5.5 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-797.data"
# $Id: $
