<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>In MIT Kerberos 5 (aka krb5) 1.7 and later, an authenticated attacker
can cause a KDC assertion failure by sending invalid S4U2Self or
S4U2Proxy requests.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1.10.1+dfsg-5+deb7u8.</p>

<p>We recommend that you upgrade your krb5 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1058.data"
# $Id: $
