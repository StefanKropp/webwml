<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were found in QEMU, a fast processor emulator:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-17958">CVE-2018-17958</a>

    <p>The rtl8139 emulator is affected by an integer overflow and subsequent
    buffer overflow. This vulnerability might be triggered by remote
    attackers with crafted packets to perform denial of service (via OOB
    stack buffer access).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-19364">CVE-2018-19364</a>

    <p>The 9pfs subsystem is affected by a race condition allowing threads to
    modify an fid path while it is being accessed by another thread,
    leading to (for example) a use-after-free outcome. This vulnerability
    might be triggered by local attackers to perform denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-19489">CVE-2018-19489</a>

    <p>The 9pfs subsystem is affected by a race condition during file
    renaming. This vulnerability might be triggered by local attackers to
    perform denial of service.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
1:2.1+dfsg-12+deb8u9.</p>

<p>We recommend that you upgrade your qemu packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1646.data"
# $Id: $
