<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Ross Geerlings discovered that the XMLTooling library didn't correctly
handle exceptions on malformed XML declarations, which could result in
denial of service against the application using XMLTooling.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
1.5.3-2+deb8u4.</p>

<p>We recommend that you upgrade your xmltooling packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1710.data"
# $Id: $
