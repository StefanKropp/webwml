<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>In the nfs-utils package, providing support files for Network File
System (NFS) including the rpc.statd daemon, the directory
/var/lib/nfs is owned by statd:nogroup.  This directory contains files
owned and managed by root.  If statd is compromised, it can therefore
trick processes running with root privileges into creating/overwriting
files anywhere on the system.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
1.2.8-9+deb8u1.</p>

<p>We recommend that you upgrade your nfs-utils packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1965.data"
# $Id: $
