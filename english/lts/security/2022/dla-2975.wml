<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple vulnerabilities have been discovered in openjpeg2, the open-source
JPEG 2000 codec.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27842">CVE-2020-27842</a>

    <p>Null pointer dereference through specially crafted input. The highest impact
    of this flaw is to application availability.</p>
</li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27843">CVE-2020-27843</a>

    <p>The flaw allows an attacker to provide specially crafted input to the
    conversion or encoding functionality, causing an out-of-bounds read. The
    highest threat from this vulnerability is system availability.</p>
</li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-29338">CVE-2021-29338</a>

    <p>Integer overflow allows remote attackers to crash the application, causing a
    denial of service. This occurs when the attacker uses the command line
    option "-ImgDir" on a directory that contains 1048576 files.</p>
</li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1122">CVE-2022-1122</a>

    <p>Input directory with a large number of files can lead to a segmentation
    fault and a denial of service due to a call of free() on an uninitialized
    pointer.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
2.1.2-1.1+deb9u7.</p>

<p>We recommend that you upgrade your openjpeg2 packages.</p>

<p>For the detailed security status of openjpeg2 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/openjpeg2">https://security-tracker.debian.org/tracker/openjpeg2</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2975.data"
# $Id: $
