<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several issues have been found in tiff, a library and tools to manipulate
and convert files in the Tag Image File Format (TIFF).</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-22844">CVE-2022-22844</a>

     <p>out-of-bounds read in _TIFFmemcpy in certain situations involving a
     custom tag and 0x0200 as the second word of the DE field.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0562">CVE-2022-0562</a>

     <p>Null source pointer passed as an argument to memcpy() function within
     TIFFReadDirectory(). This could result in a Denial of Service via
     crafted TIFF files.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0561">CVE-2022-0561</a>

     <p>Null source pointer passed as an argument to memcpy() function within
     TIFFFetchStripThing(). This could result in a Denial of Service via
     crafted TIFF files.</p>


<p>For Debian 9 stretch, these problems have been fixed in version
4.0.8-2+deb9u8.</p>

<p>We recommend that you upgrade your tiff packages.</p>

<p>For the detailed security status of tiff please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/tiff">https://security-tracker.debian.org/tracker/tiff</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p></li>

</ul>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2932.data"
# $Id: $
