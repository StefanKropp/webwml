<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that zsh, a powerful shell and scripting language,
did not prevent recursive prompt expansion. This would allow an
attacker to execute arbitrary commands into a user's shell, for
instance by tricking a vcs_info user into checking out a git branch
with a specially crafted name.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
5.3.1-4+deb9u5.</p>

<p>We recommend that you upgrade your zsh packages.</p>

<p>For the detailed security status of zsh please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/zsh">https://security-tracker.debian.org/tracker/zsh</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2926.data"
# $Id: $
