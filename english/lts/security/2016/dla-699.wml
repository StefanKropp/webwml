<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Xen does not properly honor CR0.TS and CR0.EM, which allows local x86
HVM guest OS users to read or modify FPU, MMX, or XMM register state
information belonging to arbitrary tasks on the guest by modifying an
instruction while the hypervisor is preparing to emulate it.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
4.1.6.lts1-3.</p>

<p>We recommend that you upgrade your xen packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-699.data"
# $Id: $
