<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A use-after-free was found in the Mozilla Firefox web browser, which
could potentially result in the execution of arbitrary code.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
78.4.1esr-1~deb9u1.</p>

<p>We recommend that you upgrade your firefox-esr packages.</p>

<p>For the detailed security status of firefox-esr please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/firefox-esr">https://security-tracker.debian.org/tracker/firefox-esr</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2448.data"
# $Id: $
