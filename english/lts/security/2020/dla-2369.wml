<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several security vulnerabilities were corrected in libxml2, the GNOME
XML library.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-8872">CVE-2017-8872</a>

    <p>Global buffer-overflow in the htmlParseTryOrFinish function.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-18258">CVE-2017-18258</a>

    <p>The xz_head function in libxml2 allows remote attackers to cause a
    denial of service (memory consumption) via a crafted LZMA file,
    because the decoder functionality does not restrict memory usage to
    what is required for a legitimate file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-14404">CVE-2018-14404</a>

    <p>A NULL pointer dereference vulnerability exists in the
    xpath.c:xmlXPathCompOpEval() function of libxml2 when parsing an
    invalid XPath expression in the XPATH_OP_AND or XPATH_OP_OR case.
    Applications processing untrusted XSL format inputs may be
    vulnerable to a denial of service attack.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-14567">CVE-2018-14567</a>

    <p>If the option --with-lzma is used, allows remote attackers to cause
    a denial of service (infinite loop) via a crafted XML file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19956">CVE-2019-19956</a>

    <p>The xmlParseBalancedChunkMemoryRecover function has a memory leak
    related to newDoc-&gt;oldNs.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-20388">CVE-2019-20388</a>

    <p>A memory leak was found in the xmlSchemaValidateStream function of
    libxml2. Applications that use this library may be vulnerable to
    memory not being freed leading to a denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-7595">CVE-2020-7595</a>

    <p>Infinite loop in xmlStringLenDecodeEntities can cause a denial of
    service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-24977">CVE-2020-24977</a>

    <p>Out-of-bounds read restricted to xmllint --htmlout.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
2.9.4+dfsg1-2.2+deb9u3.</p>

<p>We recommend that you upgrade your libxml2 packages.</p>

<p>For the detailed security status of libxml2 please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/libxml2">https://security-tracker.debian.org/tracker/libxml2</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2369.data"
# $Id: $
