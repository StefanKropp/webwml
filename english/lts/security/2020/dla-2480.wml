<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were discovered in salt.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-16846">CVE-2020-16846</a>

    <p>An unauthenticated user with network access to the Salt API can use
    shell injections to run code on the Salt-API using the SSH client</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-17490">CVE-2020-17490</a>

    <p>When using the functions create_ca, create_csr, and
    create_self_signed_cert in the tls execution module, it would not
    ensure the key was created with the correct permissions.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25592">CVE-2020-25592</a>

    <p>Properly validate eauth credentials and tokens along with their Access
    Control Lists – ACLs. Prior to this change, eauth was not properly
    validated when calling Salt SSH via the salt-api. Any value for “eauth”
    or “token” would allow a user to bypass authentication and make calls
    to Salt SSH</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
2016.11.2+ds-1+deb9u6.</p>

<p>We recommend that you upgrade your salt packages.</p>

<p>For the detailed security status of salt please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/salt">https://security-tracker.debian.org/tracker/salt</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2480.data"
# $Id: $
