<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Apache HTTP Server versions before 2.4.32 uses src:uwsgi where a flaw
was discovered. The uwsgi protocol does not let us serialize more
than 16K of HTTP header leading to resource exhaustion and denial of
service.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
2.0.14+20161117-3+deb9u3.</p>

<p>We recommend that you upgrade your uwsgi packages.</p>

<p>For the detailed security status of uwsgi please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/uwsgi">https://security-tracker.debian.org/tracker/uwsgi</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2362.data"
# $Id: $
