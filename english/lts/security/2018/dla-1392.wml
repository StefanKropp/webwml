<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been discovered in the Linux kernel that
may lead to a denial of service.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1093">CVE-2018-1093</a>

    <p>Wen Xu reported that a crafted ext4 filesystem image could trigger
    an out-of-bounds read in the ext4_valid_block_bitmap() function. A
    local user able to mount arbitrary filesystems could use this for
    denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1130">CVE-2018-1130</a>

    <p>The syzbot software found that the DCCP implementation of
    sendmsg() does not check the socket state, potentially leading
    to a null pointer dereference.  A local user could use this to
    cause a denial of service (crash).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-8897">CVE-2018-8897</a>

    <p>Nick Peterson of Everdox Tech LLC discovered that #DB exceptions
    that are deferred by MOV SS or POP SS are not properly handled,
    allowing an unprivileged user to crash the kernel and cause a
    denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-10940">CVE-2018-10940</a>

    <p>Dan Carpenter reported that the cdrom driver does not correctly
    validate the parameter to the CDROM_MEDIA_CHANGED ioctl.  A user
    with access to a cdrom device could use this to cause a denial of
    service (crash).</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
3.2.102-1.  This version also includes bug fixes from upstream version
3.2.102, including a fix for a regression in the SCTP implementation
in version 3.2.101.</p>

<p>We recommend that you upgrade your linux packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1392.data"
# $Id: $
