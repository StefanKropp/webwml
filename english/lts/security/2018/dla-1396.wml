<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that there were a number of vulnerabilities in redis,
a persistent key-value database:</p>

<ul>

  <li><a href="https://security-tracker.debian.org/tracker/CVE-2018-11218">CVE-2018-11218</a> /
    <a href="https://security-tracker.debian.org/tracker/CVE-2018-11219">CVE-2018-11219</a>
    <p>Multiple heap
    corruption and integer overflow vulnerabilities. (#901495)</p></li>

  <li><a href="https://security-tracker.debian.org/tracker/CVE-2018-12326">CVE-2018-12326</a>
    <p>Buffer overflow in the "redis-cli" tool which could
    have allowed an attacker to achieve code execution and/or escalate to
    higher privileges via a crafted command line. (#902410)</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these issues have been fixed in redis version
2:2.8.17-1+deb8u6.</p>

<p>We recommend that you upgrade your redis packages.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1396.data"
# $Id: $
