<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Reportbug, a tool designed to make the reporting of bugs in Debian
easier, was further enhanced to automatically detect bug reports for
potential regressions caused by a security update. After user
confirmation an additional email with a copy of the report will be
sent to the debian-lts mailing list.</p>

<p>This change requires two new dependencies, python-requests and python-apt.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
6.6.3+deb8u2.</p>

<p>We recommend that you upgrade your reportbug packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1511.data"
# $Id: $
