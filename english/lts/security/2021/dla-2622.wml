<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was a potential directory traversal issue in
Django, a Python-based web development framework.</p>

<p>The vulnerability could have been exploited by maliciously crafted
filenames. However, the upload handlers built into Django itself were not
affected.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-28658">CVE-2021-28658</a>

    <p>In Django 2.2 before 2.2.20, 3.0 before 3.0.14, and 3.1 before 3.1.8,
    MultiPartParser allowed directory traversal via uploaded files with
    suitably crafted file names. Built-in upload handlers were not affected by
    this vulnerability.</p></li>

</ul>

<p>For Debian 9 <q>Stretch</q>, these problems have been fixed in version
1:1.10.7-2+deb9u12.</p>

<p>We recommend that you upgrade your python-django packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2622.data"
# $Id: $
