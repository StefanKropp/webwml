#use wml::debian::template title="How to contact us" NOCOMMENTS="yes" MAINPAGE="true"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<ul class="toc">
  <li><a href="#generalinfo">General Information</a>
  <li><a href="#installuse">Installing and using Debian</a>
  <li><a href="#press">Publicity &amp; Press</a>
  <li><a href="#events">Events &amp; Conferences</a>
  <li><a href="#helping">Helping Debian</a>
  <li><a href="#packageproblems">Reporting Problems in Debian Packages</a>
  <li><a href="#development">Debian Development</a>
  <li><a href="#infrastructure">Problems with the Debian Infrastructure</a>
  <li><a href="#harassment">Harassment Issues</a>
</ul>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> We kindly ask you to send initial enquiries in <strong>English</strong>, as this is the language most of us speak. If this is not possible, please ask for help on one of our <a href="https://lists.debian.org/users.html#debian-user">user mailing lists</a>, which are available in many different languages.</p>
</aside>

<p>
Debian is a large organization and there are several ways to get in touch with project members and other Debian users. This page summarizes often requested means of contact; it is by no means exhaustive. Please refer to the rest of the web pages for other contact methods.
</p>

<p>
Please note that most of the e-mail addresses below represent open mailing lists with public archives. Read the <a href="$(HOME)/MailingLists/disclaimer">disclaimer</a> before sending any messages.
</p>

<h2 id="generalinfo">General Information</h2>

<p>
Most information about Debian is collected on our website <a href="$(HOME)">https://www.debian.org/</a>, so please browse and <a href="$(SEARCH)">search</a> through it before contacting us.
</p>

<p>
You can send questions regarding the Debian project to the <email debian-project@lists.debian.org> mailing list. Please don't ask general questions about Linux on this list; read on for more information about other mailing lists.
</p>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="doc/user-manuals#faq">Read our FAQ</a></button></p>

<aside class="light">
  <span class="fa fa-envelope fa-5x"></span>
</aside>
<h2 id="installuse">Installing and using Debian</h2>

<p>
If you are certain the documentation on the installation media and on our
website doesn't have a solution to your problem, there's a very active user
mailing list where Debian users and developers can answer your questions.
Here, you can ask all kinds of questions regarding the following topics:
</p>

<ul>
  <li>Installation
  <li>Configuration
  <li>Supported hardware
  <li>Machine administration
  <li>Using Debian
</ul>

<p>
Please <a href="https://lists.debian.org/debian-user/">subscribe</a> to
the list and send your questions to <email debian-user@lists.debian.org>.
</p>

<p>
Additionally, there are user mailing lists for speakers of various languages. See the subscription info for <a href="https://lists.debian.org/users.html#debian-user">international mailing lists</a>.
</p>

<p>
On top of that, it's possible to browse our mailing lists as newsgroups, for example with <a href="http://groups.google.com/d/homeredir">Google's web interface</a>.
</p>

<p>
If you think you've found a bug in our installation system, please write to the <email debian-boot@lists.debian.org> mailing list. You can also file a <a href="$(HOME)/releases/stable/i386/ch05s04#submit-bug">bug report</a> against the <a href="https://bugs.debian.org/debian-installer">debian-installer</a> pseudo-package.
</p>

<aside class="light">
  <span class="far fa-newspaper fa-5x"></span>
</aside>
<h2 id="press">Publicity &amp; Press</h2>

<p>
The <a href="https://wiki.debian.org/Teams/Publicity">Debian Publicity Team</a> moderates news and announcements in all the Debian official resources, for example some mailing lists, the blog, the micronews website, and official social media channels.
</p>

<p>
If you're writing about Debian and need help or information, please contact the <a href="mailto:press@debian.org">publicity team</a>. This is also the right address if you're planning to submit news for our own news page.
</p>

<aside class="light">
  <span class="fas fa-handshake fa-5x"></span>
</aside>
<h2 id="events">Events &amp; Conferences</h2>

<p>
Please send invitations for <a href="$(HOME)/events/">conferences</a>, exhibitions, or other events to <email events@debian.org>.
</p>

<p>
Requests for flyers, posters, and participation in Europe should be sent to the <email debian-events-eu@lists.debian.org> mailing list.
</p>

<aside class="light">
  <span class="fas fa-hands-helping fa-5x"></span>
</aside>
<h2 id="helping">Helping Debian</h2>

<p>
There are plenty of possibilities to support the Debian project and contribute to the distribution. If you would like to offer help, please visit our <a href="devel/join/">How to join Debian</a> page first.
</p>

<p>If you would like to maintain a Debian mirror, see the pages about
<a href="mirror/">mirroring Debian</a>. New mirrors are submitted using
<a href="mirror/submit">this form</a>. Problems with existing mirrors
can be reported at <email mirrors@debian.org>.
</p>

<p>If you would like to sell Debian CDs/DVDs, see the <a href="CD/vendors/info">\
information for CD vendors</a>. To get listed on the CD vendor list, please
use <a href="CD/vendors/adding-form">this form</a>.
</p>

<aside class="light">
  <span class="fas fa-bug fa-5x"></span>
</aside>
<h2 id="packageproblems">Reporting problems in Debian packages</h2>

<p>If you would like to file a bug against a Debian package, we have a bug
tracking system where you can easily report your problem. Please read the
<a href="Bugs/Reporting">instructions for filing bug reports</a>.

<p>If you simply want to communicate with the maintainer of a Debian
package, then you can use the special mail aliases set up for each package.
Any mail sent to &lt;<var>package name</var>&gt;@packages.debian.org will be
forwarded to the maintainer in charge of that package.
</p>

<p>If you would like to make the developers aware of a Debian security
problem in a discreet manner, please send an email to
<email security@debian.org>.
</p>

<aside class="light">
  <span class="fas fa-code-branch fa-5x"></span>
</aside>
<h2 id="development">Debian Development</h2>

<p>If you have questions about the development of Debian, we have
several <a href="https://lists.debian.org/devel.html">mailing lists</a>
for that. Here you can get in touch with our developers.
</p>

<p>
There is also a general mailing list for developers: <email debian-devel@lists.debian.org>.
Please follow this <a href="https://lists.debian.org/debian-devel/">link</a> to subscribe to it.
</p>

<aside class="light">
  <span class="fas fa-network-wired fa-5x"></span>
</aside>
<h2 id="infrastructure">Problems with the Debian Infrastructure</h2>

<p>To report a problem with a Debian service, you can usually
<a href="Bugs/Reporting">report a bug</a> against the appropriate
<a href="Bugs/pseudo-packages">pseudo-package</a>.

<p>Alternatively, you can send an email to one of the following addresses:</p>

<define-tag btsurl>package: <a href="https://bugs.debian.org/%0">%0</a></define-tag>

<dl>
<dt>Web pages editors</dt>
  <dd><btsurl www.debian.org><br />
      <email debian-www@lists.debian.org></dd>
#include "$(ENGLISHDIR)/devel/website/tc.data"
<ifneq "$(CUR_LANG)" "English" "
<dt>Web pages translators</dt>
  <dd><: &list_translators($CUR_LANG); :></dd>
">
<dt>Mailing list administrators and archives maintainers</dt>
  <dd><btsurl lists.debian.org><br />
      <email listmaster@lists.debian.org></dd>
<dt>Bug tracking system administrators</dt>
  <dd><btsurl bugs.debian.org><br /> 
      <email owner@bugs.debian.org></dd>
</dl>

<aside class="light">
  <span class="fas fa-umbrella fa-5x"></span>
</aside>
<h2 id="harassment">Harassment Issues</h2>

<p>Debian is a community of people who values respect. If you are the victim of inappropriate behaviour, if someone in the project has harmed you, or if you feel you have been harassed, please contact the Community Team: <email community@debian.org>
</p>
