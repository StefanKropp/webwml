<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were discovered in BIND, a DNS server
implementation.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-6477">CVE-2019-6477</a>

    <p>It was discovered that TCP-pipelined queries can bypass tcp-client
    limits resulting in denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8616">CVE-2020-8616</a>

    <p>It was discovered that BIND does not sufficiently limit the number
    of fetches performed when processing referrals. An attacker can take
    advantage of this flaw to cause a denial of service (performance
    degradation) or use the recursing server in a reflection attack with
    a high amplification factor.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8617">CVE-2020-8617</a>

    <p>It was discovered that a logic error in the code which checks TSIG
    validity can be used to trigger an assertion failure, resulting in
    denial of service.</p></li>

</ul>

<p>For the oldstable distribution (stretch), these problems have been fixed
in version 1:9.10.3.dfsg.P4-12.3+deb9u6.</p>

<p>For the stable distribution (buster), these problems have been fixed in
version 1:9.11.5.P4+dfsg-5.1+deb10u1.</p>

<p>We recommend that you upgrade your bind9 packages.</p>

<p>For the detailed security status of bind9 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/bind9">\
https://security-tracker.debian.org/tracker/bind9</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4689.data"
# $Id: $
