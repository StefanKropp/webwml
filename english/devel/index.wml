#use wml::debian::template title="Debian Developers' Corner" MAINPAGE="true"
#use wml::debian::recent_list

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Though all information on this page and all links to other pages are publicly available, this site is primarily aimed at Debian developers.</p>
</aside>

<ul class="toc">
<li><a href="#basic">Basic</a></li>
<li><a href="#packaging">Packaging</a></li>
<li><a href="#workinprogress">Work in Progress</a></li>
<li><a href="#projects">Projects</a></li>
<li><a href="#miscellaneous">Miscellaneous</a></li>
</ul>

<div class="row">

  <!-- left column -->
  <div class="column column-left" id="basic">
    <div style="text-align: center">
      <span class="fa fa-users fa-3x" style="float:left;margin-right:5px;margin-bottom:5px;"></span>
      <h2><a id="basic">General Information</a></h2>
      <p>A list of current developers and maintainers, how to join the project, and links to the developers' database, the constitution, the voting process, releases, and architectures.</p>
    </div>

    <div style="text-align: left">
      <dl>
        <dt><a href="$(HOME)/intro/organization">Debian Organization</a></dt>
        <dd>Over one thousand volunteers are part of the Debian project. This page explains Debian's organizational structure, lists teams and their members as well as contact addresses.</dd>
        <dt><a href="$(HOME)/intro/people">People behind Debian</a></dt>
        <dd><a href="https://wiki.debian.org/DebianDeveloper">Debian Developers (DD)</a> (full members of the Debian project) and <a href="https://wiki.debian.org/DebianMaintainer">Debian Maintainers (DM)</a>, contribute to the project. Please have a look at the <a href="https://nm.debian.org/public/people/dd_all/">list of Debian Developers</a> and the <a href="https://nm.debian.org/public/people/dm_all/">list of Debian Maintainers</a> to find out more about the people involved, including the packages they maintain. We also have a <a href="developers.loc">world map of Debian developers</a> and a <a href="https://gallery.debconf.org/">gallery</a> with images from various Debian events.</dd>
        <dt><a href="join/">How to join Debian</a></dt>
        <dd>Would you like to contribute and join the project? We're always looking for new developers or free software enthusiasts with technical skills. For more information, please visit this page.</dd>
        <dt><a href="https://db.debian.org/">Developer Database</a></dt>
        <dd>Some information in this database is accessible to everybody, some information only to developers who have logged in. The database contains information such as <a href="https://db.debian.org/machines.cgi">project machines</a> and <a href="extract_key">developers' GnuPG keys</a>. Developers with an account can <a href="https://db.debian.org/password.html">change their password</a> and learn how to set up <a href="https://db.debian.org/forward.html">mail forwarding</a> for their Debian account. If you're planning to use one of the Debian machines, please make sure to read the <a href="dmup">Debian Machine Usage Policies</a>.</dd>
        <dt><a href="constitution">The Constitution</a></dt>
        <dd>This document describes the organizational structure for formal decision-making in the project.
        </dd>
        <dt><a href="$(HOME)/vote/">Voting Information</a></dt>
        <dd>How we elect our leaders, choose our logos and how we vote in general.</dd>
        <dt><a href="$(HOME)/releases/">Releases</a></dt>
        <dd>This page lists current releases (<a href="$(HOME)/releases/stable/">stable</a>, <a href="$(HOME)/releases/testing/">testing</a>, and <a href="$(HOME)/releases/unstable/">unstable</a>) and contains an index of old releases and their codenames.</dd>
        <dt><a href="$(HOME)/ports/">Different Architectures</a></dt>
        <dd>Debian runs on many different architectures. This page collects information about various Debian ports, some based on the Linux kernel, others based on the FreeBSD, NetBSD and Hurd kernels.</dd>

     </dl>
    </div>

  </div>

  <!-- right column -->
  <div class="column column-right" id="software">
    <div style="text-align: center">
      <span class="fa fa-code fa-3x" style="float:left;margin-right:5px;margin-bottom:5px;"></span>
      <h2><a id="packaging">Packaging</a></h2>
      <p>Links to our policy manual and other documents related to the Debian policy, procedures and other resources for Debian developers, and the new maintainers' guide.</p>
    </div>

    <div style="text-align: left">
      <dl>
        <dt><a href="$(DOC)/debian-policy/">Debian Policy Manual</a></dt>
        <dd>This manual describes the policy requirements for the Debian distribution. This includes the structure and contents of the Debian archive, several design issues of the operating system as well as technical requirements which each package must satisfy to be included in the distribution.

        <p>In short, you <strong>need</strong> to read it.</p>
        </dd>
      </dl>

      <p>There are several other documents related to the policy that you might be
      interested in:</p>

      <ul>
        <li><a href="https://wiki.linuxfoundation.org/lsb/fhs/">Filesystem Hierarchy Standard</a> (FHS)
        <br />The FHS defines the directory structure
        and directory contents (location of files);
        compliance with version 3.0 is mandatory (see <a
        href="https://www.debian.org/doc/debian-policy/ch-opersys.html#file-system-hierarchy">chapter
        9</a> of the Debian Policy Manual).</li>
        <li>List of <a href="$(DOC)/packaging-manuals/build-essential">build-essential packages</a>
        <br />You are expected to have these packages if you want to
        compile software, build a package or a set of packages. You don't
        have to include them in <code>Build-Depends</code> line when <a
        href="https://www.debian.org/doc/debian-policy/ch-relationships.html">declaring
        relationships</a> between packages.</li>
        <li><a href="$(DOC)/packaging-manuals/menu-policy/">Menu system</a>
        <br />Debian's structure of menu entries; please check the
        <a href="$(DOC)/packaging-manuals/menu.html/">menu system</a>
        documentation as well.</li>
        <li><a href="$(DOC)/packaging-manuals/debian-emacs-policy">Emacs Policy</a></li>
        <li><a href="$(DOC)/packaging-manuals/java-policy/">Java Policy</a></li>
        <li><a href="$(DOC)/packaging-manuals/perl-policy/">Perl Policy</a></li>
        <li><a href="$(DOC)/packaging-manuals/python-policy/">Python Policy</a></li>
        <li><a href="$(DOC)/packaging-manuals/debconf_specification.html">Debconf Specification</a></li>
	<li><a href="https://www.debian.org/doc/manuals/dbapp-policy/">Database Application Policy</a> (draft)</li>
        <li><a href="https://tcltk-team.pages.debian.net/policy-html/tcltk-policy.html/">Tcl/Tk Policy</a> (draft)</li>
        <li><a href="https://people.debian.org/~lbrenta/debian-ada-policy.html">Debian Policy for Ada</a></li>
      </ul>

      <p>Please take a look at <a
      href="https://bugs.debian.org/debian-policy">proposed updates to
      Debian Policy</a>, too.</p>

      <dl>
        <dt><a href="$(DOC)/manuals/developers-reference/">Developers' Reference</a></dt>

        <dd>
        Overview of the recommended procedures and the available resources
        for Debian developers -- another <strong>must-read</strong>.
        </dd>

        <dt><a href="$(DOC)/manuals/debmake-doc/">Guide for Debian Maintainers</a></dt>

        <dd>
        How to build a Debian package (in common language), including
        lots of examples. If you're planning to become a Debian developer
        or maintainer, this is a good starting point.
        </dd>
      </dl>


    </div>

  </div>

</div>


<h2><a id="workinprogress">Work in Progress: Links for active Debian developers and maintainers</a></h2>

<aside class="light">
  <span class="fas fa-wrench fa-5x"></span>
</aside>

<dl>
  <dt><a href="testing">Debian &lsquo;Testing&rsquo;</a></dt>
  <dd>
    Automatically generated from the &lsquo;unstable&rsquo; distribution:
    this is where you need to get your packages in order for them to be
    considered for the next Debian release.
  </dd>

  <dt><a href="https://bugs.debian.org/release-critical/">Release Critical Bugs</a></dt>
  <dd>
    A list of bugs which may cause a package to be removed from the
    &lsquo;testing&rsquo; distribution or may cause a delay for the
    next release. Bug reports with a severity higher than or equal to
    &lsquo;serious&rsquo; qualify for the list, so please make sure to
    fix those bugs against your packages as soon as you can.
  </dd>

  <dt><a href="$(HOME)/Bugs/">Debian Bug Tracking System (BTS)</a></dt>
    <dd>
    For reporting, discussing, and fixing bugs. The BTS is useful
    for both users and developers.
    </dd>
        
  <dt>Information about Debian Packages</dt>
    <dd>
      The <a href="https://qa.debian.org/developer.php">package
      information</a> and <a href="https://tracker.debian.org/">package
      tracker</a> web pages provide collections of valuable
      information to maintainers. Developers who want to keep
      track of other packages, can subscribe (through email)
      to a service which sends out copies of BTS mails and
      notifications for uploads and installations. Please see the <a
      href="$(DOC)/manuals/developers-reference/resources.html#pkg-tracker">package
      tracker manual</a> for further information.
    </dd>

    <dt><a href="wnpp/">Packages that need Help</a></dt>
      <dd>
      Work-Needing and Prospective Packages, WNPP for short, is a list
      of Debian packages in need of new maintainers and packages that
      have yet to be included in Debian.
      </dd>

    <dt><a href="$(DOC)/manuals/developers-reference/resources.html#incoming-system">Incoming System</a></dt>
      <dd>
      Internal archive servers: this is where new packages are uploaded
      to. Accepted packages are almost immediately available via a web
      browser and propagated to <a href="$(HOME)/mirror/">mirrors</a>
      four times a day.
      <br />
      <strong>Note:</strong> Due to the nature of &lsquo;incoming&rsquo;,
      we do not recommend mirroring it.
      </dd>

    <dt><a href="https://lintian.debian.org/">Lintian Reports</a></dt>
      <dd>
      <a href="https://packages.debian.org/unstable/devel/lintian">Lintian</a>
      is a program which checks whether a package conforms to the
      policy. Developers should use it before every upload.
      </dd>

    <dt><a href="$(DOC)/manuals/developers-reference/resources.html#experimental">Debian &lsquo;Experimental&rsquo;</a></dt>
      <dd>
      The &lsquo;experimental&rsquo; distribution is used as a temporary
      staging area for highly experimental software. Please install the
      <a href="https://packages.debian.org/experimental/">experimental
      packages</a> only if you already know how to use
      &lsquo;unstable&rsquo;.
      </dd>

    <dt><a href="https://wiki.debian.org/HelpDebian">Debian Wiki</a></dt>
      <dd>
      The Debian Wiki with advice for developers and other contributors.
      </dd>
</dl>

<h2><a id="projects">Projects: Internal Groups and Projects</a></h2>

<aside class="light">
  <span class="fas fa-folder-open fa-5x"></span>
</aside>

<ul>
<li><a href="website/">Debian Web Pages</a></li>
<li><a href="https://ftp-master.debian.org/">Debian Archive</a></li>
<li><a href="$(DOC)/ddp">Debian Documentation Project (DDP)</a></li>
<li><a href="https://qa.debian.org/">Debian Quality Assurance (QA) Team</a></li>
<li><a href="$(HOME)/CD/">CD/DVD Images</a></li>
<li><a href="https://wiki.debian.org/Keysigning">Keysigning</a></li>
<li><a href="https://wiki.debian.org/Keysigning/Coordination">Keysigning Coordination</a></li>
<li><a href="https://wiki.debian.org/DebianIPv6">Debian IPv6 Project</a></li>
<li><a href="buildd/">Autobuilder Network</a> and their <a href="https://buildd.debian.org/">Build Logs</a></li>
<li><a href="$(HOME)/international/l10n/ddtp">Debian Description Translation Project (DDTP)</a></li>
<li><a href="debian-installer/">The Debian Installer</a></li>
<li><a href="debian-live/">Debian Live</a></li>
<li><a href="$(HOME)/women/">Debian Women</a></li>
<li><a href="$(HOME)/blends/">Debian Pure Blends</a></li>

</ul>


<h2><a id="miscellaneous">Miscellaneous Links</a></h2>

<aside class="light">
  <span class="fas fa-bookmark fa-5x"></span>
</aside>

<ul>
<li><a href="https://debconf-video-team.pages.debian.net/videoplayer/">Recordings</a> of our Conference Talks</li>
<li><a href="passwordlessssh">Setting up SSH</a> so it doesn't ask you for a password</li>
<li>How to <a href="$(HOME)/MailingLists/HOWTO_start_list">request a new Mailing List</a></li>
<li>Information on <a href="$(HOME)/mirror/">Mirroring Debian</a></li>
<li>The <a href="https://qa.debian.org/data/bts/graphs/all.png">Graph of all Bugs</a></li>
<li><a href="https://ftp-master.debian.org/new.html">New Packages</a> waiting to be included in Debian (NEW Queue)</li>
<li><a href="https://packages.debian.org/unstable/main/newpkg">New Debian Packages</a> from the last 7 Days</li>
<li><a href="https://ftp-master.debian.org/removals.txt">Packages removed from Debian</a></li>
</ul>
