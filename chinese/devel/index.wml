#use wml::debian::template title="Debian 開發者天地" MAINPAGE="true"
#use wml::debian::recent_list
#use wml::debian::translation-check translation="fca004f92b97a053bc89c121827943f1eae0531b" maintainer="Hsin-lin Cheng"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> 尽管此页面上的所有信息以及\
指向其它页面的所有链接都是公开可访问的，但本网页主要针对 Debian 开发者。</p>
</aside>

<ul class="toc">
<li><a href="#basic">基本資訊</a></li>
<li><a href="#packaging">套件打包</a></li>
<li><a href="#workinprogress">正在进行的工作</a></li>
<li><a href="#projects">計畫</a></li>
<li><a href="#miscellaneous">雜項</a></li>
</ul>

<div class="row">

  <!-- left column -->
  <div class="column column-left" id="basic">
    <div style="text-align: center">
      <span class="fa fa-users fa-3x" style="float:left;margin-right:5px;margin-bottom:5px;"></span>
      <h2><a id="basic">一般信息</a></h2>
      <p>当前开发人员和维护者的列表，如何加入计划，以及指向开发人员数据库、章程、投票\
      过程、发布版本和体系架构的链接。</p>
    </div>

    <div style="text-align: left">
      <dl>
        <dt><a href="$(HOME)/intro/organization">Debian 组织</a></dt>
        <dd>超过一千名志愿者是 Debian 计划的一部分。此页面解释了 Debian 的组织结构，\
        列出了团队及其成员的名单及其联系地址。</dd>
        <dt><a href="$(HOME)/intro/people">成员</a></dt>
        <dd><a
        href="https://wiki.debian.org/DebianDeveloper">Debian 开发人员（DD）</a>\
        （Debian 计划中的正式成员）和 <a
        href="https://wiki.debian.org/DebianMaintainer">Debian 维护者（DM）</a>\
        对计划有所贡献。请查看 <a
        href="https://nm.debian.org/public/people/dd_all/">Debian 开发人员列表</a>\
        和 <a
        href="https://nm.debian.org/public/people/dm_all/">Debian 维护者列表</a>\
        ，以了解包括他们维护的软件包在内的相关成员的更多信息。我们还有 <a
        href="developers.loc">Debian 开发人员的世界地图</a>和包含各种 Debian \
        活动图像的<a href="https://gallery.debconf.org/">画廊</a>。</dd>
        <dt><a href="join/">如何参与 Debian</a></dt>
        <dd>您愿意参与贡献并加入该计划吗？我们一直在寻找具有技术技能的新开发人员或自由\
        软件爱好者。想要了解更多信息，请访问此页面。</dd>
        <dt><a href="https://db.debian.org/">开发人员数据库</a></dt>
        <dd>这个数据库中的一些信息是所有人都可以访问的，一些信息只有已登录帐号的开发\
        人员才能访问。数据库包含<a
        href="https://db.debian.org/machines.cgi">計畫所使用的機器</a>和<a
        href="extract_key">开发人员的 GnuPG 密钥</a>等信息。拥有帐户的开发人员\
        可以<a href="https://db.debian.org/password.html">更改他们的密码</a>\
        并了解如何为他们的 Debian 帐户设置<a
        href="https://db.debian.org/forward.html">邮件转发</a>。如果您打算使用\
        其中一台 Debian 机器，请务必阅读 <a href="dmup">Debian 機器使用方針</a>。</dd>
        <dt><a href="constitution">章程</a></dt>
        <dd>这个文档描述了计划中正式决策的组织结构。</dd>
        <dt><a href="$(HOME)/vote/">投票信息</a></dt>
        <dd>我們如何選舉領導者、選擇我們的標誌（logo），還有一般而言，我們是如何投票的。</dd>
        <dt><a href="$(HOME)/releases/">发行版本</a></dt>
        <dd>此页面列出了当前的发行版本（<a
        href="$(HOME)/releases/stable/">稳定版（stable）</a>、<a
        href="$(HOME)/releases/testing/">测试版（testing）</a>和<a
        href="$(HOME)/releases/unstable/">不稳定版（unstable）</a>）并包含\
        旧版本及其代号的索引。</dd>
        <dt><a href="$(HOME)/ports/">各种体系架构</a></dt>
        <dd>Debian 在许多不同的体系架构上运行。此页面收集了有关各种 Debian 移植\
        的信息，一些基于 Linux 内核，另一些基于 FreeBSD、NetBSD 和 Hurd 内核。</dd>
     </dl>
    </div>

  </div>

  <!-- right column -->
  <div class="column column-right" id="software">
    <div style="text-align: center">
      <span class="fa fa-code fa-3x" style="float:left;margin-right:5px;margin-bottom:5px;"></span>
      <h2><a id="packaging">套件打包</a></h2>
      <p>指向我们的政策手册和其它与 Debian 政策、程序、另外的 Debian 开发人员资源\
      以及新维护者指南相关的文档的链接。</p>
    </div>

    <div style="text-align: left">
      <dl>
        <dt><a href="$(DOC)/debian-policy/">Debian 方針手冊</a></dt>
        <dd>
        這個手冊描述 Debian 發行版（distribution）的方針需求。包含 Debian 檔案庫\
        （archive）的結構和內容，幾種此作業系統的設計議題，除了技術需求之外，每個套件\
        還必須滿足這些需求才能被放進發行版中。

        <p>總之您<strong>需要</strong>閱讀一下。</p>
        </dd>
      </dl>

      <p>有幾個與方針有關的文件您也許會感興趣，例如：</p>

      <ul>
        <li><a href="https://wiki.linuxfoundation.org/lsb/fhs/">檔案系統階層式標準</a> (FHS)
        <br />FHS 定义了目录结构和目录内容（文件的位置）；必须遵守 3.0 版（请\
        参阅 Debian 政策手册的<a
        href="https://www.debian.org/doc/debian-policy/ch-opersys.html#file-system-hierarchy">第 9 章</a>）。</li>
        <li><a href="$(DOC)/packaging-manuals/build-essential">build-essential 套件列表</a>
        <br />build-essential 套件是在你嘗試编译软件以及构建一个或一套软件包之前\
        預期要有的套件。在<a
        href="https://www.debian.org/doc/debian-policy/ch-relationships.html">声明关系</a>\
        时，您不必在 Build-Depends 段落中引入（include）它们。</li>
        <li><a href="$(DOC)/packaging-manuals/menu-policy/">菜单系統</a>
        <br />Debian 的菜单条目结构；请同时查阅<a
        href="$(DOC)/packaging-manuals/menu.html/">菜单系统</a>文档。</li>
        <li><a href="$(DOC)/packaging-manuals/debian-emacs-policy">Emacs 方針</a>
        <li><a href="$(DOC)/packaging-manuals/java-policy/">Java 方針</a>
        <li><a href="$(DOC)/packaging-manuals/perl-policy/">Perl 方針</a>
        <li><a href="$(DOC)/packaging-manuals/python-policy/">Python 方針</a>
#	<li><a href="https://pkg-mono.alioth.debian.org/cli-policy/">Debian CLI 方針</a>
#	<br />關於打包 Mono 、其他 CLR 與 CLI 應用程式還有函式庫的基本方針。</li>
        <li><a href="$(DOC)/packaging-manuals/debconf_specification.html">Debconf 規格書</a>
#        <li><a href="https://dict-common.alioth.debian.org/">拼寫字典與工具方針</a>
#        <br /><kbd>ispell</kbd> / <kbd>myspell</kbd> 字典與詞彙表子方針。</li>
#        <li><a href="https://webapps-common.alioth.debian.org/draft/html/">Webapps 方針手冊</a>（草稿）
#	<br />網頁應用程式的子方針。</li>
#        <li><a href="https://webapps-common.alioth.debian.org/draft-php/html/">PHP 方針</a>（草稿）
#	<br />PHP 的套件打包標準。</li>
        <li><a href="https://www.debian.org/doc/manuals/dbapp-policy/">資料庫應用程式方針</a>（草稿）
        <li><a href="https://tcltk-team.pages.debian.net/policy-html/tcltk-policy.html/">Tcl/Tk 方針</a>（草稿）
        <li><a
        href="https://people.debian.org/~lbrenta/debian-ada-policy.html">对 Ada 的 Debian 方針</a>
      </ul>

      <p>也请看一看<a
      href="https://bugs.debian.org/debian-policy">被提議更新的 Debian 方針</a>。</p>

      <dl>
        <dt><a href="$(DOC)/manuals/developers-reference/">开发人员参考文档</a></dt>

        <dd>
        为 Debian 开发人员推荐的程序和可用资源的概述 —— 另一个<strong>必读</strong>的文档。
        </dd>

        <dt><a href="$(DOC)/manuals/debmake-doc/">Debian 维护者指南</a></dt>

        <dd>
        以通用語言描述如何构建 Debian 软件包，且附有大量示例。如果你想成为 Debian \
        开发人员或维护者，这是一个很好的起点。
        </dd>
      </dl>


    </div>

  </div>

</div>


<h2><a id="workinprogress">正在进行的工作：为活跃的 Debian 开发人员和维护者准备的链接</a></h2>

<aside class="light">
  <span class="fas fa-wrench fa-5x"></span>
</aside>

<dl>
  <dt><a href="testing">Debian 测试版</a></dt>
  <dd>
    自动从不稳定版生成：就是那個您必須關注您的套件，讓它們得以隨著下次 Debian 發行新版\
    時一起發行的地方。
  </dd>

  <dt><a href="https://bugs.debian.org/release-critical/">影響發行的致命缺陷 （Release Critical Bugs）</a></dt>
  <dd>
    這個缺陷列表，列出可能導致一個套件從測試版本除名，或甚至延後正式發行的嚴重缺陷。\
    這些具有 &lsquo;serious&rsquo; 或更高嚴重性等級的缺陷 -- 盡你所能的愈快修正它們愈好。
  </dd>

  <dt><a href="$(HOME)/Bugs/">Debian Bug 回報系統（BTS）</a></dt>
    <dd>
    用來回報、討論，與修正 bug。BTS 對使用者與開發者都非常有用。
    </dd>

  <dt>有关 Debian 软件包的信息</dt>
    <dd>
      <a href="https://qa.debian.org/developer.php">套件資訊</a>\
      與<a href="https://tracker.debian.org/">套件追蹤</a>網頁提供維護者有\
      價值的資訊集。想要追踪其它软件包的开发人员可以（通过电子邮件）订阅一项服务，\
      该服务会发送 BTS 邮件副本以及上传和安装通知。请查看<a
      href="$(DOC)/manuals/developers-reference/resources.html#pkg-tracker">套件追蹤系統</a>\
      以获得更多信息。
    </dd>

    <dt><a href="wnpp/">需要協助的套件（WNPP）</a></dt>
      <dd>
      WNPP 是 Work-Needing and Prospective Packages 的縮寫，列出需要新維護者的 Debian 套件，以及還未被打包的套件。
      </dd>

    <dt><a href="$(DOC)/manuals/developers-reference/resources.html#incoming-system">新進（Incoming）系統</a></dt>
      <dd>
      內部倉庫服务器：新進套件首先上傳到的地方。被接受的套件幾乎立刻就可以通过浏览器取得\
      ，并会以每天四次的頻率更新到各<a href="$(HOME)/mirror/">鏡像站</a>。
      <br />
      <strong>注意</strong>：因為新進系統隨時變化的特性，並不建議鏡像這個目錄本身。
      </dd>

    <dt><a href="https://lintian.debian.org/">Lintian 報告</a></dt>
      <dd>
      <a href="https://packages.debian.org/unstable/devel/lintian">Lintian</a> \
      是檢查套件是否符合 Debian 方針的規範的程序。开发人员應該在每次上傳前使用它。
      </dd>

    <dt><a href="$(DOC)/manuals/developers-reference/resources.html#experimental">Debian 實驗版本（Experimental）</a></dt>
      <dd>
      實驗版本是用來暫放高度實驗性軟體的臨時地方。<a
      href="https://packages.debian.org/experimental/">實驗性套件</a>僅供你\
      已經懂得如何使用不穩定版（unstable）後使用。
      </dd>

    <dt><a href="https://wiki.debian.org/HelpDebian">Debian Wiki</a></dt>
      <dd>
      Debian Wiki 为开发人员和其他贡献者提供建议。
      </dd>
</dl>

<h2><a id="projects">计划：内部群组与计划</a></h2>

<aside class="light">
  <span class="fas fa-folder-open fa-5x"></span>
</aside>

<ul>
<li><a href="website/">Debian 網頁</a></li>
<li><a href="https://ftp-master.debian.org/">Debian 檔案庫 (archive)</a></li>
<li><a href="$(DOC)/ddp">Debian 文件計畫（DDP）</a></li>
<li><a href="https://qa.debian.org/">品質擔保（QA）</a>組</li>
<li><a href="$(HOME)/CD/">Debian 光碟映像檔</a></li>
<li><a href="https://wiki.debian.org/Keysigning">密钥签署</a></li>
<li><a href="https://wiki.debian.org/Keysigning/Coordination">[CN:密钥:][HKTW:金鑰:]簽署協調網頁</a></li>
<li><a href="https://wiki.debian.org/DebianIPv6">Debian IPv6 計畫</a></li>
<li><a href="buildd/">Auto-builder 網路</a>及其<a href="https://buildd.debian.org/">构建日志</a></li>
<li><a href="$(HOME)/international/l10n/ddtp">Debian 套件描述（Description）翻譯計畫（DDTP）</a></li>
<li><a href="debian-installer/">Debian 安裝程式</a></li>
<li><a href="debian-live/">Debian Live</a></li>
<li><a href="$(HOME)/women/">Debian 女性</a></li>
<li><a href="$(HOME)/blends/">Debian Pure Blends</a></li>
</ul>


<h2><a id="miscellaneous">杂项链接</a></h2>

<aside class="light">
  <span class="fas fa-bookmark fa-5x"></span>
</aside>

<ul>
<li>我们的会议讨论的<a href="https://debconf-video-team.pages.debian.net/videoplayer/">录像</a></li>
<li><a href="passwordlessssh">配置 ssh 使其不再向您索取密码</a></li>
<li>如何<a href="$(HOME)/MailingLists/HOWTO_start_list">请求建立一个新的邮件列表</a></li>
<li>有关<a href="$(HOME)/mirror/">为 Debian 做镜像</a>的信息</li>
<li><a href="https://qa.debian.org/data/bts/graphs/all.png">非 \
wishlist 级别的 bug 图表</a></li>
<li><a href="https://ftp-master.debian.org/new.html">等待进入 Debian 的\
新软件包</a>（NEW 队列）</li>
<li><a href="https://packages.debian.org/unstable/main/newpkg">最近 7 天\
进入 Debian 的新软件包</a></li>
<li><a href="https://ftp-master.debian.org/removals.txt">从 Debian 中移除\
的软件包</a></li>
</ul>
